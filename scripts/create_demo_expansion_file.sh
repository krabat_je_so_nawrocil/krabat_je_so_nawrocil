#!/bin/bash

rm -f main.2.rapaki.krabat.obb

rm -rf tmp/
mkdir tmp

mkdir tmp/gfx
cp -r gfx/anims tmp/gfx
cp -r gfx/char tmp/gfx
cp -r gfx/cursors tmp/gfx
cp -r gfx/doma tmp/gfx
cp -r gfx/icons tmp/gfx
cp -r gfx/intro tmp/gfx
cp -r gfx/inventar tmp/gfx
cp -r gfx/mainmenu tmp/gfx
cp -r gfx/mainmenu tmp/gfx
cp -r gfx/mainmenu tmp/gfx
cp -r gfx/mainmenu tmp/gfx
cp -r gfx/mainmenu tmp/gfx
cp -r gfx/*.png tmp/gfx

mkdir -p tmp/sound/music/ogg
cp sound/music/ogg/titel01.ogg tmp/sound/music/ogg
cp sound/music/ogg/titel02.ogg tmp/sound/music/ogg
cp sound/music/ogg/titel03.ogg tmp/sound/music/ogg
cp sound/music/ogg/titel23.ogg tmp/sound/music/ogg
cp -r sound/sfx tmp/sound

mkdir tmp/resource
cp resource/*.kra tmp/resource
cp resource/*.txt tmp/resource

cd tmp/
for i in $(find . -name ".svn"); do rm -rf $i; done

zip -n .ogg:.wav -r expansion_demo.zip gfx/ sound/music/ogg sound/sfx resource/*.kra resource/*.txt -x *.svn-base -x .svn/

mv expansion_demo.zip ../main.5.rapaki.krabat.obb
cd ..

