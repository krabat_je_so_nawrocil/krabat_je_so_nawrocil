#!/bin/bash

rm -f expansion_full.zip
rm -f main.1.rapaki.krabat.obb

rm -rf tmp/
mkdir tmp

cp -r gfx tmp/
cp -r gfx-dd tmp/
mkdir -p tmp/sound/music
cp -r sound/music/ogg tmp/sound/music
cp -r sound/sfx tmp/sound
cp -r sound/sfx-dd tmp/sound
mkdir tmp/resource
cp resource/*.kra tmp/resource
cp resource/*.txt tmp/resource

cd tmp/
for i in $(find . -name ".svn"); do rm -rf $i; done

zip -n .ogg:.wav -r expansion_full.zip gfx/ gfx-dd/ sound/music/ogg sound/sfx sound/sfx-dd resource/*.kra resource/*.txt -x *.svn-base -x .svn/

mv expansion_full.zip ../main.1.rapaki.krabat.obb
cd ..

