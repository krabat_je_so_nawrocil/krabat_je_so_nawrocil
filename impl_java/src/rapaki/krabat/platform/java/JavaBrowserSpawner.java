package rapaki.krabat.platform.java;

import rapaki.krabat.platform.GenericBrowserSpawner;

public class JavaBrowserSpawner implements GenericBrowserSpawner {

	public void spawnBrowser(String urlString) {
        
		if( !java.awt.Desktop.isDesktopSupported() ) {
            System.err.println( "Desktop is not supported (fatal)" );
        }

        java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

        if( !desktop.isSupported( java.awt.Desktop.Action.BROWSE ) ) {

            System.err.println( "Desktop doesn't support the browse action (fatal)" );
        }

        try {
        	
        	java.net.URI uri = new java.net.URI( urlString );
        	desktop.browse( uri );
        }
        catch ( Exception e ) {

        	System.err.println( e.getMessage() );
        }

	}

}
