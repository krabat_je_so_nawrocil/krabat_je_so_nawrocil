package rapaki.krabat.platform;

public interface GenericBrowserSpawner {

	public void spawnBrowser(String urlString);
	
}
