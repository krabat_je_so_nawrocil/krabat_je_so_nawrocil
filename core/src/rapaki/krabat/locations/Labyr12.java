/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.locations;

import rapaki.krabat.Start;
import rapaki.krabat.main.GenericInputEvent;
import rapaki.krabat.main.GenericKeyEvent;
import rapaki.krabat.main.GenericMouseEvent;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.GenericRectangle;
import rapaki.krabat.main.Mainlaby;
import rapaki.krabat.main.Borderrect;
import rapaki.krabat.main.Bordertrapez;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.sound.BackgroundMusicPlayer;

public class Labyr12 extends Mainlaby 
{
  private GenericImage background, lab122, lab123; 

  // Konstanten - Rects
  private static final Borderrect untererAusgang = new Borderrect (141, 435, 356, 479);
  private static final Borderrect rechterAusgang = new Borderrect (611, 248, 639, 326);
  private static final Borderrect lab122Rect     = new Borderrect (296, 162, 610, 355);
  private static final Borderrect lab123Rect     = new Borderrect (189, 329, 252, 377);

  // Exitlocation - Array
  // private static final int[] Exitdown  = {52, 54, 55, 58, 60, 61};
  private static final int[] Exitright = {51, 52, 53, 54, /*56,*/ 57, 58, 59, 61};

  // Konstante Points
  private static final GenericPoint Pright = new GenericPoint (639, 282);
  private static final GenericPoint Pdown  = new GenericPoint (254, 479);

  // Initialisierung ////////////////////////////////////////////////////////

  // Instanz von dieser Location erzeugen
  public Labyr12 (Start caller, int Richtung) 
  {
    super (caller);
    System.out.println("Laby 12");

    mainFrame.Freeze (true);

    mainFrame.CheckKrabat ();

    mainFrame.krabat.maxx = 0;
    mainFrame.krabat.zoomf = 6.97f;
    mainFrame.krabat.defScale = 0;
  	
    InitLocation (Richtung);
    
    mainFrame.Freeze (false);
  }
  
  // Gegend intialisieren (Grenzen u.s.w.)
  private void InitLocation (int Richtung)
  {
    // Grenzen setzen
    mainFrame.wegGeher.vBorders.removeAllElements ();
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (277, 295, 225, 295, 389, 479));
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (277, 356, 295, 388));
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (317, 327, 257, 327, 305, 355));
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (328, 305, 411, 319));
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (412, 269, 441, 319));
    mainFrame.wegGeher.vBorders.addElement (new Bordertrapez (442, 269, 639, 293));
       
    // Matrix loeschen
    mainFrame.wegSucher.ClearMatrix (6);
	
    // moegliche Wege eintragen (Positionen (= Rechtecke) verbinden)
    mainFrame.wegSucher.PosVerbinden (0, 1); 
    mainFrame.wegSucher.PosVerbinden (1, 2); 
    mainFrame.wegSucher.PosVerbinden (2, 3); 
    mainFrame.wegSucher.PosVerbinden (3, 4);
    mainFrame.wegSucher.PosVerbinden (4, 5);

    InitImages();
    switch (Richtung)
    {
      case 0: 
        // Einsprung fuer Load
        BackgroundMusicPlayer.getInstance ().playTrack (10, true);
        break;
    	case 9:
        // von rechts aus
    		mainFrame.krabat.SetKrabatPos (new GenericPoint (619, 282));
        mainFrame.krabat.SetFacing (9);
        break;
      case 12: 
        // von unten aus
        BackgroundMusicPlayer.getInstance ().playTrack (10, true);
        mainFrame.krabat.SetKrabatPos (new GenericPoint (254, 452));
        mainFrame.krabat.SetFacing (12);
        break;
    }    
    
    // Am Ein- / Ausgang immer alle Variablen loeschen, damit es von vorn losgeht...
    ClearErscheinen();
  }

  // Bilder vorbereiten
  private void InitImages() 
  {
    background = getPicture ("gfx/labyrinth/laby12.gif");
    lab122     = getPicture ("gfx/labyrinth/lab12-2.gif");
    lab123     = getPicture ("gfx/labyrinth/lab12-3.gif");

    loadPicture();
  }

  public void cleanup() {
	  background = null;
	  lab122 = null;
	  lab123 = null;
  }

  // Paint-Routine dieser Location //////////////////////////////////////////

  public void paintLocation (GenericDrawingContext g)
  {

    // Clipping -Region initialisieren
    if (mainFrame.Clipset == false)
    {
      mainFrame.scrollx = 0;
      mainFrame.scrolly = 0;
      Cursorform = 200;
    	evalMouseMoveEvent (mainFrame.Mousepoint);
    	mainFrame.Clipset = true;
      g.setClip (0, 0, 644, 484);
      mainFrame.isAnim = true;
    }

    // Hintergrund und Krabat zeichnen
    g.drawImage (background, 0, 0, null);

    // Debugging - Zeichnen der Laufrechtecke
    // mainFrame.showrect.Zeichne(g, mainFrame.wegGeher.vBorders);
  	
    
    mainFrame.wegGeher.GeheWeg ();
    
    // Animation??
    if (mainFrame.krabat.nAnimation != 0)
    { 
      mainFrame.krabat.DoAnimation (g);
      
      // Cursorruecksetzung nach Animationsende
      if (mainFrame.krabat.nAnimation == 0) evalMouseMoveEvent (mainFrame.Mousepoint);
    }  
    else
    {
      if ((mainFrame.talkCount > 0) && (TalkPerson != 0))
      {
        // beim Reden
        switch (TalkPerson)
        {
          case 1:
            // Krabat spricht gestikulierend
            mainFrame.krabat.talkKrabat (g);
            break;
          case 3:
            // Krabat spricht im Monolog
            mainFrame.krabat.describeKrabat (g);
            break;
          default:
            // Krabat steht nur da
            mainFrame.krabat.drawKrabat (g);
            break;
        }    
      }
      // Rumstehen oder Laufen
      else mainFrame.krabat.drawKrabat (g);
    }  
    
    // Steht Krabat hinter einem Gegenstand ? Koordinaten noch mal checken !!!
    GenericPoint pKrTemp = mainFrame.krabat.GetKrabatPos ();

    // hinterm horiz3 (nur Clipping - Region wird neugezeichnet)
    if (lab122Rect.IsPointInRect (pKrTemp) == true)
    {
      g.drawImage (lab122, 273, 162, null);
    }

    // hinterm horiz3 (nur Clipping - Region wird neugezeichnet)
    if (lab123Rect.IsPointInRect (pKrTemp) == true)
    {
      g.drawImage (lab123, 211, 354, null);
    }

    // sonst noch was zu tun ?
    if  (outputText != "")
    {
      // Textausgabe
      GenericRectangle my;
      my = g.getClipBounds();
      g.setClip (0, 0, 644, 484);
      mainFrame.ifont.drawString (g, outputText, outputTextPos.x, outputTextPos.y, FarbenArray[TalkPerson]);
      g.setClip( (int) my.getX(), (int) my.getY(), (int) my.getWidth(), (int) my.getHeight()); 
    }

    // Redeschleife herunterzaehlen und Neuzeichnen ermoeglichen
    if (mainFrame.talkCount > 0)
    {
      -- mainFrame.talkCount;
      if (mainFrame.talkCount <= 1)
      {
        mainFrame.Clipset = false;
        outputText = "";
        TalkPerson = 0;
      }
    }  
    
    if ((TalkPause > 0) && (mainFrame.talkCount < 1)) TalkPause--;

    // Gibt es was zu tun ?
    if ((nextActionID != 0) && (TalkPause < 1) && (mainFrame.talkCount < 1)) DoAction ();
  }


  // Mouse-Auswertung dieser Location ///////////////////////////////////////

  public void evalMouseEvent (GenericMouseEvent e)
  {
    GenericPoint pTemp = e.getPoint ();
    if (mainFrame.talkCount != 0) mainFrame.Clipset = false;
    if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
    outputText="";
    
    // Wenn in Animation, dann normales Gameplay aussetzen
    if (mainFrame.fPlayAnim == true)
    {
      return;
    }
    
    // Wenn Krabat - Animation, dann normales Gameplay aussetzen
    if (mainFrame.krabat.nAnimation != 0)
    {
    	return;
    }    

    // wenn InventarCursor, dann anders reagieren
    if (mainFrame.invCursor == true)
    {
      // linker Maustaste
      if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
      {
	  nextActionID = 0;

        Borderrect tmp = mainFrame.krabat.KrabatRect();

        // Aktion, wenn Krabat angeclickt wurde
        if (tmp.IsPointInRect (pTemp) == true)
        {
        	nextActionID = 500 + mainFrame.whatItem;
        	mainFrame.repaint();
        	return;
        }	

        // wenn nichts anderes gewaehlt, dann nur hinlaufen
        mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
        mainFrame.repaint();
      }
      
      // rechte Maustaste
      else
      {
        // grundsaetzlich Gegenstand wieder ablegen
        mainFrame.invCursor = false;
        evalMouseMoveEvent (mainFrame.Mousepoint);
        nextActionID = 0;
        mainFrame.krabat.StopWalking();
        mainFrame.repaint();
        return;
      }  
    }

    // normaler Cursor, normale Reaktion
    else
    {
      if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
      {   
        // linke Maustaste
        nextActionID = 0;

        // zu naechstem Laby gehen rechts
        if (rechterAusgang.IsPointInRect (pTemp) == true)
        {
          nextActionID = 101;
          GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
          // Wenn nahe am Ausgang, dann "gerade" verlassen
          if (rechterAusgang.IsPointInRect (kt) == false)
          {
          	pTemp = Pright;
          }
          else
          {
          	pTemp = new GenericPoint (Pright.x, kt.y);
          }
            
          evalNewLocationRight();
          if (mainFrame.dClick == true)
          {
            mainFrame.krabat.StopWalking();
            mainFrame.repaint();
            return;
          }  
        }
        
        // zu naechstem Laby gehen unten
        if (untererAusgang.IsPointInRect (pTemp) == true)
        {
          nextActionID = 100;
          GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
          // Wenn nahe am Ausgang, dann "gerade" verlassen
          if (untererAusgang.IsPointInRect (kt) == false)
          {
          	pTemp = Pdown;
          }
          else
          {
          	pTemp = new GenericPoint (kt.x, Pdown.y);
          }
            
          // evalNewLocationDown();
          if (mainFrame.dClick == true)
          {
            mainFrame.krabat.StopWalking();
            mainFrame.repaint();
            return;
          }  
        }

        mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
        mainFrame.repaint();
      }
      
      else
      {
        // rechte Maustaste

        // Naechstes Laby anschauen
        if (rechterAusgang.IsPointInRect (pTemp) == true)
        {
          return;  
        }
        
        // Naechstes Laby anschauen
        if (untererAusgang.IsPointInRect (pTemp) == true)
        {
          return;
        }
             
      	// Inventarroutine aktivieren, wenn nichts anderes angeklickt ist
        nextActionID = 123;
        mainFrame.krabat.StopWalking();
        mainFrame.repaint();
      }
    }
  }

  // befindet sich Cursor ueber Gegenstand, dann Kreuz-Cursor
  public void evalMouseMoveEvent (GenericPoint pTemp)
  {
    // Wenn Animation oder Krabat - Animation, dann transparenter Cursor
    if ((mainFrame.fPlayAnim == true) || (mainFrame.krabat.nAnimation != 0))
    {
      if (Cursorform != 20)
      {
      	Cursorform = 20;
      	mainFrame.setCursor (mainFrame.Nix);
      }
      return;		
    }
    
    // wenn InventarCursor, dann anders reagieren
    if (mainFrame.invCursor == true)
    {
      // hier kommt Routine hin, die Highlight berechnet
    	Borderrect tmp = mainFrame.krabat.KrabatRect();
    	if (tmp.IsPointInRect (pTemp) == true)
    	{
    		mainFrame.invHighCursor = true;
    	}
    	else mainFrame.invHighCursor = false;
    	
    	if ((Cursorform != 10) && (mainFrame.invHighCursor == false))
    	{
    		Cursorform = 10;
    		mainFrame.setCursor (mainFrame.Cinventar);
    	}
    	
    	if ((Cursorform != 11) && (mainFrame.invHighCursor == true))
    	{
    		Cursorform = 11;
    		mainFrame.setCursor (mainFrame.CHinventar);
    	}	
    }
  	
    // normaler Cursor, normale Reaktion
    else
    {
      /*if ((brEingang.IsPointInRect (pTemp) == true) ||
          (brWagen.IsPointInRect (pTemp) == true))
      {
        if (Cursorform != 1)
        {
          mainFrame.setCursor (mainFrame.Kreuz);
          Cursorform = 1;
        }
        return;
      }*/

      if (untererAusgang.IsPointInRect (pTemp) == true)
      {
        if (Cursorform != 5)
        {
          mainFrame.setCursor(mainFrame.Cdown);
          Cursorform = 5;
        }
        return;
      }
    
      if (rechterAusgang.IsPointInRect (pTemp) == true)
      {
        if (Cursorform != 3)
        {
          mainFrame.setCursor (mainFrame.Cright);
          Cursorform = 3;
        }
        return;
      }

      // sonst normal-Cursor
      if (Cursorform != 0)
      {
        mainFrame.setCursor (mainFrame.Normal);
        Cursorform = 0;
      }
    }
  }

  // dieses Event nicht beachten
  public void evalMouseExitEvent (GenericMouseEvent e) {
  }

  // Key - Auswertung dieser Location /////////////////////////////////

  public void evalKeyEvent (GenericKeyEvent e)
  {
    // Wenn Inventarcursor, dann keine Keys
    if (mainFrame.invCursor == true) return;

    // Bei Animationen keine Keys
    if (mainFrame.fPlayAnim == true) return;

    // Bei Krabat - Animation keine Keys
    if (mainFrame.krabat.nAnimation != 0) return;

    // Nur auf Funktionstasten reagieren
    int Taste = e.getKeyCode();

    // Hauptmenue aktivieren
    if (Taste == GenericKeyEvent.VK_F1)
    {
      Keyclear();
      nextActionID = 122;
      mainFrame.repaint();
      return;
    }    

    // Save - Screen aktivieren
    if (Taste == GenericKeyEvent.VK_F2)
    {
      Keyclear();
      nextActionID = 121;
      mainFrame.repaint();
      return;
    }

    // Load - Screen aktivieren
    if (Taste == GenericKeyEvent.VK_F3)
    {
      Keyclear();
      nextActionID = 120;
      mainFrame.repaint();
      return;
    } 
  }  

  // Vor Key - Events alles deaktivieren
  private void Keyclear()
  {
    outputText="";
    if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
    mainFrame.Clipset = false;
    mainFrame.isAnim = false;
    mainFrame.krabat.StopWalking();
  }

  private void evalNewLocationRight()
  {
   	boolean exit = false;
   	int zuffi = 0;
   	while (exit == false)
   	{
   		zuffi = (int) Math.round (Math.random() * 12) + 50;
      for (int i = 0; i < Exitright.length; i++)
      {
      	if (zuffi == Exitright[i]) exit = true;
      }
    }  		
    newloc = zuffi;
  }
  
  /*
  private void evalNewLocationDown()
  {
   	boolean exit = false;
   	int zuffi = 0;
   	while (exit == false)
   	{
   		zuffi = (int) Math.round (Math.random() * 12) + 50;
      for (int i = 0; i < Exitdown.length; i++)
      {
      	if (zuffi == Exitdown[i]) exit = true;
      }
    }
    newloc = zuffi;  		
  }	
  */			

  // Aktionen dieser Location ////////////////////////////////////////

  private void DoAction ()
  {
    // nichts zu tun, oder Krabat laeuft noch
    if ((mainFrame.krabat.isWandering == true) ||
	(mainFrame.krabat.isWalking == true))
      return;

    // hier wird zu den Standardausreden von Krabat verzweigt, wenn noetig (in Superklasse)
    if ((nextActionID > 499) && (nextActionID < 600))
    {
    	setKrabatAusrede();
    	
    	// manche Ausreden erfordern neuen Cursor !!!
    	
    	evalMouseMoveEvent (mainFrame.Mousepoint);
    	
    	return;
    }		

  	// Hier Evaluation der Screenaufrufe, in Superklasse
  	if ((nextActionID > 119) && (nextActionID < 129))
  	{
  		SwitchScreen ();
  		return;
  	}		
  	
    // Was soll Krabat machen ?
    switch (nextActionID)
      {
      case 100:
        // Gehe zu naechstem Laby unten
        mainFrame.Clipset = false;
        mainFrame.isAnim = false;
        nextActionID = 0;
        
        // Hier Unterscheidung, ob Exit oder weiter im Laby
        /*if (newloc == 17)*/ mainFrame.ConstructLocation (17);
        /*else              mainFrame.ConstructLocation (newloc, 6);*/
        mainFrame.DestructLocation  (62);
        mainFrame.repaint();
        break;

      case 101:
        // gehe zu naechstem Laby rechts 
        mainFrame.Clipset = false;
        mainFrame.isAnim = false;
        nextActionID = 0;
        mainFrame.ConstructLocation (newloc, 3);
        mainFrame.DestructLocation  (62);
      	mainFrame.repaint();
        break;

      default:
        System.out.println ("Falsche Action-ID !");
      }

  }
}