/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.anims;

import rapaki.krabat.Start;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.Borderrect;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;

public class Mlynk2 extends Mainanim
{
    // Alle GenericImage - Objekte
    private GenericImage[] krabat_back;
  
    private GenericImage[] krabat_stand_front_head;
    private GenericImage[] krabat_stand_front_body;
    private GenericImage[] krabat_stand_left_head;
    private GenericImage[] krabat_stand_left_body;
    private GenericImage[] krabat_stand_right_head;
    private GenericImage[] krabat_stand_right_body;
  
    private GenericImage[] krabat_talk_front_head;
    private GenericImage[] krabat_talk_front_body;
    private GenericImage[] krabat_talk_left_head;
    private GenericImage[] krabat_talk_left_body;
    private GenericImage[] krabat_talk_right_head;
    private GenericImage[] krabat_talk_right_body;
  
    private GenericImage krabat_hat_stock;
    private GenericImage krabat_gib_karte;
    private GenericImage krabat_hat_stock_andersrum;
    
  // Grundlegende Variablen
    private float xps, yps;               // genaue Position der Fuesse fuer Offsetberechnung
    private float txps, typs;             // temporaere Variablen fuer genaue Position
    // public  boolean isWandering = false;  // gilt fuer ganze Route
    // public  boolean isWalking = false;    // gilt bis zum naechsten Rect.
    private int anim_pos = 0;             // Animationsbild
    // public  boolean clearanimpos = true;  // Bewirkt Standsprite nach Laufen 
  
    private int Zwinker = 0;
    private static final int BODYOFFSET = 29;
  
    private int Verhinderkopf;
    private static final int MAX_VERHINDERKOPF = 2;
  
    private int Verhinderbody;
    private static final int MAX_VERHINDERBODY = 8;
  
    private int Head = 0;
    private int Body = 0;
  
  // Variablen fuer Bewegung und Richtung
    private GenericPoint walkto = new GenericPoint (0, 0);                 // Zielpunkt fuer Move()
    private GenericPoint Twalkto = new GenericPoint (0, 0);                // Zielpunkt, der in MoveTo() gesetzt und von Move uebernommen wird
    // hier ist das Problem der Threadsynchronisierung !!!!!!!
    private int direction_x = 1;          // Laufrichtung x
    private int Tdirection_x = 1;
  
    private int direction_y = 1;          // Laufrichtung y
    private int Tdirection_y = 1;
  
    private boolean horizontal = true;    // Animationen in x oder y Richtung
    private boolean Thorizontal = true;
  
    public  boolean upsidedown = false;   // Beim Berg - und Tallauf GenericImage wenden
  
  // Spritevariablen
    private static final int CWIDTH  = 70;// Default - Werte Hoehe,Breite
    private static final int CHEIGHT = 100;
    private static final int CWIDTH_STOCK = 100;  // so breit ist er, wenn er den Stock hat
    private static final int COFFSET_STOCK = 132;  // so weit ist das ungezoomte GenericImage nach links verschoben (* 2) = virtuelle Breite
    // wenn man beachtet, dass die Fussposition so bleiben soll
  
    private float scaleVerhaeltnisNormal;
    private float scaleVerhaeltnisStock;

    // Abstaende default
    // private static final int[] CHORIZ_DIST = {6, 10, 11, 9, 10, 10, 10, 11, 11, 10};
    private static final int CVERT_DIST = 1; 
  
  // Variablen fuer Laufberechnung
    // private static final int CLOHNENX = 49;  // Werte fuer Entscheidung, ob sich
    // private static final int CLOHNENY = 10;  // Laufen ueberhaupt lohnt (halber Schritt)

  // Variablen fuer Animationen
  // public  int nAnimation = 0;           // ID der ggw. Animation
  // public  boolean fAnimHelper = false;  // Hilfsflag bei Animation
  // private int nAnimStep = 0;            // ggw. Pos in Animation
  
  // Variablen fuer Zooming
    public int maxx;                      // X - Koordinate, bis zu der nicht gezoomt wird
    // (Vordergrund) bildabhaengig
    public float zoomf;                   // gibt an, wie stark gezoomt wird, wenn Figur in
    // den Hintergrund geht (bildabhaengig)
    // private static final int SLOWX = 14;  // Konstante, die angibt, wie sich die x - Abstaende
    // beim Zoomen veraendern
    private static final int SLOWY = 10;  // dsgl. fuer y - Richtung                                      
    public int defScale;                  // definiert maximale Groesse von Krabat bei x > maxx
    public int minx;                      // "Falschherum" - X - Koordinate, damit Scaling wieder stimmt...

    private boolean istMuellerSchonTot = false;
	
    // Initialisierung ////////////////////////////////////////////////////////////////

    public Mlynk2 (Start caller)
    {
	super (caller);

	krabat_back      = new GenericImage[9];

	krabat_stand_front_head = new GenericImage[2];
	krabat_stand_front_body = new GenericImage[1];
	krabat_stand_left_head  = new GenericImage[2];
	krabat_stand_left_body  = new GenericImage[1];
	krabat_stand_right_head = new GenericImage[2];
	krabat_stand_right_body = new GenericImage[1];
    
	krabat_talk_front_head = new GenericImage[5];
	krabat_talk_front_body = new GenericImage[3];
	krabat_talk_left_head  = new GenericImage[4];
	krabat_talk_left_body  = new GenericImage[3];
	krabat_talk_right_head = new GenericImage[4];
	krabat_talk_right_body = new GenericImage[3];
    
	InitImages ();

	Verhinderkopf = MAX_VERHINDERKOPF;
	Verhinderbody = MAX_VERHINDERBODY;
		
	float fWidth  = CWIDTH;
	float fHeight = CHEIGHT;
		
	scaleVerhaeltnisNormal = fWidth / fHeight;
		
	fWidth = COFFSET_STOCK;
		
	scaleVerhaeltnisStock = fWidth / fHeight;
    }
  
    // Bilder vorbereiten
    private void InitImages () 
    {
	krabat_back[0]  = getPicture ("gfx/anims/ml3.gif");
	krabat_back[1]  = getPicture ("gfx/anims/ml3-1.gif");
	krabat_back[2]  = getPicture ("gfx/anims/ml3-1.gif");
	krabat_back[3]  = getPicture ("gfx/anims/ml3-2.gif");
	krabat_back[4]  = getPicture ("gfx/anims/ml3-2.gif");
	krabat_back[5]  = getPicture ("gfx/anims/ml3-3.gif");
	krabat_back[6]  = getPicture ("gfx/anims/ml3-3.gif");
	krabat_back[7]  = getPicture ("gfx/anims/ml3-4.gif");
	krabat_back[8]  = getPicture ("gfx/anims/ml3-4.gif");

	krabat_stand_front_head[0]  = getPicture ("gfx/anims/ml1-h.gif");
	krabat_stand_front_head[1]  = getPicture ("gfx/anims/ml1-ha.gif");
	krabat_stand_front_body[0]  = getPicture ("gfx/anims/ml1-b.gif");

	krabat_stand_left_head[0]  = getPicture ("gfx/anims/ml2-h.gif");
	krabat_stand_left_head[1]  = getPicture ("gfx/anims/ml2-ha.gif");
	krabat_stand_left_body[0]  = getPicture ("gfx/anims/ml2-b.gif");

	krabat_stand_right_head[0]  = getPicture ("gfx/anims/ml4-h.gif");
	krabat_stand_right_head[1]  = getPicture ("gfx/anims/ml4-ha.gif");
	krabat_stand_right_body[0]  = getPicture ("gfx/anims/ml4-b.gif");

	krabat_talk_front_head[0]  = getPicture ("gfx/anims/ml1-h1.gif");
	krabat_talk_front_head[1]  = getPicture ("gfx/anims/ml1-h2.gif");
	krabat_talk_front_head[2]  = getPicture ("gfx/anims/ml1-h3.gif");
	krabat_talk_front_head[3]  = getPicture ("gfx/anims/ml1-h4.gif");
	krabat_talk_front_head[4]  = getPicture ("gfx/anims/ml1-h5.gif");

	krabat_talk_front_body[0]  = getPicture ("gfx/anims/ml1-b1.gif");
	krabat_talk_front_body[1]  = getPicture ("gfx/anims/ml1-b2.gif");
	krabat_talk_front_body[2]  = getPicture ("gfx/anims/ml1-b3.gif");

	krabat_talk_left_head[0]  = getPicture ("gfx/anims/ml2-h1.gif");
	krabat_talk_left_head[1]  = getPicture ("gfx/anims/ml2-h2.gif");
	krabat_talk_left_head[2]  = getPicture ("gfx/anims/ml2-h3.gif");
	// krabat_talk_left_head[3]  = getPicture ("gfx/anims/ml2-h4.gif");
	krabat_talk_left_head[3]  = getPicture ("gfx/anims/ml2-h5.gif");

	krabat_talk_left_body[0]  = getPicture ("gfx/anims/ml2-b1.gif");
	krabat_talk_left_body[1]  = getPicture ("gfx/anims/ml2-b2.gif");
	krabat_talk_left_body[2]  = getPicture ("gfx/anims/ml2-b3.gif");

	// krabat_talk_right_head[0]  = getPicture ("gfx/anims/ml4-h1.gif");
	krabat_talk_right_head[0]  = getPicture ("gfx/anims/ml4-h2.gif");
	krabat_talk_right_head[1]  = getPicture ("gfx/anims/ml4-h3.gif");
	krabat_talk_right_head[2]  = getPicture ("gfx/anims/ml4-h4.gif");
	krabat_talk_right_head[3]  = getPicture ("gfx/anims/ml4-h5.gif");

	krabat_talk_right_body[0]  = getPicture ("gfx/anims/ml4-b1.gif");
	krabat_talk_right_body[1]  = getPicture ("gfx/anims/ml4-b2.gif");
	krabat_talk_right_body[2]  = getPicture ("gfx/anims/ml4-b3.gif");

	krabat_hat_stock = getPicture ("gfx/anims/ml5-b.gif");
	krabat_gib_karte = getPicture ("gfx/anims/ml1-gibkarte.gif");

	krabat_hat_stock_andersrum = getPicture ("gfx/anims/ml5-c.gif");
    }


    // Laufen mit Mlynk ////////////////////////////////////////////////////////////////

  // Plokarka um einen Schritt weitersetzen
  // false = weiterlaufen, true = stehengebleibt
    public synchronized boolean Move ()
    {
	// Variablen uebernehmen (Threadsynchronisierung)
	horizontal = Thorizontal;
	walkto = Twalkto;
	direction_x = Tdirection_x;
	direction_y = Tdirection_y;
    
	if (horizontal == true)
	    // Horizontal laufen
	    {
		/*
		  // neuen Punkt ermitteln und setzen
		  VerschiebeX();
		  xps = txps;
		  yps = typs;
    	
		  // Animationsphase weiterschalten
		  anim_pos++;
		  if (anim_pos == 5) anim_pos = 1;
      
		  // Naechsten Schritt auf Gueltigkeit ueberpruefen
		  VerschiebeX();
      
		  // Ueberschreitung feststellen in X - Richtung
		  if (((walkto.x - (int) txps) * direction_x) <= 0)
		  {    	
		  // System.out.println("Ueberschreitung x! " + walkto.x + " " + walkto.y + " " + txps + " " + typs);
		  SetMlynkPos (walkto);
		  anim_pos = 0;
		  return true;
		  }	
		*/
	    }

	else
	    // Vertikal laufen
	    {
		// neuen Punkt ermitteln und setzen
		VerschiebeY();
		xps = txps;
		yps = typs;
    	
		// Animationsphase weiterschalten
		anim_pos++;
		if (anim_pos == 9) anim_pos = 1;
      
		// Naechsten Schritt auf Gueltigkeit ueberpruefen
		VerschiebeY();
    	
		// Ueberschreitung feststellen in Y - Richtung
		if (((walkto.y - (int) typs) * direction_y) <= 0)
		    {	
			// System.out.println("Ueberschreitung y! " + walkto.x + " " + walkto.y + " " + txps + " " + typs);
			SetMlynkPos (walkto);
			anim_pos = 0;
			return true;
		    }	
	    }
	return false;
    }

    // Horizontal - Positions - Verschieberoutine
    /*private void VerschiebeX ()
      {
      // Skalierungsfaktor holen
      int scale = getScale(((int) xps), ((int) yps));
    	
    // Zooming - Faktor beruecksichtigen in x - Richtung
    float horiz_dist = CHORIZ_DIST [anim_pos] - (scale / SLOWX);
    if (horiz_dist < 1) horiz_dist = 1;

    // Verschiebungsoffset berechnen (fuer schraege Bewegung)
    float z = 0;
    if (horiz_dist != 0) z = Math.abs (xps - walkto.x) / horiz_dist;
      
    typs = yps;
    if (z != 0) typs += direction_y * (Math.abs (yps - walkto.y) / z); 
	  
    txps = xps + (direction_x * horiz_dist);
    // System.out.println(xps + " " + txps + " " + yps + " " + typs);
    }	*/

    // Vertikal - Positions - Verschieberoutine
    private void VerschiebeY ()
    {
	// Skalierungsfaktor holen
	int scale = getScale(((int) xps), ((int) yps));
    	
    // Zooming - Faktor beruecksichtigen in y-Richtung
	float vert_dist = CVERT_DIST - (scale / SLOWY);
	if (vert_dist < 1) 
	    {
		vert_dist = 1;
		// hier kann noch eine Entscheidungsroutine hin, die je nach Animationsphase
		// und vert_distance ein Pixel erlaubt oder nicht
	    }
    
	// Verschiebungsoffset berechnen (fuer schraege Bewegung)
	float z = Math.abs(yps - walkto.y) / vert_dist;
      
	txps = xps;  
	if (z != 0) txps += direction_x * (Math.abs(xps - walkto.x) / z);
		
	typs = yps + (direction_y * vert_dist);
	// System.out.println(xps + " " + txps + " " + yps + " " + typs);
    }	
	
    // Vorbereitungen fuer das Laufen treffen und starten
    // Diese Routine wird nur im "MousePressed" - Event angesprungen
    public synchronized void MoveTo (GenericPoint aim)
    {
	int xricht, yricht;
	boolean horiz;
   
	// Lohnt es sich zu laufen ?
	/*int scale = getScale ((int) xps, (int) yps);
	  int lohnenx = CLOHNENX - (scale / 2);
	  int lohneny = CLOHNENY - (scale / 4);
	  if (lohnenx < 1) lohnenx = 1;
	  if (lohneny < 1) lohneny = 1;
	  if ((Math.abs (aim.x - ((int) xps)) < lohnenx) && 
	  (Math.abs (aim.y - ((int) yps)) < lohneny))
	  {
	  System.out.println("Lohnt sich nicht !");
	  anim_pos = 0;
	  return;
	  }*/

	// Laufrichtung ermitteln
	if (aim.x > ((int) xps) ) xricht = 1; else xricht = -1;
	if (aim.y > ((int) yps) ) yricht = 1; else yricht = -1;

	// Horizontal oder verikal laufen ?
	/*if (aim.x == ((int) xps) ) horiz = false;
	  else
	  {
	  // Winkel berechnen, den Krabat laufen soll
	  double yangle = Math.abs (aim.y - ((int) yps) );
	  double xangle = Math.abs (aim.x - ((int) xps) );
	  double angle = Math.atan (yangle / xangle);
	  // System.out.println ((angle * 180 / Math.PI) + " Grad");
	  if (angle > (22 * Math.PI / 180))  horiz = false;
	  else horiz = true;
	  }*/
    
	horiz = false;
    
	// Variablen an Move uebergeben
	Twalkto      = aim;
	Thorizontal  = horiz;
	Tdirection_x = xricht;
	Tdirection_y = yricht;

	if (anim_pos == 0) anim_pos = 1;       // Animationsimage bei Neubeginn initialis.
	// System.out.println("Animpos ist . " + anim_pos);
    }

    // Krabat an bestimmte Position setzen incl richtigem Zoomfaktor (Fuss-Koordinaten angegeben)
    public void SetMlynkPos (GenericPoint aim)
    {
	// point bezeichnet Fusskoordinaten
	// pos_x = aim.x;
	// pos_y = aim.y;

	xps = aim.x;        // Float - Variablen initialisieren
	yps = aim.y;

	//System.out.println("Setkrabatpos allgemein "+pos_x+" "+pos_y);
    }

    // Krabats Position ermitteln incl richtigem Zoomfaktor (Ausgabe der Fuss-Koordinaten)
    public GenericPoint GetMlynkPos ()
    {
	//System.out.println(" Aktuelle Pos : "+pos_x+" "+pos_y);
	return (new GenericPoint (((int) xps), ((int) yps)));
    }
  
    // Krabat - Animationen /////////////////////////////////////////////////////////////

  // je nach Laufrichtung Krabat zeichnen
    public void drawMlynk (GenericDrawingContext offGraph)
    {
	// je nach Richtung Sprite auswaehlen und zeichnen (hier aber nur Stehen , ausser back = laufen)
    	
	// Zufallscounter fuer Zwinkern
	int zuffZahl = (int) Math.round (Math.random () * 50);
    
	if (Zwinker == 1) Zwinker = 0;
	else
	    {
		if (zuffZahl > 45) Zwinker = 1;
	    }	

	// Override, wenn der Mueller gestorben ist
	if (istMuellerSchonTot == true) Zwinker = 0;
    	
	// fuer MaleIhn nur Richtung angeben, restlichen Variablen koennen dort ausgewertet werden
	if (horizontal == true)
	    {
		// nach links laufen
		if (direction_x  == -1) MaleIhn (offGraph, 9);
      
      // nach rechts laufen
		if (direction_x == 1) MaleIhn (offGraph, 3);
	    }
	else
	    {
		// Bei normaler Darstellung
		if (upsidedown == false)
		    {   
			// nach oben laufen
			if (direction_y  == -1) MaleIhn (offGraph, 12);
      
        // nach unten laufen
			if (direction_y == 1) MaleIhn (offGraph, 6);
		    }
		else
		    {
			// nach oben laufen
			/*if (direction_y  == -1) MaleIhn (offGraph, krabat_front[anim_pos]);
      
			  // nach unten laufen
			  if (direction_y == 1) MaleIhn (offGraph, krabat_back[anim_pos]);
			*/
		    }  
	    }
    }

    // Zwinkern verhindern 
    public void drawMlynkWithoutZwinkern (GenericDrawingContext g)
    {
	istMuellerSchonTot = true;  // gehackt, aber egal, brauchen den Mueller nicht wieder ab hier...
	drawMlynk (g);
    }

    // Hier hat der Mueller den Stock erhoben
    public void drawMlynkWithKij (GenericDrawingContext offGraph)
    {
	// Zufallscounter fuer Zwinkern
	int zuffZahl = (int) Math.round (Math.random () * 50);
    
	if (Zwinker == 1) Zwinker = 0;
	else
	    {
		if (zuffZahl > 45) Zwinker = 1;
	    }	
    
	MaleIhnMitStock (offGraph, 150);
    }
  
    // hier gibt er die Karte an Krabat
    public void drawMlynkWithKarte (GenericDrawingContext offGraph)
    {
  	MaleIhn (offGraph, 20);
    }
  
    // Lasse Krabat in eine bestimmte Richtung schauen (nach Uhrzeit!)
    public void SetFacing(int direction)
    {
	switch (direction)
	    {
	    case 3:
		horizontal=true;
		direction_x=1;
		break;
	    case 6:
		horizontal=false;
		direction_y=1;
		break;
	    case 9:
		horizontal=true;
		direction_x=-1;
		break;
	    case 12:
		horizontal=false;
		direction_y=-1;
		break;
	    default:
		System.out.println("Falsche Uhrzeit zum Witzereissen!");
	    }
    }

    // Richtung, in die Krabat schaut, ermitteln (wieder nach Uhrzeit)
    public int GetFacing()
    {
	int rgabe = 0;
	if (horizontal == true)
	    {
		if (direction_x == 1) rgabe = 3;
		else rgabe = 9;
	    }
	else
	    {
		if (direction_y == 1) rgabe = 6;
		else rgabe = 12;
	    }
	if (rgabe == 0)
	    {
		System.out.println("Fehler bei GetFacing !!!");
	    }
	return rgabe;
    }
  

    // Zeichne Mlynk beim Sprechen mit anderen Personen (normal)
    public void talkMlynk (GenericDrawingContext offGraph)
    {
	if ((--Verhinderkopf) < 1)
	    {
		Verhinderkopf = MAX_VERHINDERKOPF;
    	
		Head = (int) Math.round (Math.random () * 5);
		if (Head == 5) Head = 0;
	    }		
    
	if (Body == 3) Body = 0;
        
        if ((--Verhinderbody) < 1)
	    {
		Verhinderbody = MAX_VERHINDERBODY;
		Body = (int) Math.round (Math.random () * 3);
		if (Body == 3) Body = 0;
	    }
    
	int tFacing = GetFacing();
	if (tFacing == 12)
	    {
		System.out.println ("Nach hinten kann er nicht reden !!!");
		tFacing = 9;
	    }	
    
	// einen Kopf einsparen bei zur seite reden
	if ((tFacing != 6) && (Head == 4)) Head = 0;
    
	tFacing *= 10;
    
	MaleIhn (offGraph, tFacing);
    }
  
    // hat Stock immer nach links erhoben fuers Verzaubern
    public void talkMlynkWithKij (GenericDrawingContext offGraph)
    {
	// Hier muss nur der Kopf bewegt werden, sonst ni#ko
	if ((--Verhinderkopf) < 1)
	    {
		Verhinderkopf = MAX_VERHINDERKOPF;
    	
		Head = (int) Math.round (Math.random () * 4);
		if (Head == 4) Head = 0;
	    }		

	MaleIhnMitStock (offGraph, 180);
    } 

    public void talkMlynkWithKijAndersrum (GenericDrawingContext offGraph, boolean stockIstImmerOben)
    {
	// Kopf verschieben
	if ((--Verhinderkopf) < 1)
	    {
		Verhinderkopf = MAX_VERHINDERKOPF;
       		Head = (int) (Math.random () * 3.9);
	    }		
    
	// Koerper verschieben
	if ((--Verhinderbody) < 1)
	    {
		Verhinderbody = MAX_VERHINDERBODY;
		Body = (int) (Math.random () * 3.9);
	    }

        if (stockIstImmerOben == true)
        {
            Body = 3;
        }

	// Malen und gut
	MaleIhnMitStockAndersrum (offGraph);
    
    }

    public GenericPoint evalMlynkTalkPoint ()
    {
	// Hier Position des Textes berechnen
	Borderrect temp = MlynkRect ();
	GenericPoint tTalk = new GenericPoint ((temp.ru_point.x + temp.lo_point.x) / 2, temp.lo_point.y - 50);
	return tTalk;
    }
    
    // Hojnt beim Monolog (ohne Gestikulieren)
    /*public void describeHojnt(Graphics offGraph)
      {
      if (mainFrame.talkCount != 1)
      {
      int nTemp;
      do
      nTemp = (int) Math.round(Math.random()*6);
      while ((nTemp==3)||(nTemp==4)||(nTemp==6)||(nTemp==0));
      MaleIhn (offGraph, krabat_talk[nTemp]);
      }
      else drawHojnt (offGraph);
      }*/

    // Zooming-Variablen berechnen

    private int getLeftPos (int pox,int poy)
    {
	// Linke x-Koordinate = Fusspunkt - halbe Breite
	// + halbe Hoehendifferenz
	int helper = getScale(pox, poy);
    
	float fHelper = helper;
	fHelper *= scaleVerhaeltnisNormal;
    
	return (pox - ((CWIDTH - ((int) (fHelper))) / 2));
    }  

    // hier beachten, dass er einen Stock in der Hand haelt !!!!!!
    private int getLeftPosWithKij (int pox,int poy)
    {
	// Skalierungsfaktor holen
	int helper = getScale (pox, poy);
    
	float fHelper = helper;
	fHelper *= scaleVerhaeltnisStock;
    
	return (pox - (   (COFFSET_STOCK - ((int) (fHelper)))    / 2));
    }  

    private int getUpPos (int pox, int poy)
    {
	// obere y-Koordinate = untere y-Koordinate - konstante Hoehe 
	// + Hoehendifferenz
	int helper = getScale (pox, poy);
	return (poy - CHEIGHT + helper);
    }

    // fuer Debugging public - wird wieder private !!!
    public int getScale (int pox, int poy)
    {
    
	// Hier kann override eingeschaltet werden (F7/F8)
	// return mainFrame.override;
    
	// Ermittlung der Hoehendifferenz beim Zooming
	if (upsidedown == false)
	    {
		// normale Berechnung	
		float helper = (maxx - poy) / zoomf;
		if (helper < 0) helper = 0;
		helper += defScale;
		return ((int) helper);
	    }
	else
	    {
		// Berechnung bei "upsidedown" - Berg/Tallauf
		float help2 = (poy - minx) / zoomf;
		if (help2 < 0) help2 = 0;
		help2 += defScale;
		// System.out.println (minx + " + " + poy + " und " + zoomf + " ergeben " + help2); 
		return ((int) help2);
	    }
    }  

    // Clipping - Region vor Zeichnen von Krabat setzen
    private void KrabatClip(GenericDrawingContext g, int xx, int yy)
    {
	// Links - oben - Korrdinaten ermitteln
	int x = getLeftPos(xx,yy); 
	int y = getUpPos  (xx,yy);
	// System.out.println(xx +  " " + x);
    
    // Breite und Hoehe ermitteln
	int xd = 2 * (xx - x);
	int yd = yy - y;     
	g.setClip (x, y, xd, yd);
    
	// Fuer Debugging ClipRectangle zeichnen 
	// g.setColor(Color.white);
	// g.drawRect(x, y, xd - 1, yd - 1);
  	// System.out.println(x + " " + y + " " + xd + " " + yd);
    }  

    // Clipping - Region vor Zeichnen von Krabat setzen, hier mit Stock in der Hand
    private void KrabatClipWithKij (GenericDrawingContext g, int xx, int yy)
    {
	// Links - oben - Korrdinaten ermitteln
	int x = getLeftPosWithKij (xx,yy); 
	int y = getUpPos  (xx,yy);
	// System.out.println(xx +  " " + x);
    
    // Breite und Hoehe ermitteln
	int xd = 2 * (xx - x);
	int yd = yy - y;     
	g.setClip (x, y, xd, yd);
    
	// Fuer Debugging ClipRectangle zeichnen 
	// g.setColor(Color.white);
	// g.drawRect(x, y, xd - 1, yd - 1);
  	// System.out.println(x + " " + y + " " + xd + " " + yd);
    }  

    // Clipping-Region wird gross gesetzt, denn man weiss ja nie, ob er den Stock in der Hand haelt oder nicht
    private void KrabatClipWithKijAndersrum (GenericDrawingContext g, int xx, int yy)
    {
	// Links - oben - Korrdinaten ermitteln
	int x = getLeftPos (xx,yy); 
	int y = getUpPos   (xx,yy);
	// System.out.println(xx +  " " + x);
    
	// Skalierungsfaktor holen
	int helper = getScale (xx, yy);
    
	float fHelper = helper;
	fHelper *= scaleVerhaeltnisStock;
    
	// Breite und Hoehe ermitteln
	int xd = x + (CWIDTH_STOCK - ((int) (fHelper)));
	int yd = yy - y;   
  
	g.setClip (x, y, xd, yd);
    
	// Fuer Debugging ClipRectangle zeichnen 
	// g.setColor(Color.white);
	// g.drawRect(x, y, xd - 1, yd - 1);
  	// System.out.println(x + " " + y + " " + xd + " " + yd);
    }  

    // Routine, die BorderRect zurueckgibt, wo sich Krabat gerade befindet
    public Borderrect MlynkRect()
    {
	int x = getLeftPos ( ((int) xps), ((int) yps) );
	int y = getUpPos   ( ((int) xps), ((int) yps) );
	int xd = (2 * ( ((int) xps) - x)) + x;
	int yd = ((int) yps);
	// System.out.println(x + " " + y + " " + xd + " " + yd);
	return (new Borderrect (x, y, xd, yd)); 
    }		
  
    // Routine, die BorderRect zurueckgibt, wo sich Krabat gerade befindet, wenn er den Stock hat !!!
    public Borderrect MlynkRectMitStock()
    {
	int x = getLeftPosWithKij ( ((int) xps), ((int) yps) );
	int y = getUpPos   ( ((int) xps), ((int) yps) );
	int xd = (2 * ( ((int) xps) - x)) + x;
	int yd = ((int) yps);
	// System.out.println(x + " " + y + " " + xd + " " + yd);
	return (new Borderrect (x, y, xd, yd)); 
    }

    public Borderrect MlynkRectMitStockAndersrum ()
    {
	// Links - oben - Korrdinaten ermitteln
	int x = getLeftPos ( ((int) xps), ((int) yps) ); 
	int y = getUpPos   ( ((int) xps), ((int) yps) );
	// System.out.println(xx +  " " + x);
    
	// Skalierungsfaktor holen
	int helper = getScale ( ((int) xps), ((int) yps) );
    
	float fHelper = helper;
	fHelper *= scaleVerhaeltnisStock;
    
	// Breite und Hoehe ermitteln
	int xd = x + (CWIDTH_STOCK - ((int) (fHelper)));
	int yd = ((int) yps) ;   

	return (new Borderrect (x, y, xd, yd)); 
    }
  
    // ganz normales Darstellen
    private void MaleIhn (GenericDrawingContext g, int Richtung)
    {
	// Clipping - Region setzen
	KrabatClip(g, ((int) xps), ((int) yps));
    
    // Groesse und Position der Figur berechnen
	int left  = getLeftPos ( ((int) xps), ((int) yps) );
	int up    = getUpPos   ( ((int) xps), ((int) yps) );
	int scale = getScale   ( ((int) xps), ((int) yps) );
    
	// hier die Breiten und Hoehenscalings fuer Kopf und Body berechnen
	float fScale = scale;
	float fBodyoffset = BODYOFFSET;
	float fHoehe = CHEIGHT;
    
	float fScaleY = fScale * scaleVerhaeltnisNormal;
	int Koerperbreite = CWIDTH - ((int) fScaleY);
	int Kopfhoehe     = (int) (fBodyoffset - (fScale * (fBodyoffset / fHoehe)));
	int Koerperhoehe  = (int) (fHoehe - scale - Kopfhoehe);
    
	// System.out.println ("Mueller ist " + Koerperbreite + " breit und Kopf " + Kopfhoehe + " und Body " + Koerperhoehe + " hoch.");
    
    // Figur zeichnen
	switch (Richtung)
	    {
	    case 3: // nach rechts gehen
		g.drawImage (krabat_stand_right_head[Zwinker], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_stand_right_body[0]      , left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	  
	    case 6: // nach vorn gehen  
		g.drawImage (krabat_stand_front_head[Zwinker], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_stand_front_body[0]      , left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	  
	    case 9: // nach links gehen
		g.drawImage (krabat_stand_left_head[Zwinker], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_stand_left_body[0]      , left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	  
	    case 12: // nach hinten gehen
		g.drawImage (krabat_back[anim_pos], left, up, Koerperbreite, Kopfhoehe + Koerperhoehe, null);
		break;    
    
	    case 20: // Karte geben, nach vorn schauen
		g.drawImage (krabat_stand_front_head[Zwinker], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_gib_karte                , left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	
	    case 30: // nach rechts reden
		g.drawImage (krabat_talk_right_head[Head], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_talk_right_body[Body], left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	  
	    case 60: // nach vorn reden  
		g.drawImage (krabat_talk_front_head[Head], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_talk_front_body[Body], left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
    	  
	    case 90: // nach links reden
		g.drawImage (krabat_talk_left_head[Head], left, up            , Koerperbreite, Kopfhoehe   , null);
		g.drawImage (krabat_talk_left_body[Body], left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
		break;
      
	    default: 
		System.out.println ("Falsche Richtung fuer Mueller angegeben !");
		break;
	    }
    }

    // Stock nach links und immer oben
    private void MaleIhnMitStock (GenericDrawingContext g, int Richtung)
    {
	// Clipping - Region setzen
	KrabatClipWithKij(g, ((int) xps), ((int) yps));
    
    // Groesse und Position der Figur berechnen
	int left  = getLeftPosWithKij ( ((int) xps), ((int) yps) );
	int up    = getUpPos   ( ((int) xps), ((int) yps) );
	int scale = getScale   ( ((int) xps), ((int) yps) );
    
	// hier die Breiten und Hoehenscalings fuer Kopf und Body berechnen
	float fScale = scale;
	float fBodyoffset = BODYOFFSET;
	float fHoehe = CHEIGHT;
    
	float fScaleY = fScale * scaleVerhaeltnisNormal;
	int Koerperbreite = CWIDTH - ((int) fScaleY);
	int Kopfhoehe     = (int) (fBodyoffset - (fScale * (fBodyoffset / fHoehe)));
	int Koerperhoehe  = (int) (fHoehe - scale - Kopfhoehe);
    
	// hier die Extrawurst, wenn er den Stock hat
	int KoerperbreiteMitStock = CWIDTH_STOCK - scale;
	int OffsetMitStock        = getLeftPos ( ((int) xps), ((int) yps) ) - getLeftPosWithKij ( ((int) xps), ((int) yps) );
    
    // System.out.println ("Mueller ist " + Koerperbreite + " breit und Kopf " + Kopfhoehe + " und Body " + Koerperhoehe + " hoch.");
    
	// System.out.println ("Zoomfaktor : " + scale + " Offset : " + OffsetMitStock);
    
	// Figur zeichnen
	switch (Richtung)
	    {
	    case 150: // nach links schauen mit erhobenem Stock
		g.drawImage (krabat_stand_left_head[Zwinker], left + OffsetMitStock, up            , Koerperbreite        , Kopfhoehe   , null);
		g.drawImage (krabat_hat_stock               , left                 , up + Kopfhoehe, KoerperbreiteMitStock, Koerperhoehe, null);
		break;
      
	    case 180: // nach links reden mit erhobenem Stock
		g.drawImage (krabat_talk_left_head[Head], left + OffsetMitStock, up            , Koerperbreite        , Kopfhoehe   , null);
		g.drawImage (krabat_hat_stock           , left                 , up + Kopfhoehe, KoerperbreiteMitStock, Koerperhoehe, null);
		break;
      
	    default: 
		System.out.println ("Falsche Richtung fuer Mueller angegeben !");
		break;
	    }
    }

    // hier soll er den Stock nur ab und zu hochhalten
    private void MaleIhnMitStockAndersrum (GenericDrawingContext g)
    {
	// Clipping - Region setzen
	KrabatClipWithKijAndersrum (g, ((int) xps), ((int) yps));
    
	// Groesse und Position der Figur berechnen
	int left  = getLeftPos ( ((int) xps), ((int) yps) );
	int up    = getUpPos   ( ((int) xps), ((int) yps) );
	int scale = getScale   ( ((int) xps), ((int) yps) );
    
	// hier die Breiten und Hoehenscalings fuer Kopf und Body berechnen
	float fScale = scale;
	float fBodyoffset = BODYOFFSET;
	float fHoehe = CHEIGHT;
    
	float fScaleY = fScale * scaleVerhaeltnisNormal;
	int Koerperbreite = CWIDTH - ((int) fScaleY);
	int Kopfhoehe     = (int) (fBodyoffset - (fScale * (fBodyoffset / fHoehe)));
	int Koerperhoehe  = (int) (fHoehe - scale - Kopfhoehe);
    
	// hier die Extrawurst, wenn er den Stock hat
	int KoerperbreiteMitStock = CWIDTH_STOCK - scale;
	// int OffsetMitStock        = getLeftPos ( ((int) xps), ((int) yps) ) - getLeftPosWithKij ( ((int) xps), ((int) yps) );
    
	// System.out.println ("Mueller ist " + Koerperbreite + " breit und Kopf " + Kopfhoehe + " und Body " + Koerperhoehe + " hoch.");
    
	// System.out.println ("Zoomfaktor : " + scale + " Offset : " + OffsetMitStock);
    
	// Figur zeichnen
	g.drawImage (krabat_talk_right_head[Head], left, up, Koerperbreite, Kopfhoehe, null);

	if (Body == 3)
	    g.drawImage (krabat_hat_stock_andersrum, left, up + Kopfhoehe, KoerperbreiteMitStock, Koerperhoehe, null);
	else
	    g.drawImage (krabat_talk_right_body[Body], left, up + Kopfhoehe, Koerperbreite, Koerperhoehe, null);
  
    }		
}