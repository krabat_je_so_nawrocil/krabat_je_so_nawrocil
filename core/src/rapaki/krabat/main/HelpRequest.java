/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.main;

import rapaki.krabat.Start;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageObserver;

public class HelpRequest extends Mainloc 
{
    private GenericImage background;

    private static final GenericPoint pOkUnten    = new GenericPoint(500, 380);
    
    private Borderrect brOk;

    private int Cursorform = 200;

    private int menuitem = 0;
    private int olditem  = 0;
  
    private boolean Paintcall = false;
    
    private static final int ABSTAND = 50;
  
    // Initialisierung ////////////////////////////////////////////////////////

    private GenericImageObserver observer = null;
    
    // Instanz von dieser Location erzeugen
    public HelpRequest (Start caller) 
    {
    	super (caller);
    	mainFrame.Freeze (true);

    	mainFrame.NoPaint (true);
    
    	brOk = new Borderrect(pOkUnten.x, pOkUnten.y, pOkUnten.x + 40, pOkUnten.y + 20);
    	
    	InitImages ();
    	
    	mainFrame.Freeze (false);
    }
  
    // Bilder vorbereiten
    private void InitImages() 
    {
		background  = getPicture ("gfx/mainmenu/background2.gif");
    }
    
    // Paint-Routine dieser Location //////////////////////////////////////////

    public void paintLocation (GenericDrawingContext g)
    {

    	// Credits-Background zeichnen
    	if (mainFrame.Clipset == false)
	    {
    		int yKoord = 80;
    	    mainFrame.scrollx = 0;
    	    mainFrame.scrolly = 0;
    		
    		mainFrame.Clipset = true;
    		g.setClip (0, 0, 1280, 480);
    		Cursorform = 200;
    		Paintcall = true;
    		evalMouseMoveEvent (mainFrame.Mousepoint);
	    
    		// alles loeschen und neuzeichnen - hier die texte, die sich nur bei "Clipset = false" aendern (Mouseclick)
    		g.drawImage (background, mainFrame.scrollx, 0, null);

    		mainFrame.ifont.drawString (g, "Kak d#de dale? Pomhaj#ce nam!", 80, yKoord, 0xffff0000);
    		yKoord += ABSTAND;
    		mainFrame.ifont.drawString (g, "Wie geht#Gs weiter? Helft uns!", 80, yKoord, 0xffff0000);
    		yKoord += ABSTAND;
    		mainFrame.ifont.drawString (g, "What#Gs next? Help us!", 80, yKoord, 0xffff0000);
    		yKoord += ABSTAND;
    		
    		mainFrame.ifont.drawString (g, "OK", pOkUnten.x + mainFrame.scrollx, mainFrame.scrolly + pOkUnten.y, 0xff800000);
	    }
    	
    	// Wenn noetig, dann highlight aufheben!!!
    	switch (olditem)
	    {
	    	case 0: 
	    		break;
	    	case 3:
	    		mainFrame.ifont.drawString (g, "OK", pOkUnten.x + mainFrame.scrollx, mainFrame.scrolly + pOkUnten.y, 0xff800000);
	    		break;
	    	default: 
	    		System.out.println("Falsches Menu-Item zum abdunkeln!!!");
	    }  
     
    	if (olditem != 0) olditem = 0;

    	// Wenn noetig, dann highlighten!!!
    	switch (menuitem)
	    {
	    	case 0: 
	    		break;
	    	case 3:
	    		mainFrame.ifont.drawString (g, "OK", pOkUnten.x + mainFrame.scrollx, mainFrame.scrolly + pOkUnten.y, 0xffff0000);
	    		break;
	    	default: 
	    		System.out.println("Falsches Menu-Item!!!");
	    }  

    	if (menuitem != 0) olditem = menuitem;
    }  

    // Mouse-Auswertung dieser Location ///////////////////////////////////////

    public GenericMousePressAction evalMouseEventFeedback (GenericMouseEvent e)
    {
    	GenericPoint pTemp = e.getPoint ();
    
    	if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
	    {   
    		if (brOk.IsPointInRect(pTemp) == true) {
    			mainFrame.browserSpawner.spawnBrowser("http://sourceforge.net/projects/krabat");
    			System.exit(0);
    			return GenericMousePressAction.MOUSE_PRESS_ACTION_LEFT_ITEM;
    		}
	    }
		else
	    {
			// rechte Maustaste, ignorieren
	    }
    	return GenericMousePressAction.MOUSE_PRESS_ACTION_NONE;
    }

    public void evalMouseMoveEvent (GenericPoint pTemp)
    {
    	if (Cursorform != 0)
	    {
    		Cursorform = 0;
    		mainFrame.setCursor (mainFrame.Normal);
	    }		

    	// Highlight im Menue festlegen
    	menuitem = 0;
    	if (brOk.IsPointInRect (pTemp) == true) menuitem = 3;
    	
    
    	// wenn noetig , dann Neuzeichnen!
    	if (Paintcall == true)
	    {
    		Paintcall = false;
    		return;
	    }  
    	if (menuitem != olditem) mainFrame.repaint();
    }

    public void evalMouseExitEvent (GenericMouseEvent e)
    {
    	menuitem = 0;
    	mainFrame.repaint();
    }  
  
    // Key - Auswertung dieser Location /////////////////////////////////

    public GenericKeyAction evalKeyEventFeedback (GenericKeyEvent e)
    {
    	// currently no keys
    	return GenericKeyAction.KEY_ACTION_NONE;
    }  
}
