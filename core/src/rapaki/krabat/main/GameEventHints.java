package rapaki.krabat.main;

public class GameEventHints {

	public static final int GAME_EVENT_HINT_EXIT       = 1;
	public static final int GAME_EVENT_HINT_OPEN       = 2;
	public static final int GAME_EVENT_HINT_SAVE       = 4;
	public static final int GAME_EVENT_HINT_MENU       = 8;
	public static final int GAME_EVENT_HINT_ESCAPE     = 16;
	public static final int GAME_EVENT_HINT_HOTSPOT    = 32;
	public static final int GAME_EVENT_HINT_DICTIONARY = 64;
	public static final int GAME_EVENT_HINT_ZOOM       = 128;
	
	private int gameEventHintMask;
	
	public GameEventHints() {
		gameEventHintMask = 0;
	}
	
	public void addHints(int hints) {
		gameEventHintMask |= hints;
	}
	
	public void removeHints(int hints) {
		gameEventHintMask &= ~hints;
	}
	
	public void removeAllHints() {
		gameEventHintMask = 0;
	}
	
	public int getCurrentHints() {
		return gameEventHintMask;
	}
}
