package rapaki.krabat.main;

public class GenericMousePressAction {

	public static GenericMousePressAction MOUSE_PRESS_ACTION_NONE = new GenericMousePressAction(0);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_LEFT_ARROW = new GenericMousePressAction(1);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_LEFT_ITEM = new GenericMousePressAction(2);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_LEFT_ITEM_COMBINE = new GenericMousePressAction(3);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_RIGHT_DROPITEM = new GenericMousePressAction(4);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_LEFT_WALK = new GenericMousePressAction(5);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_LEFT_ARROW_JUMP = new GenericMousePressAction(6);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_RIGHT_INVENTORY = new GenericMousePressAction(7);
	
	public static GenericMousePressAction MOUSE_PRESS_ACTION_RIGHT_USE = new GenericMousePressAction(8);
	
	private final int code;
	
	private GenericMousePressAction(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
