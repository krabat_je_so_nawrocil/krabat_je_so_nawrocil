package rapaki.krabat.main;

import rapaki.krabat.Start;

public abstract class MainlocTransitional extends Mainloc {

	public MainlocTransitional(Start caller) {
		super(caller);
	}
	
    public MainlocTransitional(Start caller, int idLocation)
    {
    	super(caller, idLocation);
    }
    
	public GenericMousePressAction evalMouseEventFeedback(GenericMouseEvent e) {
		evalMouseEvent(e);
		return GenericMousePressAction.MOUSE_PRESS_ACTION_NONE;
	}

	public GenericKeyAction evalKeyEventFeedback(GenericKeyEvent e) {
		evalKeyEvent(e);
		return GenericKeyAction.KEY_ACTION_NONE;
	}
	
	abstract public void evalMouseEvent(GenericMouseEvent e);
	abstract public void evalKeyEvent(GenericKeyEvent e);
}
