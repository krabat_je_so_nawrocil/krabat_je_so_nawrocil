package rapaki.krabat.main;

public class GenericKeyAction {

	public static final GenericKeyAction KEY_ACTION_NONE = new GenericKeyAction(0);

	public static final GenericKeyAction KEY_ACTION_HANDLED = new GenericKeyAction(1);

	private final int code;
	
	private GenericKeyAction(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
