/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.locations3;

import rapaki.krabat.Start;
import rapaki.krabat.main.GenericInputEvent;
import rapaki.krabat.main.GenericKeyEvent;
import rapaki.krabat.main.GenericMouseEvent;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.GenericRectangle;
import rapaki.krabat.main.Mainloc;
import rapaki.krabat.main.Borderrect;
import rapaki.krabat.main.Bordertrapez;
import rapaki.krabat.main.MainlocTransitional;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.anims.KrabatWerfen;
import rapaki.krabat.sound.BackgroundMusicPlayer;

public class Haska extends MainlocTransitional
{
    private GenericImage background, ohneSeil;
    private GenericImage[] tropfen;
    private boolean switchanim = false;
    private int weinCount = 0;

    private KrabatWerfen krabatWerfen;

    private boolean krabatWirft = false;

    private boolean setAnim = false;

    private boolean schnauzeWein = false;

    // Konstanten - Rects
    private static final Borderrect ausgangKuchnja 
        = new Borderrect (421, 275, 463, 391);
    private static final Borderrect ausgangStadt
        = new Borderrect (330, 254, 370, 363);
    private static final Borderrect fenster
        = new Borderrect (33, 11, 110, 127);
    private static final Borderrect wino
        = new Borderrect (452, 396, 509, 452);
   // private static final borderrect komedHaus
   //     = new borderrect (115, 135, 204, 365);
    private static final Borderrect seilHaken
        = new Borderrect (60, 132, 90, 425);
  
    // Konstante Points
    private static final GenericPoint pExitKuchnja = new GenericPoint (400, 420);
    private static final GenericPoint pFenster     = new GenericPoint (170, 450);
    private static final GenericPoint pExitStadt   = new GenericPoint (320, 367);
    private static final GenericPoint pWino        = new GenericPoint (438, 460);
    private static final GenericPoint pAmHaken     = new GenericPoint (116, 464);
  
    // Konstante ints
    private static final int fKuchnja      = 3;
    private static final int fFenster      = 9;
    private static final int fWino         = 3;
    private static final int fEnterhaken   = 9;

    private int Counter = 0;

    // Initialisierung ////////////////////////////////////////////////////////

    // Instanz von dieser Location erzeugen
    public Haska (Start caller,int oldLocation) 
    {
        super (caller, 121);
        mainFrame.Freeze (true);
    
        mainFrame.CheckKrabat ();

        BackgroundMusicPlayer.getInstance ().stop (); 
        tropfen = new GenericImage [9];

        krabatWerfen = new KrabatWerfen (mainFrame);

        mainFrame.krabat.maxx = 424;
        mainFrame.krabat.zoomf = 1.48f;
        mainFrame.krabat.defScale = -50;
  	
        krabatWerfen.maxx = mainFrame.krabat.maxx;
        krabatWerfen.zoomf = mainFrame.krabat.zoomf;
        krabatWerfen.defScale = mainFrame.krabat.defScale;

        InitLocation (oldLocation);
        mainFrame.Freeze (false);
    }
  
    // Gegend intialisieren (Grenzen u.s.w.)
    private void InitLocation (int oldLocation)
    {
        // Grenzen setzen
        mainFrame.wegGeher.vBorders.removeAllElements ();
        mainFrame.wegGeher.vBorders.addElement 
            (new Bordertrapez (234, 320, 193, 370, 367, 419));
        mainFrame.wegGeher.vBorders.addElement 
            (new Bordertrapez (193, 400, 145, 443, 420, 479));
       
        mainFrame.wegSucher.ClearMatrix (2);

        mainFrame.wegSucher.PosVerbinden (0, 1);

        InitImages();
        switch (oldLocation)
            {
            case 0: 
                // Einsprung fuer Load
                break;
            case 120: // von Kuchnja aus
            case 132: // von Kuchnjaopen dasselbe
                mainFrame.krabat.SetKrabatPos (new GenericPoint (400, 420));
                mainFrame.krabat.SetFacing (9);
                break;
            case 122: // von Spaniska aus
                mainFrame.krabat.SetKrabatPos (new GenericPoint (180, 460));
                mainFrame.krabat.SetFacing (3);
                if (mainFrame.Actions[519] == false)
                    {
                        setAnim = true;
                        mainFrame.Actions[519] = true;
                    }
                break;
            case 126: // von Murja aus
                mainFrame.krabat.SetKrabatPos (new GenericPoint (322, 370));
                mainFrame.krabat.SetFacing (9);
                break;
            }    
    }

    // Bilder vorbereiten
    private void InitImages() 
    {
        background = getPicture ("gfx-dd/haska/haska.gif");
        ohneSeil   = getPicture ("gfx-dd/haska/haska2.gif");

        tropfen[0] = getPicture ("gfx-dd/haska/wino0.gif");
        tropfen[1] = getPicture ("gfx-dd/haska/wino1.gif");
        tropfen[2] = getPicture ("gfx-dd/haska/wino2.gif");
        tropfen[3] = getPicture ("gfx-dd/haska/wino3.gif");
        tropfen[4] = getPicture ("gfx-dd/haska/wino4.gif");
        tropfen[5] = getPicture ("gfx-dd/haska/wino5.gif");
        tropfen[6] = getPicture ("gfx-dd/haska/wino6.gif");
        tropfen[7] = getPicture ("gfx-dd/haska/wino7.gif");
        tropfen[8] = getPicture ("gfx-dd/haska/wino8.gif");

        loadPicture();
    }

    // Paint-Routine dieser Location //////////////////////////////////////////

    public void paintLocation (GenericDrawingContext g)
    {

        // Clipping -Region initialisieren
        if (mainFrame.Clipset == false)
            {
                mainFrame.scrollx = 0;
                mainFrame.scrolly = 0;
                Cursorform = 200;
                if (setAnim == true) mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.Clipset = true;
                g.setClip(0,0,644,484);
                mainFrame.isAnim = true;
            }

        // Hintergrund und Krabat zeichnen
        g.drawImage (background, 0, 0, null);
    
        // Seil uebermalen, falls noch keins da
        if (mainFrame.Actions[519] == false) 
            {
                g.drawImage (ohneSeil, 53, 111, null);
            }

        // Anim (Weintropfen) zeichnen, da stets im Hintergrund
        if (mainFrame.isAnim == true) 
            {
                switchanim = ! (switchanim);
                if (switchanim == true) 
                    {
                        weinCount++;
			evalSound (weinCount);
			if (weinCount == 13) weinCount = 0;
                    }		
                g.setClip (447, 436, 11, 15);
                if (weinCount < 9) g.drawImage (tropfen[weinCount], 448, 437, null);
                else							 g.drawImage (tropfen[0], 448, 437, null);
            }  	
    
        // Debugging - Zeichnen der Laufrechtecke
        // mainFrame.showrect.Zeichne(g, mainFrame.wegGeher.vBorders);
  	
        mainFrame.wegGeher.GeheWeg ();
    
        // hier die Extrawurst fuer den Hakenwurf
        if (krabatWirft == true)
            {
                krabatWirft = krabatWerfen.drawKrabat (g, mainFrame.krabat.GetKrabatPos ());
            }
        else
            {
                // Animation??
                if (mainFrame.krabat.nAnimation != 0)
                    { 
                        mainFrame.krabat.DoAnimation (g);
                        
                        // Cursorruecksetzung nach Animationsende
                        if (mainFrame.krabat.nAnimation == 0) evalMouseMoveEvent (mainFrame.Mousepoint);
                    }  
                else
                    {
                        if ((mainFrame.talkCount > 0) && (TalkPerson != 0))
                            {
                                // beim Reden
                                switch (TalkPerson)
                                    {
                                    case 1:
                                        // Krabat spricht gestikulierend
                                        mainFrame.krabat.talkKrabat (g);
                                        break;
                                    case 3:
                                        // Krabat spricht im Monolog
                                        mainFrame.krabat.describeKrabat (g);
                                        break;
                                    default:
                                        // steht Krabat nur da
                                        mainFrame.krabat.drawKrabat (g);
                                        break;
                                    }    
                            }
                        // Rumstehen oder Laufen
                        else mainFrame.krabat.drawKrabat (g);
                    }  
            }    

        // Steht Krabat hinter einem Gegenstand ? Koordinaten noch mal checken !!!
        // GenericPoint pKrTemp = mainFrame.krabat.GetKrabatPos ();

        // hinter weiden2 (nur Clipping - Region wird neugezeichnet)
        /*if (weiden2Rect.IsPointInRect (pKrTemp) == true)
          {
          g.drawImage (weiden2, 84, 221, null);
          }*/

        // sonst noch was zu tun ?
        if  (outputText != "")
            {
                // Textausgabe
                GenericRectangle my;
                my = g.getClipBounds();
                g.setClip (0, 0, 644, 484);
                mainFrame.ifont.drawString (g, outputText, outputTextPos.x, outputTextPos.y, FarbenArray[TalkPerson]);
                g.setClip( (int) my.getX(), (int) my.getY(), (int) my.getWidth(), (int) my.getHeight()); 
            }

        // Redeschleife herunterzaehlen und Neuzeichnen ermoeglichen
        if (mainFrame.talkCount > 0)
            {
                -- mainFrame.talkCount;
                if (mainFrame.talkCount <= 1)
                    {
                        mainFrame.Clipset = false;
                        outputText = "";
                        TalkPerson = 0;
                    }
            }  

        if ((TalkPause > 0) && (mainFrame.talkCount < 1)) TalkPause--;

        // wenn automatische Anim, dann jetzt beginnen
        if (setAnim == true)
            {
                mainFrame.krabat.StopWalking();
                setAnim = false;
                nextActionID = 27;
            }

        // Gibt es was zu tun ?
        if ((nextActionID != 0) && (TalkPause < 1) && (mainFrame.talkCount < 1)) DoAction ();
    }


    // Mouse-Auswertung dieser Location ///////////////////////////////////////

    public void evalMouseEvent (GenericMouseEvent e)
    {
        GenericPoint pTemp = e.getPoint ();
        if (mainFrame.talkCount != 0) mainFrame.Clipset = false;
        if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
        outputText="";

        // Wenn in Animation, dann normales Gameplay aussetzen
        if (mainFrame.fPlayAnim == true)
            {
                return;
            }
    
        // Wenn Krabat - Animation, dann normales Gameplay aussetzen
        if (mainFrame.krabat.nAnimation != 0)
            {
                return;
            }    

        // wenn InventarCursor, dann anders reagieren
        if (mainFrame.invCursor == true)
            {
                // linker Maustaste
                if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
                    {
			nextActionID = 0;

                        Borderrect tmp = mainFrame.krabat.KrabatRect();

                        // Aktion, wenn Krabat angeclickt wurde
                        if (tmp.IsPointInRect (pTemp) == true)
                            {
                                nextActionID = 500 + mainFrame.whatItem;
                                mainFrame.repaint();
                                return;
                            }	

                        // Enterhaken mit Fenster benutzen
                        if (fenster.IsPointInRect (pTemp) == true) 
                            {
                                switch (mainFrame.whatItem)
                                    {
                                    case 39: // kotwica + lajna
                                        nextActionID = 20;
                                        break;
                                    default:
	    				nextActionID = 155;
	    				break;
                                    }		  	
                                pTemp = pFenster;
                            }
        
                        // Helm mit wein fuellen
                        if (wino.IsPointInRect (pTemp) == true) 
                            {
                                switch (mainFrame.whatItem)
                                    {
                                    case 43: // Friedhelm
                                        nextActionID = 30;
                                        break;
                                    case 53: // drasta
                                        nextActionID = 200;
                                        break;
                                    case 52: // wosusk
                                        nextActionID = 210;
                                        break;
                                    default:
	    				nextActionID = 150;
	    				break;
                                    }			      	
                                pTemp = pWino;
                            }
        
                        // Enterhaken am Haus Ausreden
                        if ((seilHaken.IsPointInRect (pTemp) == true) && (mainFrame.Actions[519] == true)) 
                            {
                                pTemp = pFenster;
                                nextActionID = 160;
                            }

                        // wenn nichts anderes gewaehlt, dann nur hinlaufen
                        mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
                        mainFrame.repaint();
                    }
      
                // rechte Maustaste
                else
                    {
                        // grundsaetzlich Gegenstand wieder ablegen
                        mainFrame.invCursor = false;
                        evalMouseMoveEvent (mainFrame.Mousepoint);
                        nextActionID = 0;
                        mainFrame.krabat.StopWalking();
                        mainFrame.repaint();
                        return;
                    }  
            }

        // normaler Cursor, normale Reaktion
        else
            {
                if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
                    {   
                        // linke Maustaste
                        nextActionID = 0;

                        // zu Ostansicht gehen ?
                        if (ausgangStadt.IsPointInRect (pTemp) == true)
                            { 
                                nextActionID = 102;
                                GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
                                // Wenn nahe am Ausgang, dann "gerade" verlassen
                                if (ausgangStadt.IsPointInRect (kt) == false)
                                    {
                                        pTemp = pExitStadt;
                                    }
                                else
                                    {
                                        pTemp = new GenericPoint (pExitStadt.x, kt.y);
                                    }
            
                                if (mainFrame.dClick == true)
                                    {
                                        mainFrame.krabat.StopWalking();
                                        mainFrame.repaint();
                                        return;
                                    }  
                            }

                        // zu Kuchnja gehen ? - nicht reingehen -> Ausreden
                        if (ausgangKuchnja.IsPointInRect (pTemp) == true)
                            { 
                                nextActionID = 100;
                                GenericPoint kt = mainFrame.krabat.GetKrabatPos();
				  
                                // gerade verlassen, wenn nahe
                                // Wenn nahe am Ausgang, dann "gerade" verlassen
                                if (ausgangKuchnja.IsPointInRect (kt) == false)
                                    {
                                        pTemp = pExitKuchnja;
                                    }
                                else
                                    {
                                        pTemp = new GenericPoint (pExitKuchnja.x, kt.y);
                                    }
                      
                                // nur dann Doppelklick, wenn man in die Kueche gehen kann                      
                                if ((mainFrame.dClick == true) && (mainFrame.Actions[655] == true))
                                    {
                                        mainFrame.krabat.StopWalking();
                                        mainFrame.repaint();
                                        return;
                                    }  
                            }

                        // Fenster ansehen
                        if (fenster.IsPointInRect (pTemp) == true)
                            { 
                                nextActionID = 4;
                                pTemp = pFenster;
                            }


                        // Wino ansehen
                        if (wino.IsPointInRect (pTemp) == true) 
                            {
                                nextActionID = 1;
                                pTemp = pWino;
                            }

                        // KomedHaus ansehen
                        /*if (komedHaus.IsPointInRect (pTemp) == true) 
                          {
                          nextActionID = 3;
                          }*/

                        // Enterhaken am Haus ansehen
                        if ((seilHaken.IsPointInRect (pTemp) == true) && (mainFrame.Actions[519] == true)) 
                            {
                                pTemp = pFenster;
                                nextActionID = 6;
                            }

                        mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
                        mainFrame.repaint();
                    }
      
                else
                    {
                        // rechte Maustaste

                        // Wino trinken
                        if (wino.IsPointInRect (pTemp) == true) 
                            {
                                nextActionID = 2;
                                mainFrame.wegGeher.SetzeNeuenWeg (pWino);
	   			mainFrame.repaint();
                                return;
                            }

                        // Fenster benutzen
                        if (fenster.IsPointInRect (pTemp) == true) 
                            {
                                nextActionID = 5;
                                mainFrame.wegGeher.SetzeNeuenWeg (pFenster);
                                mainFrame.repaint();
                                return;
                            }

                        // Am Enterhaken hochklettern
                        if ((seilHaken.IsPointInRect (pTemp) == true) && (mainFrame.Actions[519] == true)) 
                            {
                                nextActionID = 101;
	   			mainFrame.wegGeher.SetzeNeuenWeg (pFenster);
	   			mainFrame.repaint();
                                return;
                            }

                        // Wenn Ausgang -> kein Inventar anzeigen
                        if ((ausgangKuchnja.IsPointInRect (pTemp)) ||
                            (ausgangStadt.IsPointInRect (pTemp)))
                            {
                                return;
                            }

                        // Inventarroutine aktivieren, wenn nichts anderes angeklickt ist
                        nextActionID = 123;
                        mainFrame.krabat.StopWalking();
                        mainFrame.repaint();
                    }
            }
    }

    // befindet sich Cursor ueber Gegenstand, dann Kreuz-Cursor
    public void evalMouseMoveEvent (GenericPoint pTemp)
    {
        // Wenn Animation oder Krabat - Animation, dann transparenter Cursor
        if ((mainFrame.fPlayAnim == true) || (mainFrame.krabat.nAnimation != 0))
            {
                if (Cursorform != 20)
                    {
                        Cursorform = 20;
                        mainFrame.setCursor (mainFrame.Nix);
                    }
                return;
            }

        // wenn InventarCursor, dann anders reagieren
        if (mainFrame.invCursor == true)
            {
                // hier kommt Routine hin, die Highlight berechnet
                Borderrect tmp = mainFrame.krabat.KrabatRect();
                if ((tmp.IsPointInRect (pTemp) == true) ||
                    (fenster.IsPointInRect (pTemp) == true) ||
                    (wino.IsPointInRect (pTemp) == true) ||
                    ((mainFrame.Actions[519] == true) && 
                     (seilHaken.IsPointInRect (pTemp) == true)))
                    {
                        mainFrame.invHighCursor = true;
                    }
                else mainFrame.invHighCursor = false;
    	
                if ((Cursorform != 10) && (mainFrame.invHighCursor == false))
                    {
                        Cursorform = 10;
                        mainFrame.setCursor (mainFrame.Cinventar);
                    }
    	
                if ((Cursorform != 11) && (mainFrame.invHighCursor == true))
                    {
                        Cursorform = 11;
                        mainFrame.setCursor (mainFrame.CHinventar);
                    }	
            }
  	
        // normaler Cursor, normale Reaktion
        else
            {
                if ((wino.IsPointInRect (pTemp) == true) ||
                    // (komedHaus.IsPointInRect (pTemp) == true) ||
                    (fenster.IsPointInRect (pTemp) == true) ||
                    ((mainFrame.Actions[519] == true) && 
                     (seilHaken.IsPointInRect (pTemp) == true)))
                    {
                        if (Cursorform != 1)
                            {
                                mainFrame.setCursor (mainFrame.Kreuz);
                                Cursorform = 1;
                            }
                        return;
                    }
   
                if ((ausgangKuchnja.IsPointInRect (pTemp) == true) ||
                    (ausgangStadt.IsPointInRect (pTemp) == true))
                    {
                        if (Cursorform != 3)
                            {
                                mainFrame.setCursor (mainFrame.Cright);
                                Cursorform = 3;
                            }
                        return;
                    }

                // sonst normal-Cursor
                if (Cursorform != 0)
                    {
                        mainFrame.setCursor (mainFrame.Normal);
                        Cursorform = 0;
                    }
            }
    }

    // dieses Event nicht beachten
    public void evalMouseExitEvent (GenericMouseEvent e) {
    }

    // Key - Auswertung dieser Location /////////////////////////////////

    public void evalKeyEvent (GenericKeyEvent e)
    {
        // Wenn Inventarcursor, dann keine Keys
        if (mainFrame.invCursor == true) return;

        // Bei Animationen keine Keys
        if (mainFrame.fPlayAnim == true) return;

        // Bei Krabat - Animation keine Keys
        if (mainFrame.krabat.nAnimation != 0) return;
  
        // Nur auf Funktionstasten reagieren
        int Taste = e.getKeyCode();

        // Hauptmenue aktivieren
        if (Taste == GenericKeyEvent.VK_F1)
            {
                Keyclear();
                nextActionID = 122;
                mainFrame.repaint();
                return;
            }    

        // Save - Screen aktivieren
        if (Taste == GenericKeyEvent.VK_F2)
            {
                Keyclear();
                nextActionID = 121;
                mainFrame.repaint();
                return;
            }

        // Load - Screen aktivieren
        if (Taste == GenericKeyEvent.VK_F3)
            {
                Keyclear();
                nextActionID = 120;
                mainFrame.repaint();
                return;
            } 
    }  

    // Vor Key - Events alles deaktivieren
    private void Keyclear()
    {
        outputText="";
        if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
        mainFrame.Clipset = false;
        mainFrame.isAnim = false;
        mainFrame.krabat.StopWalking();
    }

    private void evalSound (int wein)
    {
	// zufaellig wavs fuer Geschnatter abspielen...
	if (schnauzeWein == true) return;

	if (wein == 5) mainFrame.wave.PlayFile ("sfx-dd/tropf.wav");

    }    
  
    // Aktionen dieser Location ////////////////////////////////////////

    private void DoAction ()
    {
        // nichts zu tun, oder Krabat laeuft noch
        if ((mainFrame.krabat.isWandering == true) ||
            (mainFrame.krabat.isWalking == true))
            return;

        // hier wird zu den Standardausreden von Krabat verzweigt, 
        // wenn noetig (in Superklasse)
        if ((nextActionID > 499) && (nextActionID < 600))
            {
                setKrabatAusrede();
                // manche Ausreden erfordern neuen Cursor !!!
                evalMouseMoveEvent (mainFrame.Mousepoint);
                return;
            }		

  	// Hier Evaluation der Screenaufrufe, in Superklasse
  	if ((nextActionID > 119) && (nextActionID < 129))
            {
  		SwitchScreen ();
  		return;
            }		
  	
        // Was soll Krabat machen ?
        switch (nextActionID)
            {
            case 1:
                // Wino anschauen
                int zuffZahl = (int) (Math.random () * 1.9);
                switch (zuffZahl) 
                    {
                    case 0:
                        KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00000"),
                                    Start.stringManager.getTranslation("Loc3_Haska_00001"),
                                    Start.stringManager.getTranslation("Loc3_Haska_00002"),
                                    fWino, 3, 0, 0);
                        break;

                    case 1:
                        KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00003"),
                                    Start.stringManager.getTranslation("Loc3_Haska_00004"),
                                    Start.stringManager.getTranslation("Loc3_Haska_00005"),
                                    fWino, 3, 0, 0);
                        break;
                    }
                break;

            case 2:
                // Wino benutzen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00006"),
                            Start.stringManager.getTranslation("Loc3_Haska_00007"),
                            Start.stringManager.getTranslation("Loc3_Haska_00008"),
                            fWino, 3, 0, 0);
                break;

            case 4:
                // Fenster ansehen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00009"),
                            Start.stringManager.getTranslation("Loc3_Haska_00010"),
                            Start.stringManager.getTranslation("Loc3_Haska_00011"),
                            fFenster, 3, 0, 0);
                break;

            case 5:
                // versuch ins Fenster reinzukommen (benutzen)
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00012"),
                            Start.stringManager.getTranslation("Loc3_Haska_00013"),
                            Start.stringManager.getTranslation("Loc3_Haska_00014"),
                            fFenster, 3, 0, 0);
                break;

            case 6:
                // Enterhaken am Fenster ansehen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00015"),
                            Start.stringManager.getTranslation("Loc3_Haska_00016"),
                            Start.stringManager.getTranslation("Loc3_Haska_00017"),
                            fEnterhaken, 3, 0, 0);
                break;

            case 20:
                // Enterhaken an Fenster schmeissen
                mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.krabat.SetFacing (fEnterhaken);
		schnauzeWein = true;
		Counter = 20;
		nextActionID = 21;
		break;
		
	    case 21:
		// Weile warten (Sound-Wein zu Ende) und dann werfen
		if ((--Counter) > 1) break;
                mainFrame.invCursor = false;
                // Enterhaken-Kombi aus Inventory entfernen
                mainFrame.inventory.vInventory.removeElement (new Integer (39));
                nextActionID = 22;
                krabatWirft = true;
                break;

            case 22:
                // warten auf Ende werfe Haken
                if (krabatWirft == true) break;
                NeuesBild (122, locationID);
                break;

            case 27:
                // Bemerkung nach Hakenwurf
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00018"),
                            Start.stringManager.getTranslation("Loc3_Haska_00019"),
                            Start.stringManager.getTranslation("Loc3_Haska_00020"),
                            fEnterhaken, 3, 0, 28);
                break;
        
            case 28: 
                // Ende Anim
                mainFrame.fPlayAnim = false;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                nextActionID = 0;
                mainFrame.Clipset = false;
                mainFrame.repaint();
                break;  

            case 30:
                // Helm mit wein fuellen
		mainFrame.fPlayAnim = true;
		evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.krabat.SetFacing (12);
                mainFrame.invCursor = false;
                mainFrame.inventory.vInventory.removeElement (new Integer (43));
                mainFrame.inventory.vInventory.addElement (new Integer (44));
                nextActionID = 35;
		Counter = 20;
		schnauzeWein = true;
		break;

	    case 35:
                // Helm durch HelmWein in Inventory erstetzen
                // Buecken
		if ((--Counter) > 1) break;
                mainFrame.krabat.nAnimation = 152;
		nextActionID = 37;
		mainFrame.wave.PlayFile ("sfx-dd/tropftropf.wav");
		Counter = 15;
                break;

	    case 37:
		if ((mainFrame.krabat.nAnimation != 0) || ((--Counter) > 0)) break;
		mainFrame.fPlayAnim = false;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		nextActionID = 0;
		schnauzeWein = false;
		mainFrame.repaint();
		break;

            case 100:
                // In Kueche gehen -> Krabat will nicht
                if (mainFrame.Actions[655] == false)
                    {
                        int zuffZahl2 = (int) (Math.random () * 2.9);
                        switch (zuffZahl2) 
                            {
                            case 0:
                                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00021"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00022"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00023"),
                                            fKuchnja, 3, 0, 0);
                                break;
	
                            case 1:
                                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00024"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00025"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00026"),
                                            fKuchnja, 3, 0, 0);
                                break;
	
                            case 2:
                                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00027"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00028"),
                                            Start.stringManager.getTranslation("Loc3_Haska_00029"),
                                            fKuchnja, 3, 0, 0);
                                break;
                            }
                    }
                else
                    {
                        // ab jetzt will er...
                        NeuesBild (132, locationID);
                    }
                break;

            case 101:
                // Gehe zu Spaniska, zuerst richtig rangehen
                mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.wegGeher.SetzeGarantiertNeuenWeg (pAmHaken);
                nextActionID = 110;
                break;

            case 102:
                // Gehe zu Murja
                // hier unterscheiden, welche Location konstruhiert wird
                if ((mainFrame.Actions[655] == true) && (mainFrame.Actions[656] == false))
                    {
                        NeuesBild (129, locationID);
                    }
                else
                    {
                        NeuesBild (126, locationID);
                    }	
                break;

            case 110:
                // Spruch vorm hochklettern
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00030"),
                            Start.stringManager.getTranslation("Loc3_Haska_00031"),
                            Start.stringManager.getTranslation("Loc3_Haska_00032"),
                            fEnterhaken, 3, 0, 112);
                break;

            case 112:
                // wir sind am Haken, also jetzt "nehmen"-Anim
                mainFrame.krabat.nAnimation = 90;
                nextActionID = 114;
                Counter = 4;
                break;

            case 114:
                // kurz vor Ende Nehmen umschalten
                if ((--Counter) > 1) break;
                NeuesBild (122, locationID);
                break;

            case 200:
                // drasta auf wino
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00033"),
                            Start.stringManager.getTranslation("Loc3_Haska_00034"),
                            Start.stringManager.getTranslation("Loc3_Haska_00035"),
                            fWino, 3, 0, 0);
                break;

            case 210:
                // wosusk auf wino
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Haska_00036"),
                            Start.stringManager.getTranslation("Loc3_Haska_00037"),
                            Start.stringManager.getTranslation("Loc3_Haska_00038"),
                            fWino, 3, 0, 0);
                break;

            default:
                System.out.println ("Falsche Action-ID !");
            }

    }
}