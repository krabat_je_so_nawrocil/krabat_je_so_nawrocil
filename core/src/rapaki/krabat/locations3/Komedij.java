/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.locations3;

import rapaki.krabat.Start;
import rapaki.krabat.main.GenericInputEvent;
import rapaki.krabat.main.GenericKeyEvent;
import rapaki.krabat.main.GenericMouseEvent;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.GenericRectangle;
import rapaki.krabat.main.Mainloc;
import rapaki.krabat.main.Borderrect;
import rapaki.krabat.main.Bordertrapez;
import rapaki.krabat.main.MainlocTransitional;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.sound.BackgroundMusicPlayer;

public class Komedij extends MainlocTransitional
{
    private GenericImage background, jacke, schwert, sluzDrasta, gorilla; 
    private GenericImage[] kerzen;

    private static final GenericPoint kerzenPunkte[] = 
    {new GenericPoint (177,  37),
     new GenericPoint (192,  41),
     new GenericPoint (204,  44)};

    private int kerzenBild[];
    private int Verhinderkerze[];
    private static final int MAX_VERHINDERKERZE[] = {3, 2, 4};

    // Konstanten - Rects
    private static final Borderrect linkerAusgang 
	= new Borderrect (  0,   0, 163, 150);
    private static final Borderrect sekera
	= new Borderrect (144, 210, 185, 298);
    private static final Borderrect rectJacke // fuer vordergrund
	= new Borderrect (  0, 370, 260, 480);
    private static final Borderrect rectGorilla
	= new Borderrect (424, 190, 490, 345);
    private static final Borderrect rectSluzDrasta
	= new Borderrect (502, 209, 570, 359);
    private static final Borderrect rectSchwert // fuer Vordergrund
	= new Borderrect (210, 370, 445, 480);
    private static final Borderrect postawa
	= new Borderrect ( 55, 151, 128, 479);
    private static final Borderrect mjec
	= new Borderrect (312, 227, 336, 479);
    private static final Borderrect tesaki
	= new Borderrect (491, 251, 578, 311);
  
    // Konstante Points
    private static final GenericPoint pExitLeft   = new GenericPoint (135, 372);
    private static final GenericPoint pSekera     = new GenericPoint (165, 380);
    private static final GenericPoint pGorilla    = new GenericPoint (452, 395);
    private static final GenericPoint pSluzDrasta = new GenericPoint (520, 400);
    private static final GenericPoint pPostawa    = new GenericPoint (161, 389);
    private static final GenericPoint pMjec       = new GenericPoint (371, 444);
    private static final GenericPoint pTesaki     = new GenericPoint (521, 400);
  
    // Konstante ints
    private static final int fSekera     = 9;
    private static final int fPostawa    = 6;
    private static final int fMjec       = 6;
    private static final int fTesaki     = 12;
    private static final int fSluzDrasta = 12;
    private static final int fGorilla    = 12;
  
    private int Counter = 0;

    // Initialisierung ////////////////////////////////////////////////////////

    // Instanz von dieser Location erzeugen
    public Komedij (Start caller,int oldLocation) 
    {
	super (caller, 124);
	mainFrame.Freeze (true);
    
	mainFrame.CheckKrabat ();

	mainFrame.krabat.maxx = 50;   // nicht zoomen !!!
	mainFrame.krabat.zoomf = 1f;
	mainFrame.krabat.defScale = -100;
  	
	kerzen = new GenericImage[5];

	kerzenBild = new int[] {0, 0, 0};
	Verhinderkerze = new int[] {MAX_VERHINDERKERZE[0], MAX_VERHINDERKERZE[1], MAX_VERHINDERKERZE[2]}; 

	InitLocation (oldLocation);
	mainFrame.Freeze (false);
    }
  
    // Gegend intialisieren (Grenzen u.s.w.)
    private void InitLocation (int oldLocation)
    {
	// Grenzen setzen
	mainFrame.wegGeher.vBorders.removeAllElements ();
	mainFrame.wegGeher.vBorders.addElement 
	    (new Bordertrapez (105, 390, 105, 390, 372, 475));
	mainFrame.wegGeher.vBorders.addElement 
	    (new Bordertrapez (391, 530, 391, 530, 392, 479));
	mainFrame.wegGeher.vBorders.addElement 
	    (new Bordertrapez (531, 620, 531, 620, 415, 479));
       
	mainFrame.wegSucher.ClearMatrix (3);

	mainFrame.wegSucher.PosVerbinden (0, 1); 
	mainFrame.wegSucher.PosVerbinden (1, 2);

	InitImages();
	switch (oldLocation)
	    {
	    case 0: 
		// Einsprung fuer Load
                BackgroundMusicPlayer.getInstance ().playTrack (16, true);
		break;
	    case 123: // von Hala 
		mainFrame.krabat.SetKrabatPos (new GenericPoint (160, 395));
		mainFrame.krabat.SetFacing (3);
		break;
	    }    
    }

    // Bilder vorbereiten
    private void InitImages() 
    {
	background = getPicture ("gfx-dd/komedij/komedij.gif");
	jacke      = getPicture ("gfx-dd/komedij/jacke.gif");
	schwert    = getPicture ("gfx-dd/komedij/schwert.gif");
	gorilla    = getPicture ("gfx-dd/komedij/gorilla.gif");
	sluzDrasta = getPicture ("gfx-dd/komedij/sluz.gif");

	kerzen[0] = getPicture ("gfx-dd/komedij/kerze1.gif");
	kerzen[1] = getPicture ("gfx-dd/komedij/kerze2.gif");
	kerzen[2] = getPicture ("gfx-dd/komedij/kerze3.gif");
	kerzen[3] = getPicture ("gfx-dd/komedij/kerze4.gif");
	kerzen[4] = getPicture ("gfx-dd/komedij/kerze5.gif");
    
	loadPicture();
    }

    // Paint-Routine dieser Location //////////////////////////////////////////

    public void paintLocation (GenericDrawingContext g)
    {

	// Clipping -Region initialisieren
	if (mainFrame.Clipset == false)
	    {
		mainFrame.scrollx = 0;
		mainFrame.scrolly = 0;
		Cursorform = 200;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		mainFrame.Clipset = true;
		g.setClip(0,0,644,484);
		mainFrame.isAnim = true;
	    }

	// Hintergrund und Krabat zeichnen
	g.drawImage (background, 0, 0, null);
	g.drawImage (gorilla, 424, 190, null);
    
	// flackernde Kerzen zeichnen
	g.setClip (170, 30, 60, 60);
	for (int i = 0; i <=2; i++)
	    {
		if ((--Verhinderkerze[i]) < 1)
		    {
			Verhinderkerze[i] = MAX_VERHINDERKERZE[i];
			kerzenBild[i] = (int) (Math.random () * 4.99);
		    }

		g.drawImage (kerzen[kerzenBild[i]], kerzenPunkte[i].x, kerzenPunkte[i].y, null);
	    }

	// Ist Dienstkleidung noch da
	if (mainFrame.Actions[512] == false) 
	    {
		g.setClip (502, 209, 70, 152);
		g.drawImage (sluzDrasta, 502, 209, null);
	    }

	// Debugging - Zeichnen der Laufrechtecke
	// mainFrame.showrect.Zeichne(g, mainFrame.wegGeher.vBorders);
  	
	mainFrame.wegGeher.GeheWeg ();
    
	// Animation??
	if (mainFrame.krabat.nAnimation != 0)
	    { 
		mainFrame.krabat.DoAnimation (g);
      
		// Cursorruecksetzung nach Animationsende
		if (mainFrame.krabat.nAnimation == 0) evalMouseMoveEvent (mainFrame.Mousepoint);
	    }  
	else
	    {
		if ((mainFrame.talkCount > 0) && (TalkPerson != 0))
		    {
			// beim Reden
			switch (TalkPerson)
			    {
			    case 1:
				// Krabat spricht gestikulierend
				mainFrame.krabat.talkKrabat (g);
				break;
			    case 3:
				// Krabat spricht im Monolog
				mainFrame.krabat.describeKrabat (g);
				break;
			    default:
				// steht Krabat nur da
				mainFrame.krabat.drawKrabat (g);
				break;
			    }    
		    }
		// Rumstehen oder Laufen
		else mainFrame.krabat.drawKrabat (g);
	    }  
    
	// Steht Krabat hinter einem Gegenstand ? Koordinaten noch mal checken !!!
	GenericPoint pKrTemp = mainFrame.krabat.GetKrabatPos ();

	// hinter Jacke (nur Clipping - Region wird neugezeichnet)
	if (rectJacke.IsPointInRect (pKrTemp) == true)
	    {
		g.drawImage (jacke, 63, 163, null);
	    }
	// hinter Schwert (nur Clipping - Region wird neugezeichnet)
	if (rectSchwert.IsPointInRect (pKrTemp) == true)
	    {
		g.drawImage (schwert, 249, 216, null);
	    }

	// sonst noch was zu tun ?
	if  (outputText != "")
	    {
		// Textausgabe
		GenericRectangle my;
		my = g.getClipBounds();
		g.setClip (0, 0, 644, 484);
		mainFrame.ifont.drawString (g, outputText, outputTextPos.x, outputTextPos.y, FarbenArray[TalkPerson]);
		g.setClip( (int) my.getX(), (int) my.getY(), (int) my.getWidth(), (int) my.getHeight()); 
	    }

	// Redeschleife herunterzaehlen und Neuzeichnen ermoeglichen
	if (mainFrame.talkCount > 0)
	    {
		-- mainFrame.talkCount;
		if (mainFrame.talkCount <= 1)
		    {
			mainFrame.Clipset = false;
			outputText = "";
			TalkPerson = 0;
		    }
	    }  

	if ((TalkPause > 0) && (mainFrame.talkCount < 1)) TalkPause--;

	// Gibt es was zu tun ?
	if ((nextActionID != 0) && (TalkPause < 1) && (mainFrame.talkCount < 1)) DoAction ();
    }


    // Mouse-Auswertung dieser Location ///////////////////////////////////////

    public void evalMouseEvent (GenericMouseEvent e)
    {
	GenericPoint pTemp = e.getPoint ();
	if (mainFrame.talkCount != 0) mainFrame.Clipset = false;
	if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
	outputText="";

	// Wenn in Animation, dann normales Gameplay aussetzen
	if (mainFrame.fPlayAnim == true)
	    {
		return;
	    }
    
	// Wenn Krabat - Animation, dann normales Gameplay aussetzen
	if (mainFrame.krabat.nAnimation != 0)
	    {
		return;
	    }    

	// wenn InventarCursor, dann anders reagieren
	if (mainFrame.invCursor == true)
	    {
		// linker Maustaste
		if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
		    {
			nextActionID = 0;

			Borderrect tmp = mainFrame.krabat.KrabatRect();

			// Aktion, wenn Krabat angeclickt wurde
			if (tmp.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 500 + mainFrame.whatItem;
				mainFrame.repaint();
				return;
			    }	
        
			// Ausreden fuer Sekera
			if (sekera.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 150;
				pTemp = pSekera;
			    }
        
			// Ausreden fuer postawa, wenn geht ???
			if (postawa.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 155;
				pTemp = pPostawa;
			    }
        
			// Ausreden fuer mjec
			if (mjec.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 160;
				pTemp = pMjec;
			    }	

			// Ausreden fuer tesaki, wenn sluz dr. weg
			if ((tesaki.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == true))
			    {
				nextActionID = 165;
				pTemp = pTesaki;
			    }
        		
			// Ausreden fuer sl. dr, wenn noch da
			if ((rectSluzDrasta.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == false))
			    {
				nextActionID = 170;
				pTemp = pSluzDrasta;
			    }
        
			// Ausreden fuer Gorilla
			if (rectGorilla.IsPointInRect	(pTemp) == true)
			    {
				nextActionID = 175;
				pTemp = pGorilla;
			    }
        		
			// wenn nichts anderes gewaehlt, dann nur hinlaufen
			mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
			mainFrame.repaint();
		    }
      
		// rechte Maustaste
		else
		    {
			// grundsaetzlich Gegenstand wieder ablegen
			mainFrame.invCursor = false;
			evalMouseMoveEvent (mainFrame.Mousepoint);
			nextActionID = 0;
			mainFrame.krabat.StopWalking();
			mainFrame.repaint();
			return;
		    }  
	    }

	// normaler Cursor, normale Reaktion
	else
	    {
		if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
		    {   
			// linke Maustaste
			nextActionID = 0;

			// zu Halle gehen ?
			if (linkerAusgang.IsPointInRect (pTemp) == true)
			    { 
				nextActionID = 100;
				GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
				// Wenn nahe am Ausgang, dann "gerade" verlassen
				if (linkerAusgang.IsPointInRect (kt) == false)
				    {
					pTemp = pExitLeft;
				    }
				else
				    {
					pTemp = new GenericPoint (pExitLeft.x, kt.y);
				    }
            
				if (mainFrame.dClick == true)
				    {
					mainFrame.krabat.StopWalking();
					mainFrame.repaint();
					return;
				    }  
			    }

			// Gorilla ansehen
			if (rectGorilla.IsPointInRect (pTemp) == true) 
			    {
				nextActionID = 1;
				pTemp = pGorilla;
			    }

			// Beil ansehen
			if (sekera.IsPointInRect (pTemp) == true) 
			    {
				nextActionID = 3;
				pTemp = pSekera;
			    }

			// Dienstkleidung ansehen (falls noch da)
			if ((rectSluzDrasta.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == false)) 
			    {
				nextActionID = 5;
				pTemp = pSluzDrasta;
			    }
        
			// Postawa ansehen, wenn mgl.
			if (postawa.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 10;
				pTemp = pPostawa;
			    }
        
			// mjec ansehen
			if (mjec.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 11;
				pTemp = pMjec;
			    }
        
			// tesaki ansehen
			if ((tesaki.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == true))
			    {
				nextActionID = 12;
				pTemp = pTesaki;					
			    }	

			mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
			mainFrame.repaint();
		    }
      
		else
		    {
			// rechte Maustaste

			// Gorilla mitnehmen
			if (rectGorilla.IsPointInRect (pTemp) == true) 
			    {
				nextActionID = 2;
				mainFrame.wegGeher.SetzeNeuenWeg (pGorilla);
				mainFrame.repaint();
				return;
			    }

			// Beil mitnehmen
			if (sekera.IsPointInRect (pTemp) == true) 
			    {
				nextActionID = 4;
				mainFrame.wegGeher.SetzeNeuenWeg (pSekera);
				mainFrame.repaint();
				return;
			    }

			// SluzDrasta mitnehmen (falls noch da)
			if ((rectSluzDrasta.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == false)) 
			    {
				nextActionID = 6;
				mainFrame.wegGeher.SetzeNeuenWeg (pSluzDrasta);
				mainFrame.repaint();
				return;
			    }
        
			// postawa benutzen, wenn mgl.
			if (postawa.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 20;
				mainFrame.wegGeher.SetzeNeuenWeg (pPostawa);
				mainFrame.repaint();
				return;
			    }
        
			// mjec benutzen
			if (mjec.IsPointInRect (pTemp) == true)
			    {
				nextActionID = 25;
				mainFrame.wegGeher.SetzeNeuenWeg (pMjec);
				mainFrame.repaint();
				return;
			    }
        
			// tesaki benutzen
			if ((tesaki.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == true))
			    {
				nextActionID = 30;
				mainFrame.wegGeher.SetzeNeuenWeg (pTesaki);
				mainFrame.repaint();
				return;
			    }
        
			// Wenn Ausgang -> kein Inventar anzeigen
			if (linkerAusgang.IsPointInRect (pTemp) == true) {
			    return;
			}

			// Inventarroutine aktivieren, wenn nichts anderes angeklickt ist
			nextActionID = 123;
			mainFrame.krabat.StopWalking();
			mainFrame.repaint();
		    }
	    }
    }

    // befindet sich Cursor ueber Gegenstand, dann Kreuz-Cursor
    public void evalMouseMoveEvent (GenericPoint pTemp)
    {
	// Wenn Animation oder Krabat - Animation, dann transparenter Cursor
	if ((mainFrame.fPlayAnim == true) || (mainFrame.krabat.nAnimation != 0))
	    {
		if (Cursorform != 20)
		    {
			Cursorform = 20;
			mainFrame.setCursor (mainFrame.Nix);
		    }
		return;
	    }

	// wenn InventarCursor, dann anders reagieren
	if (mainFrame.invCursor == true)
	    {
		// hier kommt Routine hin, die Highlight berechnet
		Borderrect tmp = mainFrame.krabat.KrabatRect();
		if ((tmp.IsPointInRect (pTemp) == true) ||
		    (sekera.IsPointInRect (pTemp) == true) ||
		    (rectGorilla.IsPointInRect (pTemp) == true) ||
		    ((rectSluzDrasta.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == false)) ||
		    (postawa.IsPointInRect (pTemp) == true) ||
		    (mjec.IsPointInRect (pTemp) == true) ||
		    ((tesaki.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == true)))
		    {
			mainFrame.invHighCursor = true;
		    }
		else mainFrame.invHighCursor = false;
    	
		if ((Cursorform != 10) && (mainFrame.invHighCursor == false))
		    {
			Cursorform = 10;
			mainFrame.setCursor (mainFrame.Cinventar);
		    }
    	
		if ((Cursorform != 11) && (mainFrame.invHighCursor == true))
		    {
			Cursorform = 11;
			mainFrame.setCursor (mainFrame.CHinventar);
		    }	
	    }
  	
	// normaler Cursor, normale Reaktion
	else
	    {
		if ((sekera.IsPointInRect (pTemp) == true) ||
		    (rectGorilla.IsPointInRect (pTemp) == true) ||
		    ((rectSluzDrasta.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == false)) ||
		    (postawa.IsPointInRect (pTemp) == true) ||
		    (mjec.IsPointInRect (pTemp) == true) ||
		    ((tesaki.IsPointInRect (pTemp) == true) && (mainFrame.Actions[512] == true)))
		    {
			if (Cursorform != 1)
			    {
				mainFrame.setCursor (mainFrame.Kreuz);
				Cursorform = 1;
			    }
			return;
		    }
   
		if (linkerAusgang.IsPointInRect (pTemp) == true)
		    {
			if (Cursorform != 9)
			    {
				mainFrame.setCursor (mainFrame.Cleft);
				Cursorform = 9;
			    }
			return;
		    }

		// if (obererAusgang.IsPointInRect (pTemp) == true)
		// {
		//  if (Cursorform != 12)
		//  {
		//    mainFrame.setCursor (mainFrame.Cup);
		//    Cursorform = 12;
		//  }
		//  return;
		// }

		// sonst normal-Cursor
		if (Cursorform != 0)
		    {
			mainFrame.setCursor (mainFrame.Normal);
			Cursorform = 0;
		    }
	    }
    }

    // dieses Event nicht beachten
    public void evalMouseExitEvent (GenericMouseEvent e) {
    }

    // Key - Auswertung dieser Location /////////////////////////////////

    public void evalKeyEvent (GenericKeyEvent e)
    {
	// Wenn Inventarcursor, dann keine Keys
	if (mainFrame.invCursor == true) return;

	// Bei Animationen keine Keys
	if (mainFrame.fPlayAnim == true) return;

	// Bei Krabat - Animation keine Keys
	if (mainFrame.krabat.nAnimation != 0) return;
  
	// Nur auf Funktionstasten reagieren
	int Taste = e.getKeyCode();

	// Hauptmenue aktivieren
	if (Taste == GenericKeyEvent.VK_F1)
	    {
		Keyclear();
		nextActionID = 122;
		mainFrame.repaint();
		return;
	    }    

	// Save - Screen aktivieren
	if (Taste == GenericKeyEvent.VK_F2)
	    {
		Keyclear();
		nextActionID = 121;
		mainFrame.repaint();
		return;
	    }

	// Load - Screen aktivieren
	if (Taste == GenericKeyEvent.VK_F3)
	    {
		Keyclear();
		nextActionID = 120;
		mainFrame.repaint();
		return;
	    } 
    }  

    // Vor Key - Events alles deaktivieren
    private void Keyclear()
    {
	outputText="";
	if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
	mainFrame.Clipset = false;
	mainFrame.isAnim = false;
	mainFrame.krabat.StopWalking();
    }

    // Aktionen dieser Location ////////////////////////////////////////

    private void DoAction ()
    {
	// nichts zu tun, oder Krabat laeuft noch
	if ((mainFrame.krabat.isWandering == true) ||
	    (mainFrame.krabat.isWalking == true))
	    return;

	// hier wird zu den Standardausreden von Krabat verzweigt, 
	// wenn noetig (in Superklasse)
	if ((nextActionID > 499) && (nextActionID < 600))
	    {
		setKrabatAusrede();
		// manche Ausreden erfordern neuen Cursor !!!
		evalMouseMoveEvent (mainFrame.Mousepoint);
		return;
	    }		

  	// Hier Evaluation der Screenaufrufe, in Superklasse
  	if ((nextActionID > 119) && (nextActionID < 129))
	    {
  		SwitchScreen ();
  		return;
	    }		
  	
	// Was soll Krabat machen ?
	switch (nextActionID)
	    {
	    case 1:
		// Gorilla ansehen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00000"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00001"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00002"),
			    fGorilla, 3, 0, 0);
		break;

	    case 2:
		// Gorilla mitnehmen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00003"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00004"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00005"),
			    fGorilla, 3, 0, 0);
		break;

	    case 3:
		// Beil ansehen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00006"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00007"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00008"),
			    fSekera, 3, 0, 0);
		break;

	    case 4:
		// Beil mitnehmen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00009"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00010"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00011"),
			    fSekera, 3, 0, 0);
		break;

	    case 5:
		// Bedienstetenkleidung ansehen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00012"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00013"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00014"),
			    fSluzDrasta, 3, 0, 0);
		break;

	    case 6:
		// Bedienstetenkleidung mitnehmen (wenn noch da)
		mainFrame.fPlayAnim = true;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		// mainFrame.wave.PlayFile ("sfx-dd/wusch2.wav");
		mainFrame.krabat.SetFacing (fSluzDrasta);
		nextActionID = 7;
		mainFrame.krabat.nAnimation = 121;
		Counter = 5;
		break;
				
	    case 7:
				// Ende take sluz. drasta
		if ((--Counter) == 1)
		    {
			mainFrame.inventory.vInventory.addElement (new Integer (41));
			mainFrame.Actions[512] = true;        // Flag setzen
			mainFrame.Clipset = false;  // alles neu zeichnen
		    }
		if ((mainFrame.krabat.nAnimation != 0) || (Counter > 0)) break;
		mainFrame.fPlayAnim = false;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		nextActionID = 0;
		mainFrame.repaint();
		break;
				
	    case 10:
		// Postawa ansehen
		// zuffzahl 0...1
		int zZahl = (int) (Math.random () * 1.9);
		switch (zZahl)
		    {
		    case 0:
        		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00015"),
        		            Start.stringManager.getTranslation("Loc3_Komedij_00016"),
        		            Start.stringManager.getTranslation("Loc3_Komedij_00017"),
        		            fPostawa, 3, 0, 0);
			break;
    		    
		    case 1:
        		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00018"),
        		            Start.stringManager.getTranslation("Loc3_Komedij_00019"),
        		            Start.stringManager.getTranslation("Loc3_Komedij_00020"),
        		            fPostawa, 3, 0, 0);
			break;
		    }    
		break;

	    case 11:
		// mjec ansehen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00021"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00022"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00023"),
			    fMjec, 3, 0, 0);
		break;

	    case 12:
		// tesaki ansehen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00024"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00025"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00026"),
			    fTesaki, 3, 0, 0);
		break;

	    case 20:
		// postawa mitnehmen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00027"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00028"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00029"),
			    fPostawa, 3, 0, 0);
		break;

	    case 25:
		// mjec mitnehmen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00030"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00031"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00032"),
			    fMjec, 3, 0, 0);
		break;

	    case 30:
		// tesaki mitnehmen
		KrabatSagt (Start.stringManager.getTranslation("Loc3_Komedij_00033"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00034"),
			    Start.stringManager.getTranslation("Loc3_Komedij_00035"),
			    fTesaki, 3, 0, 0);
		break;

	    case 100:
		// Gehe zu Hala-Doppelbild
		NeuesBild (123, locationID);
		break;

	    case 150:
		// Ausreden sekera
		DingAusrede (fSekera);
		break;
      
	    case 155:
		// Ausreden postawa
		DingAusrede (fPostawa);
		break;
      
	    case 160:
		// Ausreden mjec
		DingAusrede (fMjec);
		break;
      
	    case 165:
		// Ausreden tesaki
		DingAusrede (fTesaki);
		break;
      
	    case 170:
		// Ausreden sluz Drasta
		DingAusrede (fSluzDrasta);
		break;
      
	    case 175:
		// Ausreden gorilla
		DingAusrede (fGorilla);
		break;
      
	    default:
		System.out.println ("Falsche Action-ID !");
	    }

    }
}