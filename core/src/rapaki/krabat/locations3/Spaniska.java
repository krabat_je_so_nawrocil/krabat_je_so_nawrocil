/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.locations3;

import rapaki.krabat.Start;
import rapaki.krabat.anims.Dziwadzelnica;
import rapaki.krabat.anims.KrabatFall;
import rapaki.krabat.main.GenericInputEvent;
import rapaki.krabat.main.GenericKeyEvent;
import rapaki.krabat.main.GenericMouseEvent;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.GenericRectangle;
import rapaki.krabat.main.Mainloc;
import rapaki.krabat.main.Borderrect;
import rapaki.krabat.main.Bordertrapez;
import rapaki.krabat.main.MainlocTransitional;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.sound.BackgroundMusicPlayer;

public class Spaniska extends MainlocTransitional 
{
    private GenericImage background, stolc, roze, haken, pjenjezy, haken2, krabat_steigen;
    private Dziwadzelnica dziwadzelnica;
    private GenericPoint talkPoint;
    private Borderrect rectDziwadzelnica;
    private Borderrect rectLeftDziwadzelnica;
    private Borderrect rectLookDziwadzelnica;
    private GenericPoint dziwPoint;
    private GenericPoint dziwLeftPoint;
  
    private int FadeToBlack = 0;
  
    private boolean schlaegtZu    = false;
    private boolean krabatFaellt  = false;
    private boolean fallRueckgabe = true;
    private boolean beatRueckgabe = true;
    private boolean dziwSagtNurKurzeSaetze = false; // wenn Hakenanim, dann keinen Monstersatz von sich geben -> lesbar in kurzer Zeit
  
    private KrabatFall krabatFall;

    private boolean setAnim = false;
    private int setAnimID = 0;
 
    // Variablen fuer Backgroundanim
    private int AnimID = 10;
    private int AnimCounter = 0;
    private String AnimOutputText = "";
    private GenericPoint AnimOutputTextPos = new GenericPoint (0, 0);
    private int AnimTalkPerson = 0;
  
    private boolean dziwSchreit = false;

    private boolean ersterHakenDa = true;
    private boolean zweiterHakenDa = true;

    private boolean krabatVisible = true;

    private boolean klettertRein = false;

    // Konstanten - Rects
    private static final Borderrect untererAusgang 
        = new Borderrect (275, 445, 500, 479);
    private static final Borderrect obererAusgang
        = new Borderrect (456, 126, 554, 287);
    private static final Borderrect papier 
        = new Borderrect ( 67, 352, 115, 378);
    private static final Borderrect spiegel
        = new Borderrect ( 35, 146, 110, 306);
    private static final Borderrect blumen
        = new Borderrect (558, 317, 639, 440);
    private static final Borderrect faltWand
        = new Borderrect (235, 167, 413, 373);
    private static final Borderrect rectHaken
        = new Borderrect (460, 288, 524, 333);
    private static final Borderrect geld
        = new Borderrect (144, 355, 171, 374);

    // Konstante Points
    private static final GenericPoint pExitDown       = new GenericPoint (415, 479);
    private static final GenericPoint pExitUp         = new GenericPoint (454, 369);
    private static final GenericPoint pHaken          = new GenericPoint (455, 370);
    private static final GenericPoint pDziwadzelnica  = new GenericPoint (323, 450);
    private static final GenericPoint pPapier         = new GenericPoint (192, 470);
    private static final GenericPoint pGeld           = new GenericPoint (192, 470);
    private static final GenericPoint pBlumen         = new GenericPoint (620, 430);
    private static final GenericPoint pSpiegel        = new GenericPoint (195, 470);
    private static final GenericPoint talkPointCenter = new GenericPoint (320, 220);
    private static final GenericPoint dziwFeetLeft    = new GenericPoint (255, 460);
    private static final GenericPoint dziwFeetRight   = new GenericPoint (292, 460);
    private static final GenericPoint pKletterFeet    = new GenericPoint (494, 335);
    private static final GenericPoint pFaltWand       = new GenericPoint (455, 373);
  
    // Konstante ints
    private static final int fDziw     = 9;
    private static final int fKotwica  = 12;
    private static final int fSpiegel  = 9;
    private static final int fBlumen   = 6;
    private static final int fToler    = 9;
    private static final int fPapier   = 9;
    private static final int fFaltWand = 9;
  
    private int Counter = 0;

    // Initialisierung ////////////////////////////////////////////////////////

    // Instanz von dieser Location erzeugen
    public Spaniska (Start caller,int oldLocation) 
    {
        super (caller, 122);
        mainFrame.Freeze (true);

        mainFrame.CheckKrabat ();
  	
        mainFrame.krabat.maxx = 50;   // nicht zoomen !!!
        mainFrame.krabat.zoomf = 1f;
        mainFrame.krabat.defScale = -90;

        dziwadzelnica = new Dziwadzelnica (mainFrame);
    
        dziwPoint     = new GenericPoint (dziwFeetRight.x - (Dziwadzelnica.Breite / 2), dziwFeetRight.y - Dziwadzelnica.Hoehe);
        dziwLeftPoint = new GenericPoint (dziwFeetLeft.x - (Dziwadzelnica.Breite / 2), dziwFeetLeft.y - Dziwadzelnica.Hoehe);
        talkPoint     = new GenericPoint (dziwFeetLeft.x, dziwLeftPoint.y - 50);
    
        rectDziwadzelnica = new Borderrect (dziwPoint.x, dziwPoint.y, dziwPoint.x + Dziwadzelnica.Breite,
                                            dziwPoint.y + Dziwadzelnica.Hoehe);
        rectLeftDziwadzelnica = new Borderrect (dziwLeftPoint.x, dziwLeftPoint.y, dziwLeftPoint.x + Dziwadzelnica.Breite,
                                                dziwLeftPoint.y + Dziwadzelnica.Hoehe);
	rectLookDziwadzelnica = new Borderrect (dziwLeftPoint.x, dziwLeftPoint.y, 294, /* Achtung, wird nicht berechnet!!*/ dziwPoint.y + Dziwadzelnica.Hoehe);

    
        krabatFall    = new KrabatFall (mainFrame, -90);

        InitLocation (oldLocation);
    
        mainFrame.Freeze (false);
    }
  
    // Gegend intialisieren (Grenzen u.s.w.)
    private void InitLocation (int oldLocation)
    {
        // Grenzen setzen
        mainFrame.wegGeher.vBorders.removeAllElements ();
        // mainFrame.wegGeher.vBorders.addElement 
        //	(new bordertrapez (230, 245, 230, 290, 395, 429));
        mainFrame.wegGeher.vBorders.addElement 
            (new Bordertrapez (325, 490, 318, 490, 430, 479));
        mainFrame.wegGeher.vBorders.addElement 
            (new Bordertrapez (245, 317, 185, 317, 465, 479));
        mainFrame.wegGeher.vBorders.addElement 
            (new Bordertrapez (450, 455, 435, 600, 370, 429));
	mainFrame.wegGeher.vBorders.addElement 
	    (new Bordertrapez (590, 430, 620, 432));
       
        mainFrame.wegSucher.ClearMatrix (4);

        mainFrame.wegSucher.PosVerbinden (0, 1);
        mainFrame.wegSucher.PosVerbinden (0, 2);
	mainFrame.wegSucher.PosVerbinden (2, 3);

        InitImages();
        switch (oldLocation)
            {
            case 0: 
                // Einsprung fuer Load
		if (mainFrame.Actions[514] == true) BackgroundMusicPlayer.getInstance ().playTrack (16, true);
                break;
            case 121: // von Hintergasse
		if (mainFrame.Actions[514] == true) BackgroundMusicPlayer.getInstance ().playTrack (16, true);
                mainFrame.krabat.SetKrabatPos (new GenericPoint (480, 400));
                mainFrame.krabat.SetFacing (6);
                setAnim = true;
                if (mainFrame.Actions[519] == false)
                    {
                        ersterHakenDa = false;
                        zweiterHakenDa = false;
                        krabatVisible = false;
			dziwSagtNurKurzeSaetze = true;
                        setAnimID = 1000;
                    }
                else
                    {
                        setAnimID = 2000;
                        klettertRein = true;
                    }
                break;
            case 123: // von Hala 
                mainFrame.krabat.SetKrabatPos (new GenericPoint (400, 470));
                mainFrame.krabat.SetFacing (12);
                break;
            }   
    
        // wenn Dziwadzelnica noch da, dann die Backgroundanims einschalten...
        if (mainFrame.Actions[514] == false) dziwSchreit = true;
    }

    // Bilder vorbereiten
    private void InitImages() 
    {
        background = getPicture ("gfx-dd/spaniska/spaniska.gif");
        stolc      = getPicture ("gfx-dd/spaniska/stolc.gif");
        roze       = getPicture ("gfx-dd/spaniska/roze.gif");
        haken      = getPicture ("gfx-dd/spaniska/haken.gif");
        pjenjezy   = getPicture ("gfx-dd/spaniska/pjenjezy.gif");
        haken2     = getPicture ("gfx-dd/spaniska/haken2.gif");

        krabat_steigen = getPicture ("gfx-dd/spaniska/k-u-steigen.gif");
    
        loadPicture();
    }

    // Paint-Routine dieser Location //////////////////////////////////////////

    public void paintLocation (GenericDrawingContext g)
    {
        // Clipping -Region initialisieren
        if (mainFrame.Clipset == false)
            {
                mainFrame.scrollx = 0;
                mainFrame.scrolly = 0;
                Cursorform = 200;
                if (setAnim == true) mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.Clipset = true;
                g.setClip(0, 0, 644, 484);
                mainFrame.isAnim = true;
            }

        // Hintergrund und Krabat zeichnen
        g.drawImage (background, 0, 0, null);
        if (ersterHakenDa == true)
            {
                if (zweiterHakenDa == true)
                    g.drawImage (haken, 460, 288, null);
                else 
                    g.drawImage (haken2, 452, 278, null);
            }

        // Debugging - Zeichnen der Laufrechtecke
        // mainFrame.showrect.Zeichne(g, mainFrame.wegGeher.vBorders);
  	
        // ist Schauspielerin da ?
        if (mainFrame.Actions[514] == false) 
            {
                // Dziwadzelnica zeichnen
                if (schlaegtZu == false)
                    {
				// Cliprect fuer nach rechts schauen
                        g.setClip (rectDziwadzelnica.lo_point.x, rectDziwadzelnica.lo_point.y,
                                   rectDziwadzelnica.ru_point.x - rectDziwadzelnica.lo_point.x,
                                   rectDziwadzelnica.ru_point.y - rectDziwadzelnica.lo_point.y);
		  	g.drawImage (background, 0, 0, null);
                        beatRueckgabe = dziwadzelnica.drawDziwadzelnica (g, TalkPerson, schlaegtZu, AnimTalkPerson, dziwPoint);
                    }
                else
                    {
				// Cliprect fuer nach links
                        g.setClip (rectLeftDziwadzelnica.lo_point.x, rectLeftDziwadzelnica.lo_point.y,
                                   rectLeftDziwadzelnica.ru_point.x - rectLeftDziwadzelnica.lo_point.x,
                                   rectLeftDziwadzelnica.ru_point.y - rectLeftDziwadzelnica.lo_point.y);
  			g.drawImage (background, 0, 0, null);
	  		beatRueckgabe = dziwadzelnica.drawDziwadzelnica (g, TalkPerson, schlaegtZu, AnimTalkPerson, dziwLeftPoint);
                    }
            }
	
        if (mainFrame.Actions[515] == false)
            {
                // geld zeichnen, da es noch aufm Tisch liegt
                g.setClip (144, 355, 28, 21);
                g.drawImage (background, 0, 0, null);
                g.drawImage (pjenjezy, 144, 355, null);
            }		
    
        // Krabat einen Schritt laufen lassen
        mainFrame.wegGeher.GeheWeg ();
    
        // Krabat zeichnen
        if (krabatVisible == true)
            {
                if ((krabatFaellt == true) || (klettertRein == true))
                    {
                        // hier das Hinfallen, Cliprect besorgt diese Routine selbst
                        if (krabatFaellt == true) 
                            fallRueckgabe = krabatFall.drawKrabat (g, mainFrame.krabat.GetKrabatPos ());

                        // hier das reinklettern, nur 1 Image
                        if (klettertRein == true)
                            {
                                // Groesse
                                int scale = mainFrame.krabat.defScale;
                                scale += (int) (( (float) mainFrame.krabat.maxx - (float) pKletterFeet.y) / mainFrame.krabat.zoomf);
                                
                                // hier Test auf "nicht zu gross"
                                if (scale < mainFrame.krabat.defScale) scale = mainFrame.krabat.defScale;
    	
                                GenericPoint pLeftUp = new GenericPoint (pKletterFeet.x - ((50 - (scale / 2)) / 2), pKletterFeet.y - (100 - scale));
                                
                                g.setClip (pLeftUp.x, pLeftUp.y, 50 - (scale / 2), 100 - scale);
                                g.drawImage (krabat_steigen, pLeftUp.x, pLeftUp.y, 50 - (scale / 2), 100 - scale, null);
                            }
                    }
                else
                    {    
                        // Animation??
                        if (mainFrame.krabat.nAnimation != 0)
                            { 
                                mainFrame.krabat.DoAnimation (g);
                                
                                // Cursorruecksetzung nach Animationsende
                                if (mainFrame.krabat.nAnimation == 0) evalMouseMoveEvent (mainFrame.Mousepoint);
                            }  
                        else
                            {
                                if ((mainFrame.talkCount > 0) && (TalkPerson != 0))
                                    {
                                        // beim Reden
                                        switch (TalkPerson)
                                            {
                                            case 1:
                                                // Krabat spricht gestikulierend
                                                mainFrame.krabat.talkKrabat (g);
                                                break;
                                            case 3:
                                                // Krabat spricht im Monolog
                                                mainFrame.krabat.describeKrabat (g);
                                                break;
                                            default:
                                                // steht Krabat nur da
                                                mainFrame.krabat.drawKrabat (g);
                                                break;
                                            }    
                                    }
                                // Rumstehen oder Laufen
                                else mainFrame.krabat.drawKrabat (g);
                            }
                    }  
            }
    
        // Steht Krabat hinter einem Gegenstand ? Koordinaten noch mal checken !!!
        GenericPoint pKrTemp = mainFrame.krabat.GetKrabatPos ();

        // hinter Stuhl oder Rosen ? (nur Clipping - Region wird neugezeichnet)
        if (pKrTemp.x < 310) {
            g.drawImage (stolc, 147, 419, null);
        }
        if (pKrTemp.x > 490) {
            g.drawImage (roze, 543, 309, null);
        }
    
        // Hier das FadeToBlack, wenn noetig
        if (FadeToBlack > 0)
            {
                GenericRectangle my;
                my = g.getClipBounds();
                g.setClip (0, 0, 644, 484);
      
                g.clearRect (0, 0, FadeToBlack, 479);
                g.clearRect (639 - FadeToBlack, 0, 639, 479);
                g.clearRect (0, 0, 639, FadeToBlack);
                g.clearRect (0, 479 - FadeToBlack, 639, 479);
      
                g.setClip( (int) my.getX(), (int) my.getY(), (int) my.getWidth(), (int) my.getHeight()); 
            }

        // Ausgabe von Animoutputtext
        if  (AnimOutputText != "")
            {
                // Textausgabe
        	GenericRectangle may;
                may = g.getClipBounds();
                g.setClip (0, 0, 644, 484);
                mainFrame.ifont.drawString (g, AnimOutputText, AnimOutputTextPos.x, AnimOutputTextPos.y, FarbenArray[AnimTalkPerson]);
                g.setClip( (int) may.getX(), (int) may.getY(), (int) may.getWidth(), (int) may.getHeight()); 
            }

        // sonst noch was zu tun ?
        if  (outputText != "")
            {
                // Textausgabe
                GenericRectangle my;
                my = g.getClipBounds();
                g.setClip (0, 0, 644, 484);
                mainFrame.ifont.drawString (g, outputText, outputTextPos.x, outputTextPos.y, FarbenArray[TalkPerson]);
                g.setClip( (int) my.getX(), (int) my.getY(), (int) my.getWidth(), (int) my.getHeight()); 
            }

        // Redeschleife herunterzaehlen und Neuzeichnen ermoeglichen
        if (mainFrame.talkCount > 0)
            {
                -- mainFrame.talkCount;
                if (mainFrame.talkCount <= 1)
                    {
                        mainFrame.Clipset = false;
                        outputText = "";
                        TalkPerson = 0;
                    }
            }  

        if ((TalkPause > 0) && (mainFrame.talkCount < 1)) TalkPause--;

        // Anims bedienen
        if (dziwSchreit == true) DoAnims();

        // selbstaendige Anim beginnen
        if (setAnim == true)
            {
                mainFrame.krabat.StopWalking();
                setAnim = false;
                nextActionID = setAnimID;
            }

        // Gibt es was zu tun ?
        if ((nextActionID != 0) && (TalkPause < 1) && (mainFrame.talkCount < 1)) DoAction ();
    }


    // Mouse-Auswertung dieser Location ///////////////////////////////////////

    public void evalMouseEvent (GenericMouseEvent e)
    {
        GenericPoint pTemp = e.getPoint ();
        if (mainFrame.talkCount != 0) mainFrame.Clipset = false;
        if (mainFrame.talkCount > 1) 
            {
                mainFrame.talkCount = 1;
                TalkPerson = 0;
            }	 
        outputText="";

        // Wenn in Animation, dann normales Gameplay aussetzen
        if (mainFrame.fPlayAnim == true)
            {
                return;
            }
    
        // Wenn Krabat - Animation, dann normales Gameplay aussetzen
        if (mainFrame.krabat.nAnimation != 0)
            {
                return;
            }    

        // wenn InventarCursor, dann anders reagieren
        if (mainFrame.invCursor == true)
            {
                // linker Maustaste
                if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
                    {
			// Extra-Punkt fuer Walkto wegen Ueberschneidungen
			GenericPoint pTxxx = new GenericPoint (pTemp.x, pTemp.y);
			
			nextActionID = 0;

                        Borderrect tmp = mainFrame.krabat.KrabatRect();

                        // Aktion, wenn Krabat angeclickt wurde
                        if (tmp.IsPointInRect (pTemp) == true)
                            {
                                nextActionID = 500 + mainFrame.whatItem;
                                mainFrame.repaint();
                                return;
                            }	

				// ist Schauspielerin da ?
                        if (mainFrame.Actions[515] == false) 
                            {
                                // Ausreden fuer Dziwadzelnica
                                if (rectLookDziwadzelnica.IsPointInRect (pTemp) == true)
                                    {
                                        // Extra - Sinnloszeug
                                        nextActionID = 155;
                                        pTxxx = pDziwadzelnica;
                                    }				        
                            }
        
                        // Ausreden fuer kotwica - fenster
                        if (rectHaken.IsPointInRect (pTemp) == true)
                            {
                                nextActionID = 150;
                                pTxxx = pHaken;
                            }		

                        // Ausreden fuer roze
                        if (blumen.IsPointInRect (pTemp) == true)
                            {
                                switch (mainFrame.whatItem)
                                    {
                                    case 42: // hlebija	
                                        nextActionID = 200;
                                        break;
                                    default:
                                        nextActionID = 160;
                                        break;
                                    }		
                                pTxxx = pBlumen;
                            }		

                        // Ausreden fuer spaniska
                        if (faltWand.IsPointInRect (pTemp) == true)
                            {
                                nextActionID = 165;
				pTxxx = pFaltWand;
                            }		

                        // Ausreden fuer spihel
                        if (spiegel.IsPointInRect (pTemp) == true)
                            {
                                switch (mainFrame.whatItem)
                                    {
                                    case 42: // hlebija
                                    case 46: // hammer
                                    case 48: // Metall
                                        nextActionID = 210;
                                        break;
                                    default:
                                        nextActionID = 170;
                                        break;
                                    }		
                                pTxxx = pSpiegel;
                            }		

                        // Ausreden fuer 5 tolerow
                        if ((geld.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false))
                            {
                                nextActionID = 175;
                                pTxxx = pGeld;
                            }		

                        // Ausreden fuer dokument
                        if (papier.IsPointInRect (pTemp) == true)
                            {
                                nextActionID = 180;
                                pTxxx = pPapier;
                            }		

			System.out.println ("Point = " + pTxxx.x + " " + pTxxx.y);

                        // wenn nichts anderes gewaehlt, dann nur hinlaufen
                        mainFrame.wegGeher.SetzeNeuenWeg (pTxxx);
                        mainFrame.repaint();
                    }
      
                // rechte Maustaste
                else
                    {
                        // grundsaetzlich Gegenstand wieder ablegen
                        mainFrame.invCursor = false;
                        evalMouseMoveEvent (mainFrame.Mousepoint);
                        nextActionID = 0;
                        mainFrame.krabat.StopWalking();
                        mainFrame.repaint();
                        return;
                    }  
            }

        // normaler Cursor, normale Reaktion
        else
            {
                if (e.getModifiers () != GenericInputEvent.BUTTON3_MASK)
                    {   
                        GenericPoint pTxxx = new GenericPoint (pTemp.x, pTemp.y);

			// linke Maustaste
                        nextActionID = 0;

                        // zu Halle gehen ?
                        if (untererAusgang.IsPointInRect (pTemp) == true)
                            { 
                                nextActionID = 100;
                                GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
                                // Wenn nahe am Ausgang, dann "gerade" verlassen
                                if (untererAusgang.IsPointInRect (kt) == false)
                                    {
                                        pTxxx = pExitDown;
                                    }
                                else
                                    {
                                        pTxxx = new GenericPoint (kt.x, pExitDown.y);  // X-Pos bleibt, Y vom Exitpunkt
                                    }
            
                                if (mainFrame.dClick == true)
                                    {
                                        mainFrame.krabat.StopWalking();
                                        mainFrame.repaint();
                                        return;
                                    }  
                            }

                        // nach draussen gehen ?
                        if (obererAusgang.IsPointInRect (pTemp) == true)
                            { 
                                nextActionID = 101;
                                GenericPoint kt = mainFrame.krabat.GetKrabatPos();
          
                                // Wenn nahe am Ausgang, dann "gerade" verlassen
                                if (obererAusgang.IsPointInRect (kt) == false)
                                    {
                                        pTxxx = pExitUp;
                                    }
                                else
                                    {
                                        pTxxx = new GenericPoint (kt.x, pExitUp.y);
                                    }
            
                                if (mainFrame.dClick == true)
                                    {
                                        mainFrame.krabat.StopWalking();
                                        mainFrame.repaint();
                                        return;
                                    }  
                            }

                        // Enterhaken ansehen
                        if (rectHaken.IsPointInRect (pTemp) == true) {
                            nextActionID = 3;
                            pTxxx = pHaken;
                        }
                
                        // Spiegel ansehen
                        if (spiegel.IsPointInRect (pTemp) == true) {
                            pTxxx = pSpiegel;
                            nextActionID = 4;
                        }
                
                        // Blumen ansehen
                        if (blumen.IsPointInRect (pTemp) == true) {
                            nextActionID = 6;
                            pTxxx = pBlumen;
                        }

                        // Faltwand ansehen
                        if (faltWand.IsPointInRect (pTemp) == true) {
                            nextActionID = 8;
			    pTxxx = pFaltWand;
                        }
                
                        // 5 Tolerow ansehen
                        if ((geld.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false)) {
                            nextActionID = 9;
                            pTxxx = pGeld;
                        }
                
                        // dokument ansehen
                        if (papier.IsPointInRect (pTemp) == true) {
                            nextActionID = 10;
                            pTxxx = pPapier;
                        }
                
			// ist Schauspielerin da ?
                        if (mainFrame.Actions[515] == false) 
                            {
                                // Dziwadzelnica ansehen
                                if (rectLookDziwadzelnica.IsPointInRect (pTemp) == true) 
                                    {
                                        nextActionID = 1;
                                        pTxxx = pDziwadzelnica;
                                    }
                            }
                
			System.out.println ("Point = " + pTxxx.x + " " + pTxxx.y);

                        mainFrame.wegGeher.SetzeNeuenWeg (pTxxx);
                        mainFrame.repaint();
                    }
      
                else
                    {
                        // rechte Maustaste

			// ist Schauspielerin da ?
                        if (mainFrame.Actions[515] == false) 
                            {
                                // Mit Dziwadzelnica reden
                                if (rectLookDziwadzelnica.IsPointInRect (pTemp) == true) {
                                    nextActionID = 2;
                                    pTemp = pDziwadzelnica;
                                    mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
                                    mainFrame.repaint();
                                    return;
                                }
                            }

			// ist Schauspielerin noch da ?
			if (mainFrame.Actions[515] == false) 
                            {
                                // Geld nehmen -> paar auf die Ruebe 
                                if (geld.IsPointInRect (pTemp) == true) 
                                    {
					nextActionID = 20;
					pTemp = pPapier;
					mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
					mainFrame.repaint();
					return;
                                    }
                            }

                        // Papier nehmen geht irgendwie nicht
                        if (papier.IsPointInRect (pTemp) == true) 
                            {
                                nextActionID = 30;
                                pTemp = pPapier;
                                mainFrame.wegGeher.SetzeNeuenWeg (pTemp);
                                mainFrame.repaint();
                                return;
                            }

                        // Am Enterhaken runterklettern
                        if (rectHaken.IsPointInRect (pTemp) == true) {
                            nextActionID = 40;
                            mainFrame.wegGeher.SetzeNeuenWeg (pHaken);
                            mainFrame.repaint();
                            return;
                        }

                        // in Spiegel sehen
                        if (spiegel.IsPointInRect (pTemp) == true) {
                            nextActionID = 5;
                            mainFrame.wegGeher.SetzeNeuenWeg (pSpiegel);
                            mainFrame.repaint();
                            return;
                        }

                        // an Blumen riechen
                        if (blumen.IsPointInRect (pTemp) == true) {
                            nextActionID = 7;
                            mainFrame.wegGeher.SetzeNeuenWeg (pBlumen);
                            mainFrame.repaint();
                            return;
                        }

                        // Spaniska mitnehmen
                        if (faltWand.IsPointInRect (pTemp) == true) {
                            nextActionID = 35;
                            mainFrame.wegGeher.SetzeNeuenWeg (pFaltWand);
                            mainFrame.repaint();
                            return;
                        }

                        // Wenn Ausgang -> kein Inventar anzeigen
                        if ((untererAusgang.IsPointInRect (pTemp) == true) || 
                            (obererAusgang.IsPointInRect (pTemp) == true))
                            {
                                return;
                            }
             
                        // Inventarroutine aktivieren, wenn nichts anderes angeklickt ist
                        nextActionID = 123;
                        mainFrame.krabat.StopWalking();
                        mainFrame.Clipset = false;
                        ResetAnims();
                        mainFrame.repaint();
                    }
            }
    }

    // befindet sich Cursor ueber Gegenstand, dann Kreuz-Cursor
    public void evalMouseMoveEvent (GenericPoint pTemp)
    {
        // Wenn Animation oder Krabat - Animation, dann transparenter Cursor
        if ((mainFrame.fPlayAnim == true) || (mainFrame.krabat.nAnimation != 0))
            {
                if (Cursorform != 20)
                    {
                        Cursorform = 20;
                        mainFrame.setCursor (mainFrame.Nix);
                    }
                return;		
            }
    
        // wenn InventarCursor, dann anders reagieren
        if (mainFrame.invCursor == true)
            {
                // hier kommt Routine hin, die Highlight berechnet
                Borderrect tmp = mainFrame.krabat.KrabatRect();
                if ((tmp.IsPointInRect (pTemp) == true) ||
                    ((rectLookDziwadzelnica.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false)) ||
                    (papier.IsPointInRect (pTemp) == true) ||
                    (spiegel.IsPointInRect (pTemp) == true) ||
                    (blumen.IsPointInRect (pTemp) == true) ||
                    (faltWand.IsPointInRect (pTemp) == true) ||
                    (rectHaken.IsPointInRect (pTemp) == true) ||
                    ((geld.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false)))
                    {
                        mainFrame.invHighCursor = true;
                    }
                else mainFrame.invHighCursor = false;
    	
                if ((Cursorform != 10) && (mainFrame.invHighCursor == false))
                    {
                        Cursorform = 10;
                        mainFrame.setCursor (mainFrame.Cinventar);
                    }
    	
                if ((Cursorform != 11) && (mainFrame.invHighCursor == true))
                    {
                        Cursorform = 11;
                        mainFrame.setCursor (mainFrame.CHinventar);
                    }	
            }
  	
        // normaler Cursor, normale Reaktion
        else
            {
                if (((rectLookDziwadzelnica.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false)) ||
                    (papier.IsPointInRect (pTemp) == true) ||
                    (spiegel.IsPointInRect (pTemp) == true) ||
                    (blumen.IsPointInRect (pTemp) == true) ||
                    (faltWand.IsPointInRect (pTemp) == true) ||
                    (rectHaken.IsPointInRect (pTemp) == true) ||
                    ((geld.IsPointInRect (pTemp) == true) && (mainFrame.Actions[515] == false)))
                    {
                        if (Cursorform != 1)
                            {
                                mainFrame.setCursor (mainFrame.Kreuz);
                                Cursorform = 1;
                            }
                        return;
                    }

                if (untererAusgang.IsPointInRect (pTemp) == true)
                    {
                        if (Cursorform != 3)
                            {
                                mainFrame.setCursor (mainFrame.Cdown);
                                Cursorform = 3;
                            }
                        return;
                    }

                if (obererAusgang.IsPointInRect (pTemp) == true)
                    {
                        if (Cursorform != 6)
                            {
                                mainFrame.setCursor (mainFrame.Cright);
                                Cursorform = 6;
                            }
                        return;
                    }

                // sonst normal-Cursor
                if (Cursorform != 0)
                    {
                        mainFrame.setCursor (mainFrame.Normal);
                        Cursorform = 0;
                    }
            }
    }

    public void evalMouseExitEvent (GenericMouseEvent e) {
    }		

    // Key - Auswertung dieser Location /////////////////////////////////

    public void evalKeyEvent (GenericKeyEvent e)
    {
        // Wenn Inventarcursor, dann keine Keys
        if (mainFrame.invCursor == true) return;

        // Bei Animationen keine Keys
        if (mainFrame.fPlayAnim == true) return;

        // Bei Krabat - Animation keine Keys
        if (mainFrame.krabat.nAnimation != 0) return;

        // Nur auf Funktionstasten reagieren
        int Taste = e.getKeyCode();

        // Hauptmenue aktivieren
        if (Taste == GenericKeyEvent.VK_F1)
            {
                Keyclear();
                nextActionID = 122;
                mainFrame.repaint();
                return;
            }    

        // Save - Screen aktivieren
        if (Taste == GenericKeyEvent.VK_F2)
            {
                Keyclear();
                nextActionID = 121;
                mainFrame.repaint();
                return;
            }

        // Load - Screen aktivieren
        if (Taste == GenericKeyEvent.VK_F3)
            {
                Keyclear();
                nextActionID = 120;
                mainFrame.repaint();
                return;
            } 
    }  

    // Vor Key - Events alles deaktivieren
    private void Keyclear()
    {
        outputText="";
        if (mainFrame.talkCount > 1) mainFrame.talkCount = 1;
        mainFrame.Clipset = false;
        mainFrame.isAnim = false;
        mainFrame.krabat.StopWalking();
        ResetAnims();
    }

    // Aktionen dieser Location ////////////////////////////////////////

    private void DoAction ()
    {
        // nichts zu tun, oder Krabat laeuft noch
        if ((mainFrame.krabat.isWandering == true) ||
            (mainFrame.krabat.isWalking == true))
            return;

        // hier wird zu den Standardausreden von Krabat verzweigt, wenn noetig (in Superklasse)
        if ((nextActionID > 499) && (nextActionID < 600))
            {
                setKrabatAusrede();
    	
                // manche Ausreden erfordern neuen Cursor !!!
    	
                evalMouseMoveEvent (mainFrame.Mousepoint);
    	
                return;
            }		

  	// Hier Evaluation der Screenaufrufe, in Superklasse
  	if ((nextActionID > 119) && (nextActionID < 129))
            {
  		SwitchScreen ();
  		return;
            }		
  	
        // Was soll Krabat machen ?
        switch (nextActionID)
            {
            case 1:
                // Dziwad#delnica anschauen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00000"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00001"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00002"),
                            fDziw, 3, 0, 0);
                break;

            case 2:
                // Mit Dziwadzelnica reden (versuchen)
                mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                nextActionID = 301;
                mainFrame.krabat.SetFacing (fDziw);
                break;

            case 3:
                // Enterhaken anschauen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00003"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00004"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00005"),
                            fKotwica, 3, 0, 0);
                break;

            case 4:
                // Spiegel anschauen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00006"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00007"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00008"),
                            fSpiegel, 3, 0, 0);
                break;

            case 5:
                // Spiegel benutzen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00009"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00010"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00011"),
                            fSpiegel, 3, 0, 0);
                break;

            case 6:
                // Blumen anschauen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00012"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00013"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00014"),
                            fBlumen, 3, 0, 0);
                break;

            case 7:
                // Blumen benutzen
		mainFrame.fPlayAnim = true;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		mainFrame.krabat.SetFacing (fBlumen);
		nextActionID = 15;
		Counter = 3;
		break;

            case 8:
                // Faltwand ansehen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00015"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00016"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00017"),
                            fFaltWand, 3, 0, 0);
                break;

            case 9:
                // 5 Taler ansehen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00018"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00019"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00020"),
                            fToler, 3, 0, 0);
                break;

            case 10:
                // Papier ansehen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00021"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00022"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00023"),
                            fPapier, 3, 0, 0);
                break;

	    case 15:
		// wirklich riechen
		if ((--Counter) > 1) break;
		mainFrame.krabat.nAnimation = 62;
		nextActionID = 16;
		break;

	    case 16:
		// Kommentar
		if (mainFrame.krabat.nAnimation != 0) break;
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00024"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00025"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00026"),
                            fBlumen, 3, 2, 17);
                break;

	    case 17:
		// Ende Anim
		mainFrame.fPlayAnim = false;
		evalMouseMoveEvent (mainFrame.Mousepoint);
		nextActionID = 0;
		mainFrame.repaint();
		break;

            case 20:
                // Geld nehmen und niederschlagen lassen
                mainFrame.fPlayAnim = true;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                dziwSchreit = false;
                ResetAnims ();
                mainFrame.krabat.SetFacing (fToler);
                mainFrame.krabat.nAnimation = 91;
                nextActionID = 21;
                Counter = 5;
                break;

            case 21:
                // Auf Ende des Aufhebens warten
                if ((--Counter) == 1)
                    {
                        mainFrame.inventory.vInventory.addElement (new Integer (40));
                        mainFrame.Clipset = false;
                        mainFrame.Actions[515] = true;
                    }
                if ((mainFrame.krabat.nAnimation != 0) || (Counter > 0)) break;
                nextActionID = 22;
                break;
      	
            case 22:
                // Dziw haut drauf
                schlaegtZu = true;
                nextActionID = 23;
                break;
      
            case 23:
                // Faden einleiten
                if (beatRueckgabe == true) break;
                krabatFaellt = true;
                nextActionID = 24;
                break;
      	
            case 24:
                // bis zum Ende warten
                if (fallRueckgabe == true) break;
                FadeToBlack = 1;
                nextActionID = 25;
                break;		
      	
            case 25:
                // bis zum Ende warten
                FadeToBlack += 3;
                if (FadeToBlack >= 240) nextActionID = 310;
                break;		
      	
            case 30:
                // Papier mitnehmen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00027"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00028"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00029"),
                            fPapier, 3, 0, 0);
                break;

            case 35:
                // Spaniska mitnehmen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00030"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00031"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00032"),
                            fFaltWand, 3, 0, 0);
                break;
        
            case 40:
                // Haken wieder wegnehmen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00033"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00034"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00035"),
                            fKotwica, 3, 0, 0);
                break;

            case 100:
                // Frau schreit rum (Hilfe) oder gehe zu Hala-Doppelbild
                if (mainFrame.Actions[515] == false) 
                    {
                        mainFrame.fPlayAnim = true;
                        evalMouseMoveEvent (mainFrame.Mousepoint);
                        nextActionID = 301;
	    		mainFrame.repaint();
                    }
                else 
                    {
                        NeuesBild (123, locationID);
                    }
                break;

            case 101:
                // Frau schreit rum (Hilfe) oder gehe zu Hintergasse zurueck
                NeuesBild (121, locationID);
                break;

	    case 150:
                // Wokno - Ausreden
                DingAusrede (fKotwica);
                break;
   
            case 155:
                // Dziwad#delnica - Ausreden
                MPersonAusrede (fDziw);
                break;

            case 160:
                // roze - Ausreden
                DingAusrede (fBlumen);
                break;
   
            case 165:
                // spaniska - Ausreden
                DingAusrede (fFaltWand);
                break;
   
            case 170:
                // spihel - Ausreden
                DingAusrede (fSpiegel);
                break;
        
            case 175:
                // 5 tolerow - Ausreden
                DingAusrede (fToler);
                break;
   
            case 180:
                // dokument - Ausreden
                DingAusrede (fPapier);
                break;
   
            case 200:
                // hlebija auf rosen
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00036"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00037"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00038"),
                            fBlumen, 3, 0, 0);
                break;

            case 210:
                // schwere ggst auf Spiegel
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00039"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00040"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00041"),
                            fSpiegel, 3, 0, 0);
                break;

                // Krabat schmeisst Bemerkungen
            case 301:
                // zufaellige Antwort -> Zahl von 0 bis 2 generieren
                int zuffZahl2 = (int) (Math.random () * 2.9);
                switch (zuffZahl2) 
                    {
                    case 0:
                        KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00042"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00043"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00044"),
                                    0, 3, 2, 800);
                        break;

                    case 1:
                        KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00045"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00046"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00047"),
                                    0, 3, 2, 800);
                        break;

                    case 2:
                        KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00048"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00049"),
                                    Start.stringManager.getTranslation("Loc3_Spaniska_00050"),
                                    0, 3, 2, 800);
                        break;
                    }
                break;

                // KRABAT WIRD NIEDERGESCHLAGEN
            case 310:
                // Text einblenden
                mainFrame.Actions[514] = true;  // Dziw ist weg
                PersonSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00051"), Start.stringManager.getTranslation("Loc3_Spaniska_00052"), Start.stringManager.getTranslation("Loc3_Spaniska_00053"),
                            0, 54, 2, 311, talkPointCenter);
                break;

            case 311:
                // Text einblenden
                PersonSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00054"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00055"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00056"),
                            0, 54, 2, 312, talkPointCenter);
                break;

            case 312:
                // Text einblenden
                krabatFaellt = false;
                FadeToBlack = 0;
                mainFrame.Clipset = false;
                PersonSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00057"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00058"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00059"),
                            0, 54, 2, 313, talkPointCenter);
                break;

            case 313:
                // Krabat brummt der Schaedel
                BackgroundMusicPlayer.getInstance ().playTrack (16, true);
                KrabatSagt (Start.stringManager.getTranslation("Loc3_Spaniska_00060"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00061"),
                            Start.stringManager.getTranslation("Loc3_Spaniska_00062"),
                            0, 3, 2, 800);
                break;

            case 800:
                // Dialog beenden, wenn zuende gelabert...
                mainFrame.fPlayAnim = false;
                nextActionID = 0;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.repaint();
                break;

                // Anim, wie sich Haken festbeisst im Fenster ///////////////////////////////////////

            case 1000:
                // Hakenwerfanim beginnt
                Counter = 1;
                nextActionID = 1010;
                break;

            case 1010:
                // warten, bis 1. Haken gezeigt wird
                if ((--Counter) > 0) break;
                ersterHakenDa = true;
                mainFrame.Clipset = false;
                nextActionID = 1020;
                Counter = 6;
                break;

            case 1020:
                // abwarten, bis 1. Haken time out
                if ((--Counter) > 0) break;
                zweiterHakenDa = true;
		mainFrame.wave.PlayFile ("sfx-dd/haken.wav");
                mainFrame.Clipset = false;
                nextActionID = 1030;
                Counter = 60;
                break;

            case 1030:
                // warten, bis 2. Haken Timeout und dann back
                if ((--Counter) > 0) break;
                NeuesBild (121, locationID);
                break;

                // Anim, wie Krabat reingestiegen kommt ////////////////////////////////////////
            case 2000:
                // warteschleife init.
                Counter = 7;
                nextActionID = 2010;
                break;

            case 2010:
                // warten bis Ende Counter
                if ((--Counter) > 1) break;
                klettertRein = false;
		mainFrame.wegGeher.SetzeNeuenWeg (new GenericPoint (480, 420));
		nextActionID = 2020;
		break;

            case 2020:
                // warten bis Ende Counter
                nextActionID = 0;
                mainFrame.fPlayAnim = false;
                evalMouseMoveEvent (mainFrame.Mousepoint);
                mainFrame.repaint();
                break;
      		      
            default:
                System.out.println ("Falsche Action-ID !");
            }
    }
    // Schnarchanim des Loewen ausfuehren
    private void DoAnims ()
    {
  	switch (AnimID)
            {
  	
            case 10:
                // Warteschleife, damit Menu problemlos...
                AnimCounter = 10;
                AnimID = 20;
                break;
  		  
            case 20:
                // warten...
                if ((--AnimCounter) < 1) AnimID = 30;
                break;
  		  
                // Dziwadzelnica schreit rum
            case 30:
                // zufaellige Antwort -> Zahl von 0 bis 3 generieren
                int zuffZahl = (int) (Math.random () * 3.9);
		if ((dziwSagtNurKurzeSaetze == true) && (zuffZahl > 1)) zuffZahl = (int) (Math.random () * 1.9);
                switch (zuffZahl) 
                    {
                    case 0:
                        if (mainFrame.sprache == 1) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00063");
                        if (mainFrame.sprache == 2) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00064");
                        if (mainFrame.sprache == 3) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00065");
                        AnimCounter = 50;
                        break;

                    case 1:
                        if (mainFrame.sprache == 1) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00066");
                        if (mainFrame.sprache == 2) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00067");
                        if (mainFrame.sprache == 3) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00068");
                        AnimCounter =  60;
                        break;

                    case 2:
                        if (mainFrame.sprache == 1) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00069");
                        if (mainFrame.sprache == 2) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00070");
                        if (mainFrame.sprache == 3) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00071");
                        AnimCounter = 120;
                        break;

                    case 3:
                        if (mainFrame.sprache == 1) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00072");
                        if (mainFrame.sprache == 2) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00073");
                        if (mainFrame.sprache == 3) AnimOutputText = Start.stringManager.getTranslation("Loc3_Spaniska_00074");
                        AnimCounter = 70;
                        break;
                    }
                AnimOutputTextPos = mainFrame.ifont.CenterAnimText (AnimOutputText, talkPoint);
                AnimTalkPerson = 58;
                AnimID = 40;
                break;
  			
            case 40:
                // warten, bis zu Ende geschnarcht
                if ((--AnimCounter) < 1) AnimID = 50;
                break;
  			
            case 50:
                // variable Pause dazwischen
                AnimOutputText = "";
                AnimTalkPerson = 0;
                mainFrame.Clipset = false;
                AnimCounter = (int) ((Math.random () * 70) + 50);
                AnimID = 60;
                break;
  			
            case 60:
                // Pause abwarten und von vorn...
                if ((--AnimCounter) < 1) AnimID = 10;
                break;
            }
    }
  
    // Anims zuruecksetzen, damit leerer Screen bei Menu usw...
    private void ResetAnims ()
    {
  	AnimOutputText = "";
  	AnimCounter = 10;
  	AnimID = 10;
    }
  
}