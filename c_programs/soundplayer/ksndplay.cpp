//------------------------------------------------------------------------------
// Program: Krabat Sound-Player for Win32-Systems
//
// Date:    1999/09/04
//
// Author:  Stefan Saring
//------------------------------------------------------------------------------

#include <stdio.h>

// Multimedia-API von win32 benutzen (auf fuer Linker !!! (winmm.obj))
#include <windows.h>
#include <mmsystem.h>

int main (int argc, char *argv[ ], char *envp[ ] )
{
	// Abfrage der Parameter
	if (argc < 2)  // keine Parameter
	{
		printf ("\nKSndPlay  --  Krabat Sound-Player for Win32\n");
		printf ("\n  written by Stefan Saring, 1999");
		printf ("\n  Usage:  ksndplay name.wav\n");
	}
	else {
		// Sounddatei angegeben -> abspielen
		// SND_FILENAME  = Dateiname wird bei Aufruf uebergeben
		// SND_SYNC      = warten, bis Datei fertig abgespielt
		// SND_NODEFAULT = Wenn Datei nicht gefunden, kein Standardfile abspielen
		// SND_NOWAIT    = Wenn AudioDevice z.Z. benutzt-> Ruecksprung
		if (PlaySound (argv[1], NULL, SND_FILENAME | SND_SYNC | 
				SND_NODEFAULT | SND_NOWAIT) == FALSE)
		{
			printf ("\nError: Bad filename or device is currently used !\n");
			return 1;
		}
	}

	return 0;
}
