#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <mmsystem.h>

// mainly from windows API help
// where I just modified the example

#define debug

#undef debug

int main (int argc, char *argv[])
{
  int i;
  int cd_track;
  UINT wDeviceID;
  DWORD dwReturn;
  MCI_OPEN_PARMS mciOpenParms;
  MCI_SET_PARMS mciSetParms;
  MCI_PLAY_PARMS mciPlayParms;

#ifdef debug
  char abcde[300];
  DWORD fdwError;
  char lpszErrorText[300];
  UINT cchErrorText;
#endif


#ifdef debug
  for (i = 0; i < argc; i++) {

    printf ("Argument %d is %s.\n", i, argv[i]);
  }
#endif

  // Open the CD audio device by specifying the device name.

  mciOpenParms.lpstrDeviceType = "cdaudio";
  if (dwReturn = mciSendCommand((int) NULL, MCI_OPEN,
      MCI_OPEN_TYPE, (DWORD)(LPVOID) &mciOpenParms))
  {
    // Failed to open device. Don't close it; just return error.
#ifdef debug
    printf ("Could not open CDROM-Device.");
#endif
    return (-1);
  }

  // The device opened successfully; get the device ID.
  wDeviceID = mciOpenParms.wDeviceID;

  // user wants to play a track
  if (argc > 2) {
    if (strstr (argv[1],"-p")!= NULL) {
#ifdef debug
      printf ("Trying to play CD!\n");
#endif
      cd_track = 0;
      sscanf (argv[2], "%d", &cd_track); 
      if ((cd_track > 1) && (cd_track < 27)) {

#ifdef debug
	printf ("Want to play Track %d.\n", cd_track);
#endif

	// here read the cd and play the selected track

        // Set the time format to track/minute/second/frame (TMSF).
        mciSetParms.dwTimeFormat = MCI_FORMAT_TMSF;

        if (dwReturn = mciSendCommand(wDeviceID, MCI_SET, 
            MCI_SET_TIME_FORMAT, (DWORD)(LPVOID) &mciSetParms))
        {
            mciSendCommand(wDeviceID, MCI_CLOSE, 0, (int) NULL);
#ifdef debug
            printf ("Could not set time format.\n");
#endif
            return (-1);
        } 
        // Begin playback from the given track and play until the beginning 
        // of the next track.
    
        mciPlayParms.dwFrom = 0L;
        mciPlayParms.dwTo = 0L;
        mciPlayParms.dwFrom = MCI_MAKE_TMSF (cd_track, 0, 0, 0);
	if (cd_track < 26 ) 
            mciPlayParms.dwTo = MCI_MAKE_TMSF (cd_track + 1, 0, 0, 0);
//        else
//            mciPlayParms.dwTo = MCI_MAKE_TMSF (cd_track, 0, 0, 0);
	mciPlayParms.dwCallback = 0;

        // not the last Track, play only until next track
        if (cd_track < 26) 
            dwReturn = mciSendCommand(wDeviceID, MCI_PLAY, MCI_FROM | MCI_TO, (DWORD)(LPVOID) &mciPlayParms);
	
	// last track, play until the end
	else
	    dwReturn = mciSendCommand(wDeviceID, MCI_PLAY, MCI_FROM, (DWORD)(LPVOID) &mciPlayParms);

        if (dwReturn)
        {
#ifdef debug
            mciGetErrorString (fdwError, lpszErrorText, cchErrorText);
            printf ("Error is: %s\n", lpszErrorText);
#endif

            mciSendCommand(wDeviceID, MCI_CLOSE, 0, (int) NULL);
#ifdef debug
            printf ("Could not issue a play command.\n");
#endif
            return (-1);
        }
      } 
    }
  }

  // user wants to stop CD
  if (argc > 1) {
    if (strstr (argv[1],"-s") != NULL) {
      mciSendCommand(wDeviceID, MCI_STOP, (int) NULL, (int) NULL);
#ifdef debug
      printf ("Issued a CD stop command.\n");
#endif
    }
  }

  mciSendCommand(wDeviceID, MCI_CLOSE, 0, (int) NULL);	

  return (0);
}
 