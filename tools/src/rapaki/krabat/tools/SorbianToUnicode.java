/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

public class SorbianToUnicode {

	public SorbianToUnicode(String infile, String outfile) throws IOException {
		LineNumberReader reader = new LineNumberReader(new BufferedReader(new FileReader(infile)));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outfile));

		String tmp;
		while ((tmp = reader.readLine()) != null) {
			writer.write(replaceString(tmp) + "\n");
		}

		reader.close();

		writer.flush();
		writer.close();
	}

	private String replaceString(String value) {
		value = value.replaceAll("#c", "ć"); // 'c (c)
		value = value.replaceAll("#k", "č"); // "c (k)
		value = value.replaceAll("#d", "ź"); // 'z (d)
		value = value.replaceAll("#z", "ž"); // "z (z)
		value = value.replaceAll("#e", "ě"); // "e (e)
		value = value.replaceAll("#l", "ł"); // -l (l)
		value = value.replaceAll("#n", "ń"); // 'n (n)
		value = value.replaceAll("#r", "ř"); // "r (r)
		value = value.replaceAll("#s", "š"); // "s (s)
		value = value.replaceAll("#o", "ó"); // 'o (o)

		value = value.replaceAll("#C", "Ć"); // 'C (C)
		value = value.replaceAll("#K", "Č"); // "C (K)
		value = value.replaceAll("#D", "Ź"); // 'Z (D)
		value = value.replaceAll("#Z", "Ž"); // "Z (Z)
		value = value.replaceAll("#E", "Ě"); // "E (E)
		value = value.replaceAll("#L", "Ł"); // -L (L)
		value = value.replaceAll("#N", "Ń"); // 'N (N)
		value = value.replaceAll("#R", "Ř"); // "R (R)
		value = value.replaceAll("#S", "Š"); // "S (S)
		value = value.replaceAll("#O", "Ó"); // 'O (O)

		value = value.replaceAll("#a", "ä"); // a: (a)
		value = value.replaceAll("#u", "ü"); // u: (u)
		value = value.replaceAll("#p", "ö"); // o: (p)
		value = value.replaceAll("#t", "ß"); // ss (t)

		value = value.replaceAll("#A", "Ä"); // A: (A)
		value = value.replaceAll("#U", "Ü"); // U: (U)
		value = value.replaceAll("#P", "Ö"); // O: (P)
		value = value.replaceAll("#T", "\u1E9E"); // SS (T)

		value = value.replaceAll("#f", "\u201E"); // Anfueh. unten (f)
		value = value.replaceAll("#F", "\u201C"); // Anfueh. oben (F)
		value = value.replaceAll("#g", "\u201A"); // Gedankenstrich unten (g)
		value = value.replaceAll("#G", "\u2018"); // Gedankenstrich oben (G)

		value = value.replaceAll("#Y", "Ś"); // 'S (Y)
		value = value.replaceAll("#y", "ś"); // 's (y)
		value = value.replaceAll("#X", "Ŕ"); // 'R (X)
		value = value.replaceAll("#x", "ŕ"); // 'r (x)

		return value;
	}

	public static void main(String[] args) throws IOException {
		new SorbianToUnicode(args[0], args[1]);
	}

}
