/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


package rapaki.krabat.tools;

import java.awt.*;
import java.awt.image.*;
import java.io.*;

import rapaki.krabat.Start;
import rapaki.krabat.main.GenericToolkit;
import rapaki.krabat.platform.GenericImage;

public class Imagehelpercutandsave extends Component
{
    private int char_height = 21;  // Hoehe eines Zeichens in Pixeln
    private GenericImage Fontbild;
    private int ftable[][];
    private int stat;
    
    private static final int MAXGR = 240;
    
    // Konstruktor
    public Imagehelpercutandsave (String Filename)
    {
  	InitOldImages (Filename);
    }		
  
    public void CutFont(GenericImage[] allFont)
    {
	System.out.print ("Cutting Font");
	InitCharData();
	for (int i = 1; i < MAXGR; i++)
	    {
		if (ftable[i][0] == 0) allFont[i] = null;
		else
		    {
			allFont[i] = cutChar(i);
			// prepareImage (allFont[i], this);
			// stat = 0;
			// while ((stat & ALLBITS) == 0)
			//    {
			//	stat = checkImage (allFont[i], this);
				// update (stat);
			//    }
		    }
		System.out.println (i);
	    }    
	return;  	  
    }

    //Bild laden
    private void InitOldImages(String file)
    {
	MediaTracker tracker;
	try
	    {
		// Fontbild = GenericToolkit.getDefaultToolkit().getImage(file);
      
		tracker = new MediaTracker (this);
      
		// prepareImage (Fontbild, this);
		// tracker.addImage (Fontbild,1);
      
		tracker.waitForAll();
	    }
	catch (InterruptedException e)
	    {
		e.printStackTrace ();
	    }
    }
  
    // Zuordnung aller ASCII-Zeichen einem Bildausschnitt
    private void InitCharData ()
    {
	ftable = new int [MAXGR][3];

	for (int i = 0; i < MAXGR; i++)
	    {
		ftable[i][0] = 0;  ftable[i][1] = 0;  ftable[i][2] = 0;
	    }
    
	// Li.Ob. X         Li.Ob. Y           Breite
	ftable[97][0]=12;   ftable[97][1]=6;   ftable[97][2]=12;  // a
	ftable[98][0]=30;   ftable[98][1]=6;   ftable[98][2]=12;  // b
	ftable[99][0]=49;   ftable[99][1]=6;   ftable[99][2]=10;  // c
	ftable[100][0]=67 ; ftable[100][1]=6 ; ftable[100][2]=13; // d
	ftable[101][0]=86 ; ftable[101][1]=6 ; ftable[101][2]=11; // e
	ftable[102][0]=104; ftable[102][1]=6 ; ftable[102][2]=10; // f
	ftable[103][0]=122; ftable[103][1]=10; ftable[103][2]=11; // g* 4 Pix unten
	ftable[104][0]=140; ftable[104][1]=6 ; ftable[104][2]=12; // h
	ftable[105][0]=159; ftable[105][1]=6 ; ftable[105][2]=8 ; // i
	ftable[106][0]=176; ftable[106][1]=10; ftable[106][2]=9 ; // j* 4
	ftable[107][0]=196; ftable[107][1]=6 ; ftable[107][2]=13; // k
	ftable[108][0]=214; ftable[108][1]=6 ; ftable[108][2]=8 ; // l
	ftable[109][0]=233; ftable[109][1]=6 ; ftable[109][2]=18; // m
	ftable[110][0]=270; ftable[110][1]=6 ; ftable[110][2]=13; // n
	ftable[111][0]=288; ftable[111][1]=6 ; ftable[111][2]=11; // o
	ftable[112][0]=12 ; ftable[112][1]=31; ftable[112][2]=12; // p* 4
	ftable[113][0]=30 ; ftable[113][1]=31; ftable[113][2]=13; // q* 4
	ftable[114][0]=49 ; ftable[114][1]=27; ftable[114][2]=11; // r
	ftable[115][0]=66 ; ftable[115][1]=27; ftable[115][2]=10; // s
	ftable[116][0]=86 ; ftable[116][1]=27; ftable[116][2]=9 ; // t
	ftable[117][0]=104; ftable[117][1]=27; ftable[117][2]=13; // u
	ftable[118][0]=122; ftable[118][1]=31; ftable[118][2]=12; // v
	ftable[119][0]=140; ftable[119][1]=27; ftable[119][2]=16; // w
	ftable[120][0]=159; ftable[120][1]=27; ftable[120][2]=12; // x
	ftable[121][0]=177; ftable[121][1]=31; ftable[121][2]=12; // y* 4
	ftable[122][0]=196; ftable[122][1]=27; ftable[122][2]=11; // z
    
	ftable[65][0]=159; ftable[65][1]=50; ftable[65][2]=17; // A
	ftable[66][0]=179; ftable[66][1]=56; ftable[66][2]=15; // B
	ftable[67][0]=197; ftable[67][1]=50; ftable[67][2]=15; // C
	ftable[68][0]=214; ftable[68][1]=50; ftable[68][2]=16; // D
	ftable[69][0]=233; ftable[69][1]=50; ftable[69][2]=16; // E
	ftable[70][0]=251; ftable[70][1]=50; ftable[70][2]=14; // F
	ftable[71][0]=271; ftable[71][1]=50; ftable[71][2]=17; // G
	ftable[72][0]=288; ftable[72][1]=50; ftable[72][2]=18; // H
	ftable[73][0]=12 ; ftable[73][1]=74; ftable[73][2]=10; // I
	ftable[74][0]=30 ; ftable[74][1]=76; ftable[74][2]=12; // J* 2
	ftable[75][0]=49 ; ftable[75][1]=74; ftable[75][2]=19; // K
	ftable[76][0]=69 ; ftable[76][1]=74; ftable[76][2]=15; // L
	ftable[77][0]=86 ; ftable[77][1]=74; ftable[77][2]=22; // M
	ftable[78][0]=122; ftable[78][1]=74; ftable[78][2]=18; // N
	ftable[79][0]=140; ftable[79][1]=74; ftable[79][2]=18; // O
	ftable[80][0]=159; ftable[80][1]=74; ftable[80][2]=14; // P
	ftable[81][0]=177; ftable[81][1]=78; ftable[81][2]=18; // Q* 4
	ftable[82][0]=196; ftable[82][1]=74; ftable[82][2]=18; // R
	ftable[83][0]=214; ftable[83][1]=74; ftable[83][2]=13; // S
	ftable[84][0]=234; ftable[84][1]=74; ftable[84][2]=14; // T
	ftable[85][0]=251; ftable[85][1]=74; ftable[85][2]=18; // U
	ftable[86][0]=270; ftable[86][1]=74; ftable[86][2]=17; // V
	ftable[87][0]=288; ftable[87][1]=74; ftable[87][2]=23; // W
	ftable[88][0]=12 ; ftable[88][1]=97; ftable[88][2]=17; // X
	ftable[89][0]=30 ; ftable[89][1]=97; ftable[89][2]=17; // Y
	ftable[90][0]=49 ; ftable[90][1]=97; ftable[90][2]=15; // Z
      
	// Deutsche Umlaute
	ftable[219][0]=214; ftable[219][1]=27; ftable[219][2]=12; // ?
	ftable[220][0]=233; ftable[220][1]=27; ftable[220][2]=13; // ?
	ftable[221][0]=251; ftable[221][1]=27; ftable[221][2]=11; // ?
	ftable[222][0]=270; ftable[222][1]=27; ftable[222][2]=12; // ?
	ftable[223][0]=67 ; ftable[223][1]=97; ftable[223][2]=16; // ?
	ftable[224][0]=86 ; ftable[224][1]=97; ftable[224][2]=18; // ?
	ftable[225][0]=104; ftable[225][1]=97; ftable[225][2]=18; // ?
	ftable[226][0]=122; ftable[226][1]=97; ftable[226][2]=13; // ?

	// Zahlen:
	ftable[48][0]=177; ftable[48][1]=118; ftable[48][2]=11; // 0
	ftable[49][0]=13 ; ftable[49][1]=118; ftable[49][2]=10; // 1
	ftable[50][0]=30 ; ftable[50][1]=118; ftable[50][2]=12; // 2
	ftable[51][0]=49 ; ftable[51][1]=118; ftable[51][2]=11; // 3
	ftable[52][0]=67 ; ftable[52][1]=118; ftable[52][2]=12; // 4
	ftable[53][0]=86 ; ftable[53][1]=118; ftable[53][2]=11; // 5
	ftable[54][0]=104; ftable[54][1]=118; ftable[54][2]=11; // 6
	ftable[55][0]=122; ftable[55][1]=118; ftable[55][2]=12; // 7
	ftable[56][0]=140; ftable[56][1]=118; ftable[56][2]=11; // 8
	ftable[57][0]=159; ftable[57][1]=118; ftable[57][2]=11; // 9

	// kleine sorbische Buchstaben:
	ftable[200][0]=288; ftable[200][1]=27; ftable[200][2]=10; // 'c 
	ftable[201][0]=13 ; ftable[201][1]=53; ftable[201][2]=10; // ^c
	ftable[202][0]=30 ; ftable[202][1]=53; ftable[202][2]=11; // 'z
	ftable[203][0]=49 ; ftable[203][1]=50; ftable[203][2]=11; // ^z
	ftable[204][0]=67 ; ftable[204][1]=50; ftable[204][2]=11; // ^e
	ftable[205][0]=85 ; ftable[205][1]=50; ftable[205][2]=10; // 'l
	ftable[206][0]=105; ftable[206][1]=50; ftable[206][2]=13; // 'n
	ftable[207][0]=124; ftable[207][1]=53; ftable[207][2]=11; // ^r
	ftable[208][0]=141; ftable[208][1]=50; ftable[208][2]=10 ; // ^s
	ftable[209][0]=255; ftable[209][1]=6; ftable[209][2]=11 ; // 'o

	// grosse sorbische Buchstaben:
	ftable[210][0]=141; ftable[210][1]=97 ; ftable[210][2]=15; // 'C
	ftable[211][0]=160; ftable[211][1]=97 ; ftable[211][2]=15; // ^C
	ftable[212][0]=196; ftable[212][1]=97 ; ftable[212][2]=15; // 'Z
	ftable[213][0]=173; ftable[213][1]=169; ftable[213][2]=15; // ^Z
	ftable[214][0]=214; ftable[214][1]=97 ; ftable[214][2]=16; // ^E
	ftable[215][0]=233; ftable[215][1]=97 ; ftable[215][2]=15; // 'L
	ftable[216][0]=251; ftable[216][1]=97 ; ftable[216][2]=18; // 'N
	ftable[217][0]=270; ftable[217][1]=97 ; ftable[217][2]=18; // ^R
	ftable[218][0]=288; ftable[218][1]=97 ; ftable[218][2]=13; // ^S
	ftable[227][0]=26 ; ftable[227][1]=163; ftable[227][2]=18; // 'O

	// Sonderzeichen:
	ftable[33][0]=198; ftable[33][1]=118; ftable[33][2]=6 ;  // !
	ftable[63][0]=215; ftable[63][1]=118; ftable[63][2]=10;  // ?
	ftable[44][0]=12 ; ftable[44][1]=142; ftable[44][2]=7 ;  // ,  * 3
	ftable[46][0]=234; ftable[46][1]=118; ftable[46][2]=6 ;  // .
	ftable[58][0]=253; ftable[58][1]=118; ftable[58][2]=6 ;  // :
	ftable[43][0]=12 ; ftable[43][1]=163; ftable[43][2]=11;  // +
	ftable[45][0]=270; ftable[45][1]=118; ftable[45][2]=8 ;  // -
	ftable[40][0]=159; ftable[40][1]=142; ftable[40][2]=9 ;  // (  * 3
	ftable[41][0]=177; ftable[41][1]=142; ftable[41][2]=8 ;  // )  * 3
	ftable[228][0]=49 ;ftable[228][1]=142;ftable[228][2]=11; // " unten	* 3
	ftable[229][0]=67 ;ftable[229][1]=139;ftable[229][2]=11; // " oben
	ftable[230][0]=67 ;ftable[230][1]=139;ftable[230][2]=5;  // ' oben
	ftable[231][0]=67 ;ftable[231][1]=139;ftable[231][2]=5;  // ' unten
    
	// niedersorbische Extrazeichen
	ftable[235][0]=195;ftable[235][1]=169;ftable[235][2]=13;  // Gross s mit strich
	ftable[236][0]=254;ftable[236][1]=169;ftable[236][2]=10;  // Klein s mit strich
	ftable[237][0]=288;ftable[237][1]=169;ftable[237][2]=18;  // Gross r mit strich
	ftable[238][0]=273;ftable[238][1]=169;ftable[238][2]=11;  // Klein r mit strich

    }

    // Zeichen aus Font-Bild ausschneiden
    private synchronized GenericImage cutChar (int code)
    {
	int pixels[] = new int[ftable[code][2] * char_height];
    
	int[] temp2 = null;
    
	int lalalang;
    
	byte[] zweidreinull = 
	{ 5, 20, 20,  5,  5,
	  20, 15, 15, 20,  5,
	  20, 15, 15, 20, 10,
	  20, 15, 15, 20, 10,
	  20, 15, 15, 20, 10,
	  5, 20, 15, 20, 10,
	  5,  5, 20, 20, 10,
	  5,  5,  5, 20,  5,
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5};

	byte[] zweidreieins = 
	{ 5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5, 	  
	  5,  5,  5,  5,  5,
	  5, 20, 20,  5,  5,
	  20, 15, 15, 20,  5,
	  20, 15, 15, 20, 10,
	  20, 15, 15, 20, 10,
	  20, 15, 15, 20, 10,
	  5, 20, 15, 20, 10,
	  5,  5, 20, 20, 10,
	  5,  5,  5, 20,  5};

	byte[] two = null;
    
	System.out.println ("Cutter called !");
    
	if ((code < 230) || (code > 231))
	    {
		
		/*
		PixelGrabber pg = new PixelGrabber (Fontbild, 
						    ftable[code][0], ftable[code][1], 
						    ftable[code][2], char_height, 
						    pixels, 0, ftable[code][2]);
		*/
		// Auschnitt ins Array schneiden
      
		System.out.println ("PG init");
      
		/*
		try 
		    {
			pg.grabPixels();
		    } 
		catch (InterruptedException e) {
		    System.err.println("Interrupted waiting for pixels !");
		}
     	*/
		System.out.println ("PG ready !");
      
		/*
		int ss = 0;
		while ((ss & ALLBITS) == 0)
		    {
			ss = pg.getStatus();
			update (ss);
		    }
     
		System.out.print (".");
    
		int[] temp = (int[] ) pg.getPixels();
		temp2 = temp;
		two = ConvertArray (temp);
		*/
	    }
    
	if (code == 230)
	    {
		two = zweidreinull;
		lalalang = 105;
	    }
    
	if (code == 231)
	    {
		two = zweidreieins;
		lalalang = 105;
	    }
    
	String File = "gfx/char/"+ code + ".fnt";
 
   	// File speichern
  	try
	    {
		FileOutputStream Data = new FileOutputStream(File);
		Data.write (two);
		Data.close();
	    }
	catch (IOException e)
	    {
		System.out.println("File write error "+e.toString());
	    }
    
	// GenericImage aus Ausschnitt erzeugen und zurueckgeben
	/*return Toolkit.getDefaultToolkit (). createImage (
	  new MemoryImageSource (ftable[code][2], char_height, 
	  temp2, 0, ftable[code][2]));*/
	return null;
    }

    public void update (int infoflags)
    {
	System.out.println ("Update called..");
	if ((infoflags & ABORT) != 0)
	    {
		System.out.println ("Abort");
	    }
	if ((infoflags & ALLBITS) != 0)
	    {
		System.out.println ("Allbits");
	    }
	if ((infoflags & ERROR) != 0)
	    {
		System.out.println ("Error");
	    }
	if ((infoflags & FRAMEBITS) != 0)
	    {
		System.out.println ("Framebits");
	    }
	if ((infoflags & HEIGHT) != 0)
	    {
		System.out.println ("Height");
	    }
	if ((infoflags & PROPERTIES) != 0)
	    {
		System.out.println ("Properties");
	    }
	if ((infoflags & SOMEBITS) != 0) 
	    {
		System.out.println ("Somebits");
	    }
	if ((infoflags & WIDTH) != 0) 
	    {
		System.out.println ("Width");
	    }
    }  
  
    private byte[] ConvertArray (int[] tmp)
    {
  	byte[] xx = new byte [tmp.length];
  	
  	for (int i = 0; i < tmp.length; i++)
  	{
	    // transparente Farbe
	    if (tmp[i] == 0x00c0c0c0) xx[i] = 5;
	    else
		{
		    // schwarze Farbe
		    if (tmp[i] == 0xff000000) xx[i] = 10;
		    else
			{
			    // Textfarbe
			    if (tmp[i] == 0xff00ff00) xx[i] = 15;
			    else
				{
  	  			// Randfarbe
				    if (tmp[i] == 0xfffcfcfc) xx[i] = 20;
				    else
					{
					    System.out.print ("?");
					}
				}		
			}
		}
  	}
  	
  	return xx;
    }	  						
} 