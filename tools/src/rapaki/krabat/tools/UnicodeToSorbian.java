/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

public class UnicodeToSorbian {

	public UnicodeToSorbian(String infile, String outfile) throws IOException {
		LineNumberReader reader = new LineNumberReader(new BufferedReader(new FileReader(infile)));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outfile));
		
		String tmp;
		while ((tmp = reader.readLine()) != null) {
			writer.write(replaceString(tmp) + "\n");
		}
		
		reader.close();
		
		writer.flush();
		writer.close();
	}
	
	private String replaceString(String value) {
		value = value.replaceAll("ć", "#c"); // 'c  (c)
		value = value.replaceAll("č", "#k"); // "c  (k)
		value = value.replaceAll("ź", "#d"); // 'z  (d)
		value = value.replaceAll("ž", "#z"); // "z  (z)
		value = value.replaceAll("ě", "#e"); // "e  (e)
		value = value.replaceAll("ł", "#l"); // -l  (l)
		value = value.replaceAll("ń", "#n"); // 'n  (n)
		value = value.replaceAll("ř", "#r"); // "r  (r)
		value = value.replaceAll("š", "#s"); // "s  (s)
		value = value.replaceAll("ó", "#o"); // 'o  (o)

		value = value.replaceAll("Ć", "#C"); // 'C  (C)
		value = value.replaceAll("Č", "#K"); // "C  (K)
		value = value.replaceAll("Ź", "#D"); // 'Z  (D)
		value = value.replaceAll("Ž", "#Z"); // "Z  (Z)
		value = value.replaceAll("Ě", "#E"); // "E  (E)
		value = value.replaceAll("Ł", "#L"); // -L  (L)
		value = value.replaceAll("Ń", "#N"); // 'N  (N)
		value = value.replaceAll("Ř", "#R"); // "R  (R)
		value = value.replaceAll("Š", "#S"); // "S  (S)
		value = value.replaceAll("Ó", "#O"); // 'O  (O)
		
		value = value.replaceAll("ä", "#a"); // a: (a)
		value = value.replaceAll("ü", "#u"); // u: (u)
		value = value.replaceAll("ö", "#p"); // o: (p)		
		value = value.replaceAll("ß", "#t"); // ss (t)
		
		value = value.replaceAll("Ä", "#A"); // A: (A)
		value = value.replaceAll("Ü", "#U"); // U: (U)
		value = value.replaceAll("Ö", "#P"); // O: (P)
		value = value.replaceAll("\u1E9E", "#T"); // SS (T)
		
		value = value.replaceAll("\u201E", "#f"); // Anfueh. unten (f)
		value = value.replaceAll("\u201C", "#F"); // Anfueh. oben  (F)
		value = value.replaceAll("\u201A", "#g"); // Gedankenstrich unten (g)
		value = value.replaceAll("\u2018", "#G"); // Gedankenstrich oben (G)
		
		value = value.replaceAll("Ś", "#Y"); // 'S (Y) 
		value = value.replaceAll("ś", "#y"); // 's (y)
		value = value.replaceAll("Ŕ", "#X"); // 'R (X)
		value = value.replaceAll("ŕ", "#x"); // 'r (x)
		
		return value;
	}

	public static void main(String[] args) throws IOException {
		new UnicodeToSorbian(args[0], args[1]);
	}
}
