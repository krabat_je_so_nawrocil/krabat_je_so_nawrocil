/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import rapaki.krabat.main.GameEventHints;
import rapaki.krabat.platform.GenericContainer;
import rapaki.krabat.platform.GenericCursor;

public class AndroidContainer extends GenericContainer {

	private final AndroidContainerImpl impl;
	
	public AndroidContainer(AndroidContainerImpl impl) {
		this.impl = impl;
	}
	
	public boolean repaint(GameEventHints hints) {
		return impl.repaint(hints);
	}

	public void setCursor(GenericCursor cursor) {
		impl.setCursorImage(((AndroidCursor) cursor).getCursorImage());
	}

}
