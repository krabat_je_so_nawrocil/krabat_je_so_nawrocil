/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import rapaki.krabat.main.GenericAlphaComposite;
import rapaki.krabat.main.GenericColor;
import rapaki.krabat.platform.GenericDrawingContext2D;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

public class AndroidDrawingContext2D extends GenericDrawingContext2D {

	private final Canvas canvas;
	
	private final Paint currentPaint;
	
	public AndroidDrawingContext2D(Canvas canvas) {
		this.canvas = canvas;
		currentPaint = new Paint(Paint.FILTER_BITMAP_FLAG);
		currentPaint.setARGB(255, 0, 0, 0);
		currentPaint.setStyle(Paint.Style.FILL);
		currentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
		// currentPaint.setFilterBitmap(false);
	}
	
	public void drawImage(GenericImage genericImage, int x, int y,
			GenericImageObserver observer) {
		Bitmap bitmap = ((AndroidImage) genericImage).getBitmap();
		// System.out.println("Drawing blended bitmap.");
		canvas.drawBitmap(bitmap, x, y, currentPaint);
	}

	public void fillRect(int x, int y, int width, int height) {
		// currentPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		Rect r = new Rect(x, y, x + width, y + height);
		// System.out.println("Filling rect!");
		canvas.drawRect(r, currentPaint);
	}

	public void setColor(GenericColor color) {
		// System.out.println("Setting color to black!");
		currentPaint.setARGB(currentPaint.getAlpha(), 
				color.getR(), color.getG(), color.getB());
	}

	public void setComposite(GenericAlphaComposite ad) {
		int alpha = (int) (ad.getAplha() * 255);
		currentPaint.setAlpha(alpha);
		// System.out.println("Set paint's alpha to:" + alpha);
	}
}
