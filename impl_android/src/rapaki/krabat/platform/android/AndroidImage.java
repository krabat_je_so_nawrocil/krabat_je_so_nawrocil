/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageObserver;
import rapaki.krabat.platform.GenericImageProducer;

public class AndroidImage extends GenericImage {

	private final Bitmap bitmap;
	
	public AndroidImage(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public GenericDrawingContext getGraphics() {
		AndroidDrawingContext context = new AndroidDrawingContext(new Canvas(bitmap));
		return context;
	}

	public int getHeight(GenericImageObserver object) {
		return bitmap.getHeight();
	}

	public GenericImage getScaledInstance(int width, int height, int hints) {
		Bitmap newInst = Bitmap.createScaledBitmap(bitmap, width, height, true);
		return new AndroidImage(newInst);
	}

	public GenericImageProducer getSource() {
		return new AndroidImageProducer(bitmap);
	}

	public int getWidth(GenericImageObserver object) {
		return bitmap.getWidth();
	}

}
