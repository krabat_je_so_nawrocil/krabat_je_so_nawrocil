/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import android.graphics.Bitmap;
import rapaki.krabat.main.GenericFilteredImageSource;
import rapaki.krabat.main.GenericImageFilter;
import rapaki.krabat.main.GenericMemoryImageSource;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.platform.GenericCursor;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageObserver;
import rapaki.krabat.platform.GenericToolkitImpl;

public class AndroidToolkitImpl extends GenericToolkitImpl {

	public int checkImage(GenericImage genericImage,
			GenericImageObserver observer) {
		// TODO need to do sth here?
		return GenericToolkitImpl.ALLBITS;
	}

	public GenericCursor createCustomCursor(GenericImage cursorImage,
			GenericPoint hotSpot, String string) {
		return new AndroidCursor(cursorImage);
	}

	public GenericImage createImage(
			GenericMemoryImageSource gmis) {
		// int width = gmis.getW();
		// int height = gmis.getH();
		
		Bitmap bitmap = Bitmap.createBitmap(gmis.getPix(), gmis.getOff(), 
				gmis.getScan(), gmis.getW(), gmis.getH(), Bitmap.Config.ARGB_8888);
		//  System.out.println("Density from createBitmap(pixels) is " + bitmap.getDensity());
		/*
		System.out.println("Bitmap size requested: " + width + "," + height + " ; " + 
				"Bitmap size created: " + bitmap.getWidth() + "," + bitmap.getHeight());
		*/
		return new AndroidImage(bitmap);
	}

	public GenericImage createImage(
			GenericFilteredImageSource gfis) {
		Bitmap bitmap = ((AndroidImageProducer) gfis.getProducer()).getBitmap();
		GenericImageFilter filter = gfis.getFilter();
		
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int[] pixels = new int[width * height];
		
		bitmap.getPixels(pixels, 0, width, 
				0, 0, width, height);
		// bitmap = null;
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = filter.filterRGB(0, 0, pixels[i]);
		}
		bitmap = Bitmap.createBitmap(pixels, 0, width, width, height, Bitmap.Config.ARGB_8888);
		
		return new AndroidImage(bitmap);
	}

	public GenericImage createImage(int width, int height) {
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		// System.out.println("--------------->>> Density from createBitmap(empty) is " + bitmap.getDensity());
		/*
		System.out.println("Bitmap size requested: " + width + "," + height + " ; " + 
				"Bitmap size created: " + bitmap.getWidth() + "," + bitmap.getHeight());
		*/
		return new AndroidImage(bitmap);
	}

	public void grabPixelsFromImage(GenericImage actualImage, int x, int y,
			int w, int h, int[] pix, int off, int scansize) {
		Bitmap bitmap = ((AndroidImage) actualImage).getBitmap();
		bitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
		bitmap.getPixels(pix, off, scansize, x, y, w, h);		
	}

	public void prepareImage(GenericImage genericImage,
			GenericImageObserver observer) {
		// TODO nothing to do here???
	}

}
