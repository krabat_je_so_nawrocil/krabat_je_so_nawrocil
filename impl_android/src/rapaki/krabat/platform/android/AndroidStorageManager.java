/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Properties;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;

import rapaki.krabat.platform.GenericStorageManager;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

public class AndroidStorageManager extends GenericStorageManager {

	private final Context context;
	
	private static final String loadSavePrefix="krabat";
	
	private static final String loadSaveSuffix = ".hra";
	
	private static final String propertyFileName = "game.properties";
	
	private static final String resourcePrefix = "resource";
	
	private final String urlBase;
	
	private final AssetManager manager;

	private final boolean useAssets;
	
	private final boolean useExpansionFile;
	
	private ZipResourceFile zipFile;
	
	public AndroidStorageManager(AssetManager manager, Context context, String urlBase, 
			boolean useAssets, boolean useExpansionFile, String expansionFileName) throws IOException {
		this.context = context;
		this.urlBase = urlBase;
		this.manager = manager;
		this.useAssets = useAssets;
		this.useExpansionFile = useExpansionFile;
		
		if (useExpansionFile == true) {
			zipFile = APKExpansionSupport.getResourceZipFile(new String[]{expansionFileName});
		}
	}
	
	public boolean isLoadSaveSupported() {
		return true;
	}

	/**
	 * This method will always load from the internal memory. No adaptations necessary w.r.t. 
	 * external media files.
	 * 
	 */
	public byte[] loadFromFile(int gameIndex) {
		byte[] ret;
		
		try {
			BufferedInputStream stream = new BufferedInputStream(context.openFileInput(loadSavePrefix + Integer.toString(gameIndex) + loadSaveSuffix));
			ret = new byte[fileSize];
			int ptr = 0;
			int status;
			while (ptr < ret.length) {
				status = stream.read(ret, ptr, ret.length - ptr);
				
				if (status < 0) {
					throw new IOException("EOF while still expecting data!");
				} else {
					ptr += status;
				}
			}
			stream.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			return new byte[]{};
		}
		
		return ret;
	}

	/**
	 * This method will always save to the internal memory. No adaptations necessary w.r.t. 
	 * external media files.
	 * 
	 */
	public boolean saveToFile(byte[] data, int gameIndex) {
		try {
			BufferedOutputStream stream = new BufferedOutputStream(context.openFileOutput(loadSavePrefix + Integer.toString(gameIndex) + loadSaveSuffix, Context.MODE_PRIVATE));
			stream.write(data);
			stream.flush();	
			stream.close();
			System.out.println("Successfully saved: " + (loadSavePrefix + Integer.toString(gameIndex) + loadSaveSuffix));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isSlownikSupported() {
		return true;
	}

	/**
	 * The dictionary is located in the external files.
	 */
	public byte[] loadSlownik(String relativeFileName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			BufferedInputStream stream = new BufferedInputStream(getResourceStream(urlBase + File.separator + resourcePrefix + File.separator + relativeFileName));
			byte[] tmp = new byte[1024];
			int status = 0;
			while (status != -1) {
				status = stream.read(tmp);
				
				if (status > 0) {
					baos.write(tmp, 0, status);
				}
			}
			stream.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			return new byte[]{};
		}
		
		return baos.toByteArray();
	}

	public boolean isPropertyStorageSupported() {
		return true;
	}

	/**
	 * This method will always load from the internal memory. No adaptations necessary w.r.t. 
	 * external media files.
	 * 
	 */
	public void getGameProperties(Properties props) {
		try {
			BufferedInputStream stream = new BufferedInputStream(context.openFileInput(propertyFileName));
			props.load(stream);
		} catch (IOException e) {
			// intentionally empty
		}
	}

	/**
	 * This method will always save to the internal memory. No adaptations necessary w.r.t. 
	 * external media files.
	 * 
	 */
	public void saveGameProperties(Properties props) {
		try {
			BufferedOutputStream stream = new BufferedOutputStream(context.openFileOutput(propertyFileName, Context.MODE_PRIVATE));
			props.store(stream, "Game properties");
			System.out.println("Game properties stored successfully!");
		} catch (IOException e) {
			System.err.println("There was a problem storing the game properties!");
			e.printStackTrace();
		}
	}

	/**
	 * The translation file(s) is/are located in the external files.
	 */
	public HashMap<String, String> loadTranslationsFile(String filename) {
		HashMap<String, String> translations = new HashMap<String, String>();
		
		try {
			LineNumberReader reader = new LineNumberReader(new BufferedReader(getResourceReader(urlBase + File.separator + resourcePrefix + File.separator + filename)));
			String line = "";
			String key;
			String value;
			int separatorPos;
			while ((line = reader.readLine()) != null) {
				separatorPos = line.indexOf("\t");
				key = line.substring(0, separatorPos).trim();
				value = line.substring(separatorPos + 1); // do not trim, the spaces may be wanted!!!
				translations.put(key, value);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Translations file not found!");
		}
		
		return translations;
	}
	
	public void mergeTranslationsFile(String filename, HashMap<String, String> translations, boolean isFake, String fakePrefix) {
		try {
			LineNumberReader reader = new LineNumberReader(new BufferedReader(getResourceReader(urlBase + File.separator + resourcePrefix + File.separator + filename)));
			String line = "";
			String key;
			String value;
			int separatorPos;
			while ((line = reader.readLine()) != null) {
				separatorPos = line.indexOf("\t");
				key = line.substring(0, separatorPos).trim();
				value = line.substring(separatorPos + 1); // do not trim, the spaces may be wanted!!!
				if (isFake == true) {
					value = fakePrefix + " " + value;
				}
				translations.put(key, value);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Translations file not found!");
		}
	}
	
	private Reader getResourceReader(String path) throws IOException {
		return getResourceReaderImpl(path);
	}
	
	public InputStream getResourceStream(String path) throws IOException {
		return getResourceStreamImpl(path);
	}
	
	public FileDescriptor getResourceDescriptor(String path) throws IOException {
		return getResourceDescriptorImpl(path);
	}
	
	public AssetFileDescriptor getResourceAssetDescriptor(String path) throws IOException {
		return getResourceAssetDescriptorImpl(path);
	}
	
	/*
	 * These are the implementations for external resources.
	 */
	
	private Reader getResourceReaderImpl(String path) throws IOException {
		if (useExpansionFile == true) {
			
			// System.out.println("Try to access resource: " + path);
			
			InputStream is = zipFile.getInputStream(removeLeadingSlash(path));
			/*
			if (is == null) {
				System.err.println("Inputstream is null!!!");
			}
			*/
			return new InputStreamReader(is);
		} else {
			if (useAssets == true) {
				return new InputStreamReader(manager.open(removeLeadingSlash(path)));
			} else {
				return new FileReader(new File(path));
			}
		}
	}
	
	public InputStream getResourceStreamImpl(String path) throws IOException {
		if (useExpansionFile == true) {
			return zipFile.getInputStream(removeLeadingSlash(path));
		} else {
			if (useAssets == true) {
				return manager.open(removeLeadingSlash(path));
			} else {
				return new FileInputStream(path);
			}
		}
	}

	public FileDescriptor getResourceDescriptorImpl(String path) throws IOException {
		if (useAssets == true) {
			// TODO Hmmm does not seem to work
			// TODO would have also to use the asset file descriptor???
			return manager.openFd(removeLeadingSlash(path)).getFileDescriptor();
		} else {
			return new FileInputStream(path).getFD();
		}
	}
	
	public AssetFileDescriptor getResourceAssetDescriptorImpl(String path) throws IOException {
		return zipFile.getAssetFileDescriptor(removeLeadingSlash(path));
	}
	
	private String removeLeadingSlash(String path) {
		if (path.startsWith(File.separator)) {
			return path.substring(1);
		}
		
		return path;
	}
}
