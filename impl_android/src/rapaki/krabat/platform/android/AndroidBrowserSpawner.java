package rapaki.krabat.platform.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import rapaki.krabat.platform.GenericBrowserSpawner;

public class AndroidBrowserSpawner implements GenericBrowserSpawner {

	private final Activity act;
	
	public AndroidBrowserSpawner(Activity act) {
		this.act = act;
	}
	
	public void spawnBrowser(String urlString) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(urlString));
		act.startActivity(i);
	}
}
