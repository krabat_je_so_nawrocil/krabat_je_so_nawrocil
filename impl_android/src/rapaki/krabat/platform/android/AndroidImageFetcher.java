/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;

import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageFetcher;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class AndroidImageFetcher extends GenericImageFetcher {

	private final AndroidStorageManager storageManager;
	
	private final String urlPrefix;
	
	public AndroidImageFetcher(AndroidStorageManager storageManager, String urlPrefix) {
		this.storageManager = storageManager;
		this.urlPrefix = urlPrefix;
	}
	
	public GenericImage fetchImage(String relativePath) {
		BufferedInputStream is = null;
		String tmp = relativePath.substring(0, relativePath.length() - 3) + "png";
		
		try {
			is = new BufferedInputStream(storageManager.getResourceStream(urlPrefix + File.separator + tmp));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Bitmap bitmap = BitmapFactory.decodeStream(is);
		// System.out.println("Density of .png bitmap is: " + bitmap.getDensity());
		if (bitmap == null) {
			System.err.println("Resource " + relativePath + " could not be opened!");
		}
		return new AndroidImage(bitmap);
	}

}
