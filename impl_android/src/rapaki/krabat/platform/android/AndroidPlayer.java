/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import java.io.File;
import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;

import rapaki.krabat.sound.AbstractPlayer;

public class AndroidPlayer extends AbstractPlayer {

	private final AndroidStorageManager storageManager;
	
	private final boolean useAssetFd;

	private MediaPlayer player;

	private boolean playerIsPaused;
	
	public AndroidPlayer(AndroidStorageManager storageManager, String urlBase, boolean useAssetFd) {
		super(urlBase);
		this.storageManager = storageManager;
		this.useAssetFd = useAssetFd;
		player = null;
		playerIsPaused = false;
	}

    public String getMusicDir() {
        return "sound" + File.separator + "music" + File.separator + "ogg" + File.separator;
    }
    
    public String getMusicSuffix() {
        return ".ogg";
    }
    
	public void play(String filename, boolean repeat) {
        System.out.println("Playing: " + filename);
		
        if (player != null) {
        	player.stop();
        	player.release();
        	player = null;
        }
        
		try {
    		player = new MediaPlayer();
    		if (useAssetFd == true) {
    			AssetFileDescriptor afd = storageManager.getResourceAssetDescriptor(urlBase + File.separator + filename);
    			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
    		} else {
    			player.setDataSource(storageManager.getResourceDescriptor(urlBase + File.separator + filename));
    		}
			player.setAudioStreamType(AudioManager.STREAM_MUSIC);
			player.setLooping(repeat);
			player.prepare();
			player.start();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stop() {
		if (player != null) {
			player.stop();
			player.release();
		}
		player = null;
	}

	public void pause() {
		if (player != null) {
			if (player.isPlaying()) {
				player.pause();
				playerIsPaused = true;
			}
		}
	}

	public void resume() {
		if (player != null) {
			if (playerIsPaused == true) {
				player.start();
				playerIsPaused = false;
			}
		}
	}
}
