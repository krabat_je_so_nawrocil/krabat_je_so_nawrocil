/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import rapaki.krabat.main.GenericColor;
import rapaki.krabat.main.GenericRectangle;
import rapaki.krabat.platform.GenericDrawingContext;
import rapaki.krabat.platform.GenericDrawingContext2D;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageObserver;

public class AndroidDrawingContext extends GenericDrawingContext {

	private final Canvas canvas;
	
	private final Paint currentPaint;
	
	public AndroidDrawingContext(Canvas canvas) {
		this.canvas = canvas;
		currentPaint = new Paint();
		currentPaint.setARGB(255, 0, 0, 0);
		currentPaint.setStyle(Paint.Style.STROKE);
	}
	
	public void clearRect(int x, int y, int width, int height) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		// paint.setStyle(Paint.Style.FILL_AND_STROKE);
		Rect r = new Rect(x, y, x + width, y + height);
		canvas.drawRect(r, paint);
	}

	public void drawImage(GenericImage genericImage, int x, int y,
			int width, int height, GenericImageObserver observer) {
		Bitmap bitmap = ((AndroidImage) genericImage).getBitmap();
		Rect destRect = new Rect(x, y, x + width, y + height);
		canvas.drawBitmap(bitmap, null, destRect, null);
	}

	public void drawImage(GenericImage genericImage, int x, int y,
			GenericImageObserver observer) {
		Bitmap bitmap = ((AndroidImage) genericImage).getBitmap();
		canvas.drawBitmap(bitmap, x, y, null);
	}

	public void drawImage(GenericImage genericImage, int x, int y) {
		drawImage(genericImage, x, y, null);
	}

	public void drawImage(GenericImage image, int x, int y,
			int width, int height) {
		drawImage(image, x, y, width, height, null);
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		currentPaint.setStyle(Paint.Style.STROKE);
		canvas.drawLine(x1, y1, x2, y2, currentPaint);
	}

	public void drawRect(int x, int y, int width, int height) {
		currentPaint.setStyle(Paint.Style.STROKE);
		Rect r = new Rect(x, y, x + width, y + height);
		canvas.drawRect(r, currentPaint);
	}

	public void fillRect(int x, int y, int width, int height) {
		currentPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		Rect r = new Rect(x, y, x + width, y + height);
		canvas.drawRect(r, currentPaint);
	}

	public GenericDrawingContext2D get2DContext() {
		return new AndroidDrawingContext2D(canvas);
	}

	public GenericRectangle getClipBounds() {
		Rect r = canvas.getClipBounds();
		return new GenericRectangle(
				r.left, r.top, 
				(r.right - r.left), 
				(r.bottom - r.top));
	}

	public void setClip(int x, int y, int width, int height) {
		int tmpX1 = x;
		int tmpY1 = y;
		int tmpX2 = x + width;
		int tmpY2 = y + height;
		tmpX1 = Math.min(tmpX1, canvas.getWidth());
		tmpY1 = Math.min(tmpY1, canvas.getHeight());
		tmpX2 = Math.min(tmpX2, canvas.getWidth());
		tmpY2 = Math.min(tmpY2, canvas.getHeight());
		
		Region r = new Region(tmpX1, tmpY1, tmpX2, tmpY2);
		canvas.clipRegion(r, Region.Op.REPLACE);
		// Rect r = new Rect(x, y, x + width, y + height);
		// canvas.clipRect(r);
	}

	public void setClip(GenericRectangle txx) {
		/*
		Rect r = new Rect(txx.getX(), txx.getY(), 
				txx.getX() + txx.getWidth(), 
				txx.getY() + txx.getHeight());
		canvas.clipRect(r);
		*/
		setClip(txx.getX(), txx.getY(), 
				txx.getWidth(), 
				txx.getHeight());
	}

	public void setColor(GenericColor color) {
		currentPaint.setARGB(255, color.getR(), color.getG(), color.getB());
	}

}
