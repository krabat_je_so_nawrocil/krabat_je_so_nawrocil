/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat.platform.android;

import java.io.File;
import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import rapaki.krabat.platform.GenericSoundEffectPlayer;

public class AndroidSoundEffectPlayer extends GenericSoundEffectPlayer {

	private final AndroidStorageManager storageManager;
	
	private final boolean useAssetFd;

	private static final String soundPrefix = "sound";
	
	public AndroidSoundEffectPlayer(AndroidStorageManager storageManager, String urlBase, boolean useAssetFd) {
		super(urlBase);
		this.storageManager = storageManager;
		this.useAssetFd = useAssetFd;
	}

	public void PlayFile(String filename) {
		// TODO might want to set the thread as daemon?
		new InternalPlayerThread(urlBase + File.separator + soundPrefix + File.separator + filename, useAssetFd).start();
	}
	
	class InternalPlayerThread extends Thread {
		
		private final String filename;
		
		private final boolean useAssetFd;
		
		public InternalPlayerThread(String filename, boolean useAssetFd) {
			this.filename = filename;
			this.useAssetFd = useAssetFd;
		}
		
		public void run() {
			try {
	    		MediaPlayer player = new MediaPlayer();
	    		System.out.println("Playing effect: " + filename);
	    		if (useAssetFd == true) {
	    			AssetFileDescriptor afd = storageManager.getResourceAssetDescriptor(filename);
	    			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
	    		} else {
	    			player.setDataSource(storageManager.getResourceDescriptor(filename));
	    		}
	    		player.setAudioStreamType(AudioManager.STREAM_MUSIC);
				player.setLooping(false);
				player.prepare();
				player.start();
				while (player.isPlaying() == true) {
					sleep(100);
				}
				player.release();
				player = null;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
