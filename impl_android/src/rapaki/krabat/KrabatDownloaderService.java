package rapaki.krabat;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

public class KrabatDownloaderService extends DownloaderService {

	public static final String BASE64_PUBLIC_KEY = PublicKeyAndSalt.BASE64_PUBLIC_KEY;
    
	public static final byte[] SALT = PublicKeyAndSalt.SALT;

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return KrabatAlarmReceiver.class.getName();
    }
}