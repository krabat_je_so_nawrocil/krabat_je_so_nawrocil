/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;

import rapaki.krabat.main.GameEventHints;
import rapaki.krabat.main.GenericInputEvent;
import rapaki.krabat.main.GenericKeyEvent;
import rapaki.krabat.main.GenericMouseEvent;
import rapaki.krabat.main.GenericPoint;
import rapaki.krabat.main.GenericToolkit;
import rapaki.krabat.platform.GenericContainer;
import rapaki.krabat.platform.GenericCursor;
import rapaki.krabat.platform.GenericImage;
import rapaki.krabat.platform.GenericImageFetcher;
import rapaki.krabat.platform.GenericImageObserver;
import rapaki.krabat.platform.GenericSoundEffectPlayer;
import rapaki.krabat.platform.GenericStorageManager;
import rapaki.krabat.platform.android.AndroidBrowserSpawner;
import rapaki.krabat.platform.android.AndroidContainer;
import rapaki.krabat.platform.android.AndroidContainerImpl;
import rapaki.krabat.platform.android.AndroidCursor;
import rapaki.krabat.platform.android.AndroidImage;
import rapaki.krabat.platform.android.AndroidImageFetcher;
import rapaki.krabat.platform.android.AndroidImageObserver;
import rapaki.krabat.platform.android.AndroidPlayer;
import rapaki.krabat.platform.android.AndroidSoundEffectPlayer;
import rapaki.krabat.platform.android.AndroidStorageManager;
import rapaki.krabat.platform.android.AndroidToolkitImpl;
import rapaki.krabat.sound.AbstractPlayer;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.TextView;

public class AndroidStart extends Activity implements OnTouchListener,
		AndroidContainerImpl, IDownloaderClient {
	
	private static final int expansionFileVersion = 5;
	
	private static final long expansionFileSize = 17105256;

	/*
	private static final String downloadRootPath = "http://www.rapaki.de/secret/evenmoresecret/krabatandroid/";
	
	private static final String[] gameDataFiles = new String[]{
		"graphics_id0001.zip",
		"music_id0001.zip",
		"sounds_id0001.zip"
	};
	
	private static final String[] idFiles = new String[]{
		"graphics_id0001.txt",
		"music_id0001.txt",
		"sounds_id0001.txt"
	};
	
	private static final int[] gameDataSizesMB = new int[]{
		25,
		12,
		8
	};
	*/
	
	private GenericCursor Cup, Cdown, Cleft, Cright, Cinventar, CHinventar,
			Normal, Kreuz, Warten, Nix;

	private Start appInstance;

	// private ImageView image;

	private KrabatView kView;

	private Bitmap rootMap;

	private int touchState;

	private GenericPoint downPoint;

	private GenericPoint lastMovePoint = new GenericPoint(0, 0);

	private GenericPoint firstZoomPoint;

	private GenericPoint secondZoomPoint;

	private GenericPoint ctrPoint = new GenericPoint(0, 0);

	private long downStartTime;

	private long lastDownStartTime;

	// if a down-up touch event differs in coordinates by more than this amount
	// then it is no longer a "click" but rather a "move"
	private static final int SHORT_PIXEL_DISTANCE = 35;

	private static final int SHORT_TAP_TIME_MS = 500;

	private static final int SHORT_DOWN_TIME_MS = 250;

	private GenericImage cursorImage;

	private Bitmap cursorBitmap;

	private static final boolean redirect_stdout_stderr_to_file = false;

	// private static final boolean useAssets = false;

	private static final boolean useExpansionFile = true;
	
	private SeekBar seekBar;
	
	private TextView textViewStatus;

	private TextView textViewSpeed;
	
	private CheckBox checkboxOptions;

	private IStub mDownloaderClientStub;
	
	private IDownloaderService mRemoteService;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		System.out.println("====>ON CREATE<====");

		// Check if expansion files are available before going any further
	    if (!expansionFilesDelivered()) {
	        // Build an Intent to start this activity from the Notification
	        Intent notifierIntent = new Intent(this, getClass());
	        notifierIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
	                                Intent.FLAG_ACTIVITY_CLEAR_TOP);

	        
	        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
	                notifierIntent, PendingIntent.FLAG_UPDATE_CURRENT);

	        // Start the download service (if required)
	        int startResult = DownloaderClientMarshaller.DOWNLOAD_REQUIRED;
			try {
				startResult = DownloaderClientMarshaller.startDownloadServiceIfRequired(this,
				                pendingIntent, KrabatDownloaderService.class);
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			
	        // If download has started, initialize this activity to show download progress
	        if (startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
	            // This is where you do set up to display the download progress (next step)
	            // Instantiate a member instance of IStub
	            mDownloaderClientStub = DownloaderClientMarshaller.CreateStub(this,
	                    KrabatDownloaderService.class);
	            // Inflate layout that shows download progress
	            setContentView(R.layout.downloader);

	    		seekBar = (SeekBar) findViewById(R.id.seekBarProgress);
	    		seekBar.setProgress(0);
	    		
	    		textViewStatus = (TextView) findViewById(R.id.textViewStatus);
	    		
	    		textViewSpeed = (TextView) findViewById(R.id.textViewSpeed);
	    		
	    		checkboxOptions = (CheckBox) findViewById(R.id.checkBoxOptions);
	    		checkboxOptions.setOnCheckedChangeListener(mOptionsCheckboxListener);
	    		
	            return;
	        } // If the download wasn't necessary, fall through to start the app
	        else
	        {
	        	System.out.println("Downloader service start result==no download required???");
	        }
	    }
		
		setFullscreen();
		setNoTitle();

		try {
			createGameUI();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private boolean expansionFilesDelivered() {
		String fileName = Helpers.getExpansionAPKFileName(this, true, expansionFileVersion);
		
		System.out.println("Searching for archive: " + fileName);
		
	    if (!Helpers.doesFileExist(this, fileName, expansionFileSize, false))
	    {
	    	System.out.println("File does NOT exist!");
	    	return false;
	    }
	    System.out.println("File exists with correct size!");
	    return true;
	}
	
	@Override
	public void onServiceConnected(Messenger m) {
		System.out.println("Downloader service onConnected()");
	    mRemoteService = DownloaderServiceMarshaller.CreateProxy(m);
	    mRemoteService.onClientUpdated(mDownloaderClientStub.getMessenger());
	}
	
	public void onDownloadStateChanged(int newState) {
		System.out.println("Downloader service state changed=" + newState);
		textViewStatus.setText(Helpers.getDownloaderStringResourceIDFromState(newState));
	}
	
	private static final DecimalFormat downloadFormat = new DecimalFormat("0.00");
	
	public void onDownloadProgress(DownloadProgressInfo progress) {
		System.out.println("Downloader service progress update: " 
				+ progress.mOverallProgress + "/" + progress.mOverallTotal);
		float fProgress = ((float) progress.mOverallProgress) / ((float) progress.mOverallTotal);
		int iProgress = (int) (fProgress * 100.0f);
		System.out.println("Seekbar value=" + iProgress);
		seekBar.setProgress(iProgress);
		textViewSpeed.setText(downloadFormat.format(progress.mCurrentSpeed) + " kByte/s");
	}
	
	OnCheckedChangeListener mOptionsCheckboxListener = new OnCheckedChangeListener() {

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (mRemoteService != null) {
				if (isChecked == true) {
					mRemoteService.setDownloadFlags(IDownloaderService.FLAGS_DOWNLOAD_OVER_CELLULAR);
				} else {
					mRemoteService.setDownloadFlags(0);
				}
			}
		}
		
	};
	
	private void createGameUI() throws IOException {
		setContentView(R.layout.krabatlayout);
		// image = (ImageView) findViewById(R.id.imageView1);
		// image.setScaleType(ScaleType.CENTER);
		// image.setBackgroundColor(Color.BLACK);
		kView = (KrabatView) findViewById(R.id.krabat);

		String urlPrefix;
		String expansionFileName = null;

		if (useExpansionFile == false) {
			urlPrefix = "/Android/data/rapaki.krabat.AndroidStart/files";
			urlPrefix = Environment.getExternalStorageDirectory()
					.getAbsolutePath()
					+ urlPrefix;
		} else {
			urlPrefix = "";
			expansionFileName = APKExpansionSupport.getMainExpansionFile(
					getApplicationContext(), expansionFileVersion);
		}

		System.out.println("Code base: " + urlPrefix);

		String state = Environment.getExternalStorageState();

		if (redirect_stdout_stderr_to_file == true) {
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				try {
					System.setErr(new PrintStream(new FileOutputStream(
							urlPrefix + File.separator + "systemerr.txt")));
					System.setOut(new PrintStream(new FileOutputStream(
							urlPrefix + File.separator + "systemout.txt")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("External media state is: " + state);
			}
		}

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int widthInPixels = metrics.widthPixels;
		int heightInPixels = metrics.heightPixels;

		System.out.println("-----> Display dimensions " + widthInPixels
				+ "x" + heightInPixels + " <-----");

		kView.setDisplayDimensions(widthInPixels, heightInPixels);

		appInstance = new Start();

		GenericStorageManager storageManager = new AndroidStorageManager(
				this.getAssets(), this, urlPrefix, false, useExpansionFile, 
				expansionFileName);
		GenericImageFetcher imageFetcher = new AndroidImageFetcher(
				(AndroidStorageManager) storageManager, urlPrefix);
		GenericImageObserver observer = new AndroidImageObserver();
		GenericContainer container = new AndroidContainer(this);
		GenericSoundEffectPlayer player = new AndroidSoundEffectPlayer(
				(AndroidStorageManager) storageManager, urlPrefix, useExpansionFile);
		GenericToolkit.impl = new AndroidToolkitImpl();
		AbstractPlayer musicPlayer = new AndroidPlayer(
				(AndroidStorageManager) storageManager, urlPrefix, useExpansionFile);

		appInstance.runGamePt1(0, imageFetcher, observer, container,
				player, musicPlayer, storageManager);
		GenericPoint pt = InitImages(imageFetcher);
		cursorImage = ((AndroidCursor) Nix).getCursorImage();
		appInstance
				.runGamePt2(pt, new GenericCursor[] { Cup, Cdown, Cleft,
						Cright, Cinventar, CHinventar, Normal, Kreuz,
						Warten, Nix });

		Bitmap escapeIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/escape.png")).getBitmap();
		Bitmap exitIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/exit.png")).getBitmap();
		Bitmap helpIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/help.png")).getBitmap();
		Bitmap menuIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/menu.png")).getBitmap();
		Bitmap openIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/open.png")).getBitmap();
		Bitmap saveIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/save.png")).getBitmap();
		Bitmap slownikIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/slownik.png")).getBitmap();
		Bitmap hotspotIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/hotspot.png")).getBitmap();
		Bitmap zoomIcon = ((AndroidImage) imageFetcher
				.fetchImage("gfx/icons/zoom.png")).getBitmap();

		kView.setIcons(escapeIcon, exitIcon, helpIcon, menuIcon, openIcon,
				saveIcon, slownikIcon, hotspotIcon, zoomIcon);

		touchState = 0;
		downPoint = new GenericPoint(0, 0);
		downStartTime = 0;
		lastDownStartTime = 0;

		kView.setOnTouchListener(this);

		appInstance.runGamePt3(new AndroidBrowserSpawner(this));
	}

	protected void onStart() {
		super.onStart();

		System.out.println("====>ON START<====");
		
		// Event-Listener hinzufuegen
		// image.setOnClickListener(this);
		// image.setOnLongClickListener(this);
		// if (downloadRequired == false) {
	}

	protected void onResume() {
		super.onResume();

		System.out.println("====>ON RESUME<====");
		
	    if (null != mDownloaderClientStub) {
	        mDownloaderClientStub.connect(this);
	    } else {
	    	if (appInstance != null) {
	    		appInstance.Freeze(false);
	    	}
	    	if (Start.player != null) {
	    		Start.player.resume();
	    	}
		}
	}

	protected void onPause() {
		super.onPause();

		System.out.println("====>ON PAUSE<====");
		
    	if (appInstance != null) {
    		appInstance.Freeze(true);
    	}
    	if (Start.player != null) {
    		Start.player.pause();
    	}
	}

	protected void onStop() {
		super.onStop();

		System.out.println("====>ON STOP<====");
		
	    if (null != mDownloaderClientStub) {
	        mDownloaderClientStub.disconnect(this);
	    } 
	}

	protected void onDestroy() {
		super.onDestroy();

		System.out.println("====>ON DESTROY<====");
		
		// System.exit(0);
	}

	// Cursorbilder vorbereiten
	private GenericPoint InitImages(GenericImageFetcher fetcher) {
		GenericImage Ccup = null;
		GenericImage Ccdown = null;
		GenericImage Ccleft = null;
		GenericImage Ccright = null;
		GenericImage NNormal = null;
		GenericImage KKreuz = null;
		GenericImage WWarten = null;
		GenericImage NNix = null;

		Ccup = fetcher.fetchImage("gfx/cursors/horje.gif");
		Ccdown = fetcher.fetchImage("gfx/cursors/dele.gif");
		Ccleft = fetcher.fetchImage("gfx/cursors/nalewo.gif");
		Ccright = fetcher.fetchImage("gfx/cursors/naprawo.gif");
		NNormal = fetcher.fetchImage("gfx/cursors/bezec4.gif");
		KKreuz = fetcher.fetchImage("gfx/cursors/bezec10.gif");
		WWarten = fetcher.fetchImage("gfx/cursors/cakac.gif");
		NNix = fetcher.fetchImage("gfx/cursors/trans.gif");

		// Cursorgroesse fuer jeweiliges System bestimmen
		// Dimension cursor = getToolkit().getBestCursorSize (32, 32);
		double x = 32;
		double y = 32;
		// System.out.println("Cursorgroesse : " + x + " " + y);

		int xxx = (int) x / 2;
		int yyy = (int) y / 2;
		// System.out.println ("Anpassung auf " + xxx + " " + yyy);

		// Mauscursor initialisieren
		Cup = GenericToolkit.getDefaultToolkit().createCustomCursor(Ccup,
				new GenericPoint(xxx, yyy), "Up");
		Cdown = GenericToolkit.getDefaultToolkit().createCustomCursor(Ccdown,
				new GenericPoint(xxx, yyy), "Down");
		Cleft = GenericToolkit.getDefaultToolkit().createCustomCursor(Ccleft,
				new GenericPoint(xxx, yyy), "Left");
		Cright = GenericToolkit.getDefaultToolkit().createCustomCursor(Ccright,
				new GenericPoint(xxx, yyy), "Right");
		Normal = GenericToolkit.getDefaultToolkit().createCustomCursor(NNormal,
				new GenericPoint(xxx, yyy), "Normal");
		Kreuz = GenericToolkit.getDefaultToolkit().createCustomCursor(KKreuz,
				new GenericPoint(xxx, yyy), "Kreuz");
		Warten = GenericToolkit.getDefaultToolkit().createCustomCursor(WWarten,
				new GenericPoint(xxx, yyy), "Warten");
		Nix = GenericToolkit.getDefaultToolkit().createCustomCursor(NNix,
				new GenericPoint(xxx, yyy), "Nix");

		return (new GenericPoint(xxx, yyy));
	}

	public boolean repaint(GameEventHints hints) {
		GenericImage img = appInstance.paint(null);
		if (img != null) {
			rootMap = ((AndroidImage) img).getBitmap();

			int scrollx = appInstance.scrollx;
			int scrolly = appInstance.scrolly;

			cursorBitmap = ((AndroidImage) cursorImage).getBitmap();

			return kView.drawNewBitmap(rootMap, scrollx, scrolly, cursorBitmap, hints);

			/*
			 * Message msg = handler.obtainMessage(0, scrollx, scrolly);
			 * handler.sendMessage(msg);
			 */
		} else {
			// System.out.println("Not drawing to screen - offscreen image is null!!!");
			return false;
		}
	}

	public void setCursorImage(GenericImage cursorImage) {
		this.cursorImage = cursorImage;
	}

	/*
	 * Handler handler = new Handler() { public void handleMessage(Message msg)
	 * { System.out.println("Obtained message with scroll coords " + msg.arg1 +
	 * " and " + msg.arg2 + ".");
	 * 
	 * image.setImageBitmap(Bitmap.createBitmap(rootMap, msg.arg1, msg.arg2,
	 * 640, 480)); } };
	 */

	/*
	 * public void onClick(View v) {
	 * 
	 * }
	 * 
	 * public boolean onLongClick(View v) { return true; }
	 */

	public boolean onTouch(View v, MotionEvent event) {
		if (v == kView) {

			GenericPoint currentEventPoint = new GenericPoint(kView
					.xCoordDisplayToViewport((int) event.getX()), kView
					.yCoordDisplayToViewport((int) event.getY()));

			switch (touchState) {

			case 0: // IDLE (UP)
				// this is the default state which is reached on UP and any
				// non-handled events (CANCEL ...)
				if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
					downPoint = new GenericPoint(currentEventPoint.x,
							currentEventPoint.y);
					lastMovePoint = new GenericPoint(currentEventPoint.x,
							currentEventPoint.y);
					lastDownStartTime = downStartTime;
					downStartTime = event.getEventTime();
					touchState = 1;
				}
				break;

			case 1: // DOWN
				// if we are in this state and receive an UP we
				// check for the time difference and the pointer difference
				if (event.getActionMasked() == MotionEvent.ACTION_UP) {
					GenericMouseEvent ge;
					if (isShortDistance(downPoint, currentEventPoint) == true) {
						if (isShortTime(downStartTime, event.getEventTime()) == true) {
							// a.k.a. left click, check for double-click
							ge = new GenericMouseEvent(
									(GenericInputEvent.BUTTON3_MASK - 1),
									downPoint, isShortDClickTime(
											lastDownStartTime, downStartTime));
						} else {
							// a.k.a. right click, no double-clicking
							ge = new GenericMouseEvent(
									(GenericInputEvent.BUTTON3_MASK),
									downPoint, false);
						}
						// left/right touch outside generates a separate event
						// for shortcuts
						if (event.getX() <= kView.getIconRegionStartX()) {
							appInstance.mousePressed(ge);
						} else {
							int iconsize = kView.getIconSize();

							if (event.getY() < (1 * iconsize)) {
								// exit
								appInstance.windowClosing();
							} else if (event.getY() < (2 * iconsize)) {
								// open
								appInstance.keyPressed(new GenericKeyEvent(
										GenericKeyEvent.VK_F3));
							} else if (event.getY() < (3 * iconsize)) {
								// save
								appInstance.keyPressed(new GenericKeyEvent(
										GenericKeyEvent.VK_F2));
							} else if (event.getY() < (4 * iconsize)) {
								// menu
								appInstance.keyPressed(new GenericKeyEvent(
										GenericKeyEvent.VK_F1));
							} else if (event.getY() < (5 * iconsize)) {
								// escape
								appInstance.keyPressed(new GenericKeyEvent(
										GenericKeyEvent.VK_ESCAPE));
							} else if (event.getY() < (6 * iconsize)) {
								// help
								kView.enableHotspots(appInstance.getHotspots());
							} else if (event.getY() < (7 * iconsize)) {
								// slownik
								appInstance.keyPressed(new GenericKeyEvent(
										GenericKeyEvent.VK_F5));
							} else if (event.getY() < (8 * iconsize)) {
								// zoom
								kView.resetZooming();
							}
						}
					} else {
						ge = new GenericMouseEvent(
								(GenericInputEvent.BUTTON3_MASK), downPoint,
								false);
						appInstance.mouseMoved(ge);
					}
					touchState = 0;
				} else if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
					GenericMouseEvent ge = new GenericMouseEvent(
							(GenericInputEvent.BUTTON3_MASK),
							currentEventPoint, false);
					appInstance.mouseMoved(ge);
					// only go to the real MOVE if we moved too far
					if (isShortDistance(downPoint, currentEventPoint) == false) {
						touchState = 2;
					}
				} else if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
					firstZoomPoint = new GenericPoint((int) event.getX(0),
							(int) event.getY(0));
					secondZoomPoint = new GenericPoint((int) event.getX(1),
							(int) event.getY(1));

					int newX = (firstZoomPoint.x + secondZoomPoint.x) / 2;
					int newY = (firstZoomPoint.y + secondZoomPoint.y) / 2;

					// newX and newY are now in display coordiantes, but we need
					// game coordinates
					ctrPoint = new GenericPoint(kView
							.xCoordDisplayToViewport(newX)
							+ appInstance.scrollx, kView
							.yCoordDisplayToViewport(newY)
							+ appInstance.scrolly);

					touchState = 3;
				} else {
					touchState = 0;
				}
				break;

			case 2: // MOVE
				GenericMouseEvent ge = new GenericMouseEvent(
						(GenericInputEvent.BUTTON3_MASK), currentEventPoint,
						false);
				if (event.getActionMasked() == MotionEvent.ACTION_UP) {
					appInstance.mouseMoved(ge);
					touchState = 0;
				} else if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
					ctrPoint.x += (int) ((lastMovePoint.x - currentEventPoint.x) * kView
							.getXScalingDisplayToViewport());
					ctrPoint.y += (int) ((lastMovePoint.y - currentEventPoint.y) * kView
							.getYScalingDisplayToViewport());
					kView.updateCenterPoint(ctrPoint);
					appInstance.mouseMoved(ge);
					lastMovePoint = currentEventPoint;
				} else if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
					firstZoomPoint = new GenericPoint((int) event.getX(0),
							(int) event.getY(0));
					secondZoomPoint = new GenericPoint((int) event.getX(1),
							(int) event.getY(1));

					int newX = (firstZoomPoint.x + secondZoomPoint.x) / 2;
					int newY = (firstZoomPoint.y + secondZoomPoint.y) / 2;

					// newX and newY are now in display coordiantes, but we need
					// game coordinates
					ctrPoint = new GenericPoint(kView
							.xCoordDisplayToViewport(newX)
							+ appInstance.scrollx, kView
							.yCoordDisplayToViewport(newY)
							+ appInstance.scrolly);

					touchState = 3;
				} else {
					touchState = 0;
				}
				break;
			case 3: // another pointer down
				if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
					// compute the distance changes of the two pointers for
					// every (historical) event
					// and update the zoom accordingly

					int pointerCount = event.getPointerCount();

					GenericPoint newFirstZoomPoint = new GenericPoint(
							(int) event.getX(0), (int) event.getY(0));

					GenericPoint newSecondZoomPoint = null;
					if (pointerCount > 1) {
						newSecondZoomPoint = new GenericPoint((int) event
								.getX(1), (int) event.getY(1));
					}

					// System.out.println("Old X coords: " + firstZoomPoint.x +
					// "," + secondZoomPoint.x);
					// System.out.println("New X coords: " + newFirstZoomPoint.x
					// + "," + newSecondZoomPoint.x);

					int oldDistance = Math.abs(secondZoomPoint.x
							- firstZoomPoint.x);
					int newDistance = Math.abs(newSecondZoomPoint.x
							- newFirstZoomPoint.x);

					// System.out.println("Distance change from " + oldDistance
					// + " to " + newDistance);

					// int newX = (newFirstZoomPoint.x + newSecondZoomPoint.x) /
					// 2;
					// int newY = (newFirstZoomPoint.y + newSecondZoomPoint.y) /
					// 2;

					// newX and newY are now in display coordiantes, but we need
					// game coordinates
					// GenericPoint ctrPoint = new
					// GenericPoint(kView.xCoordDisplayToViewport(newX) +
					// appInstance.scrollx,
					// kView.yCoordDisplayToViewport(newY) +
					// appInstance.scrolly);

					kView.updateCenterPoint(ctrPoint);
					kView.updateZooming(newDistance - oldDistance);

					// System.out.println("Applied zoom of " + (newDistance -
					// oldDistance));

					firstZoomPoint = newFirstZoomPoint;
					if (pointerCount > 1) {
						secondZoomPoint = newSecondZoomPoint;
					}
				} else {
					touchState = 0;
				}
				break;
			}

			return true;
		} else {
			System.out.println("Received event for view " + v.toString()
					+ " and ignored it.");
			return false;
		}
	}

	private void setFullscreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	private void setNoTitle() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	private boolean isShortDistance(GenericPoint p1, GenericPoint p2) {
		if (Math.abs(p1.x - p2.x) > SHORT_PIXEL_DISTANCE) {
			return false;
		}
		if (Math.abs(p1.y - p2.y) > SHORT_PIXEL_DISTANCE) {
			return false;
		}
		return true;
	}

	private boolean isShortTime(long time1, long time2) {
		if (time2 > time1) {
			if ((time2 - time1) < SHORT_TAP_TIME_MS) {
				return true;
			}
		}

		return false;
	}

	private boolean isShortDClickTime(long time1, long time2) {
		if (time2 > time1) {
			if ((time2 - time1) < SHORT_DOWN_TIME_MS) {
				return true;
			}
		}

		return false;
	}

	/*
	Handler seekBarHandler = new Handler() {
		public void handleMessage(Message msg) {
			if ((msg.what >= 0) && (msg.what <= 100)) {
				int progress = (int) (((float) msg.what) / ((float) totalDownloadSize) * 100.0f);
				seekBar.setProgress(progress);
			} else if (msg.what == 1000) {
				createGameUI();
			}
		}
	};
	*/

	/*
	OnClickListener mBackListener = new OnClickListener() {

		public void onClick(View v) {
			AndroidStart.this.finish();
		}

	};

	OnClickListener mStartListener = new OnClickListener() {

		public void onClick(View v) {
			if (dlThread == null) {
				dlThread = new DownloadThread();
				dlThread.start();
			}
		}

	};
	
	private boolean downloadRequired() {
		for (String oneFile : idFiles) {
			if (checkArchiveFinished(oneFile) == false) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean checkArchiveFinished(String idFileName) {
		File file = new File(storageRootPath + "/" + idFileName);
		boolean result = file.exists();
		System.out.println(file.getAbsolutePath() + " exists: " + result);
		return result;
	}

	private class DownloadThread extends Thread {

		public void run() {
			try {
				boolean success;
				File noMediaDir = new File(storageRootPath);
				if (noMediaDir.exists() == false) {
					success = noMediaDir.mkdirs();
					if (success == false) {
						throw new IOException("Creating root directory failed!");
					}
				}
				File noMediaIdentification = new File(noMediaFile);
				noMediaIdentification.createNewFile();
				downloadDataCounter = 0;
				for (int i = 0; i < gameDataFiles.length; i++) {
					if (checkArchiveFinished(idFiles[i]) == false) {
						System.out.println("Downloading file: " + gameDataFiles[i]);
						downloadFile(gameDataFiles[i]);
					} else {
						downloadDataCounter += gameDataSizesMB[i] * 1024;
					}
				}
				
				seekBarHandler.sendMessage(Message.obtain(seekBarHandler, 1000));
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void downloadFile(String filename) throws IOException {
			URL url1 = new URL(downloadRootPath + filename);
			HttpURLConnection conn1 = (HttpURLConnection) url1.openConnection();
			InputStream in1 = new BufferedInputStream(conn1.getInputStream());
			OutputStream out1 = new BufferedOutputStream(new FileOutputStream(
					storageRootPath + "/" + filename));

			boolean atEOF = false;
			int pos;
			byte[] data = new byte[1024];
			long currentDataCounter = downloadDataCounter;
			do {
				pos = in1.read(data);
				if (pos == -1) {
					atEOF = true;
				} else {
					out1.write(data, 0, pos);
				}
				downloadDataCounter++;
				if (downloadDataCounter - currentDataCounter > 512) {
					System.out.println("-------->SEEKBAR UPDATE<--------");
					System.out.println("Downloaded data=" + downloadDataCounter + " kB");
					seekBarHandler.sendMessage(Message.obtain(seekBarHandler, (int) (downloadDataCounter / 512)));
					currentDataCounter = downloadDataCounter;
				}
			} while (atEOF == false);

			out1.flush();
			out1.close();
			conn1.disconnect();

			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(
					new FileInputStream(storageRootPath + "/" + filename)));
			ZipEntry ze;
			while ((ze = zis.getNextEntry()) != null) {
				atEOF = false;

				System.out.println("Unzipping file: " + ze.getName());

				File existing = new File(storageRootPath + File.separator
						+ ze.getName());
				if (existing.exists()) {
					// if directory removal failed, we might not even care
					existing.delete();
				}

				// assume that the appropriate directory entry is always packed
				// into
				// the zip file before any files within that dir
				// so we only need to check for directory or file
				if (ze.isDirectory() == true) {
					// System.out.println("is a directory.");
					existing.mkdirs();
				} else {

					// System.out.println("is a file.");

					OutputStream file = new BufferedOutputStream(
							new FileOutputStream(storageRootPath + File.separator
									+ ze.getName()));

					do {
						pos = zis.read(data);
						if (pos == -1) {
							atEOF = true;
						} else {
							file.write(data, 0, pos);
						}
					} while (atEOF == false);

					file.flush();
					file.close();
				}
			}
			zis.close();

		}
	}
	*/
}
