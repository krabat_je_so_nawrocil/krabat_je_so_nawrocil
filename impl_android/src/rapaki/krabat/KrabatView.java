/*
    The Krabat Adventure
    Copyright (C) 2001  Rapaki 
    http://www.rapaki.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package rapaki.krabat;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import rapaki.krabat.main.GameEventHints;
import rapaki.krabat.main.GenericPoint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class KrabatView extends SurfaceView implements SurfaceHolder.Callback {

    public static final int MIN_X_WIDTH = 160;
    
    private static final int MAX_ZOOMSTEP = 40;
	
	private final SurfaceHolder holder;
    
    private Bitmap bitmap;
    
    private Bitmap cursorBitmap;
    
    private int scrollx;
    
    private int scrolly;
    
    private boolean surfaceValid;
    
    private Semaphore drawingFinishedSem;
    
    private final Paint cursorPaint;
    
    private int displayWidth;
    
    private int displayHeight;
    
    private int maxGameWidth;
    
    private int maxGameHeight;
    
    private int iconsize;
    
    private int xWidth;
    
    private int newXWidth;
    
    private GenericPoint centerPoint;

    private Rect srcRect;
    
    private Rect destRect;
	
    private Rect cursorRect;
    
    private Bitmap escapeIcon;
    
    private Rect escapeRect;
    
    private Bitmap exitIcon;
    
    private Rect exitRect;
    
    private Bitmap helpIcon;
    
    private Rect helpRect;
    
    private Bitmap menuIcon;
    
    private Rect menuRect;
    
    private Bitmap openIcon;
    
    private Rect openRect;
    
    private Bitmap saveIcon;
    
    private Rect saveRect;
    
    private Bitmap slownikIcon;
    
    private Rect slownikRect;
    
    private Bitmap zoomIcon;
    
    private Rect zoomRect;
    
    private Bitmap hotspotIcon;
    
    private int hotspotCounter;
    
    private ArrayList<GenericPoint> hotspots;
    
	public KrabatView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        surfaceValid = false;
        
        drawingFinishedSem = new Semaphore(0);
        
        hotspotCounter = 0;
        
        cursorPaint = new Paint();
        cursorPaint.setColor(Color.BLACK);
        // cursorPaint.setStyle(Paint.Style.FILL);
        
        // register our interest in hearing about changes to our surface
        holder = getHolder();
        holder.addCallback(this);

        setFocusable(true); // make sure we get key events
    }
	
	public void setIcons(Bitmap escapeIcon, Bitmap exitIcon, Bitmap helpIcon, Bitmap menuIcon, 
			Bitmap openIcon, Bitmap saveIcon, Bitmap slownikIcon, Bitmap hotspotIcon,
			Bitmap zoomIcon) {
		this.escapeIcon = escapeIcon;
		this.exitIcon = exitIcon;
		this.helpIcon = helpIcon;
		this.menuIcon = menuIcon;
		this.openIcon = openIcon;
		this.saveIcon = saveIcon;
		this.slownikIcon = slownikIcon;
		this.hotspotIcon = hotspotIcon;
		this.zoomIcon = zoomIcon;
	}
	
	public void enableHotspots(ArrayList<GenericPoint> hotspots) {
		this.hotspots = hotspots;
		hotspotCounter = 20;
	}
    
    /*
     * Callback invoked when the Surface has been created and is ready to be
     * used.
     */
    public void surfaceCreated(SurfaceHolder holder) {
    	surfaceValid = true;
    }

    /* Callback invoked when the surface dimensions change. */
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    	surfaceValid = false;
    }
    
	public void setDisplayDimensions(int widthInPixels, int heightInPixels) {
		displayWidth = widthInPixels;
		displayHeight = heightInPixels;
		
		// compute the "destination rectangle", i.e. that part of the screen
		// that we will draw the game window on
		// we want to have some space at the right side for extra actions and displays
		// so keep that in mind
		
		maxGameWidth = displayWidth;
		maxGameHeight = displayHeight;
		
		if (maxGameHeight < ((maxGameWidth * 3) / 4)) {
			maxGameWidth = ((maxGameHeight * 4) / 3);
		} else {
			maxGameHeight = (maxGameWidth * 3) / 4;
		}
		
		// now check if we have, or make some space for the icons & status on the right
		int resultingSpaceX = displayWidth - maxGameWidth;
		int maxIconHeight = displayHeight / 9;
		
		if (resultingSpaceX < maxIconHeight) {
			maxGameWidth -= (maxIconHeight - resultingSpaceX);
		}
		
		System.out.println("Dimension of game window: " + maxGameWidth + "x" + maxGameHeight);
		
		destRect = new Rect(0, 0, maxGameWidth, maxGameHeight);
		
		// define the place where to draw the icons and the status window (cursor
		int tmp = 0;
		iconsize = maxIconHeight;
		exitRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		openRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		saveRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		menuRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		escapeRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		helpRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		slownikRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		zoomRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		tmp += iconsize;
		cursorRect = new Rect(maxGameWidth, tmp, maxGameWidth + iconsize, tmp + iconsize);
		
		// start with no scrolling active
		scrollx = 0;
		scrolly = 0;
		
		// no zoom active
		xWidth = 640;
		newXWidth = 640;
		
		// centerpoint's game screen coords are chosen so that they are in the
		// middle of the viewport
		centerPoint = new GenericPoint(320, 240);
		
		computeSrcRect();
	}
	
	public int getIconRegionStartX() {
		return maxGameWidth;
	}
	
	public int getIconSize() {
		return iconsize;
	}
	
	public void updateCenterPoint(GenericPoint pt) {
		centerPoint = pt;
		
		computeSrcRect();
	}
	
	public void resetZooming() {
		xWidth = 640;
		newXWidth = 640;
		
		computeSrcRect();
	}
	
	public void updateZooming(int relZoom) {
		xWidth -= relZoom;
		newXWidth = xWidth;
		
		computeSrcRect();
	}
	
	public void updateSmoothZooming(int newRelZoom) {
		newXWidth = xWidth - newRelZoom;
		
		// sanity check of requested zooming
		if (newXWidth < MIN_X_WIDTH) {
			newXWidth = MIN_X_WIDTH;
		}
		if (newXWidth > 640) {
			newXWidth = 640;
		}

		computeSrcRect();
	}
	
	// translate the touch coordinate from display coordinates
	// to those of the viewport
	public int xCoordDisplayToViewport(int x) {
		// don't do it of it is outside of the range of the game
		if (x > maxGameWidth) {
			return x;
		}
		
		// at first compute the scale ratio
		float ratio = getXScalingDisplayToViewport();
		
		// return the x coordinate in viewport coordinates
		return ((int) (((float) x) * ratio)) + srcRect.left - scrollx; 
	}
	
	// translate the touch coordinate from display coordinates
	// to those of the viewport
	public int yCoordDisplayToViewport(int y) {
		// don't do it of it is outside of the range of the game
		if (y > maxGameHeight) {
			return y;
		}
		
		// at first compute the scale ratio
		float ratio = getYScalingDisplayToViewport();
		
		return ((int) (((float) y) * ratio)) + srcRect.top - scrolly; 
	}
	
	public int xCoordViewportToDisplay(int x) {
		// at first compute the scale ratio
		float ratio = getXScalingDisplayToViewport();
		
		// return the x coordinate in viewport coordinates
		return ((int) (((float) (x + scrollx - srcRect.left)) / ratio)); 
	}
	
	// translate the touch coordinate from display coordinates
	// to those of the viewport
	public int yCoordViewportToDisplay(int y) {
		// at first compute the scale ratio
		float ratio = getYScalingDisplayToViewport();
		
		return ((int) (((float) (y + scrolly - srcRect.top)) / ratio)); 
	}
	
	public float getXScalingDisplayToViewport() {
		return (((float) srcRect.right - srcRect.left) / (float) maxGameWidth);
	}
	
	public float getYScalingDisplayToViewport() {
		return (((float) srcRect.bottom - srcRect.top) / (float) maxGameHeight);
	}

	// float scaleX = ((float) (destRect.right - destRect.left) / ((float) (srcRect.right - srcRect.left)));
	// float scaleY = ((float) (destRect.bottom - destRect.top) / ((float) (srcRect.bottom - srcRect.top)));


    
    public boolean drawNewBitmap(Bitmap bitmap, int scrollx, int scrolly, Bitmap cursorBitmap, GameEventHints hints) {
		if (surfaceValid == true) {
	    	// this.bitmap = Bitmap.createBitmap(bitmap, scrollx, scrolly, 640, 480);
			this.bitmap = bitmap;
			this.scrollx = scrollx;
			this.scrolly = scrolly;
			computeSrcRect();
			this.cursorBitmap = cursorBitmap;
	    	Message msg = handler.obtainMessage(0);
	    	msg.obj = hints;
			handler.sendMessage(msg);
			try {
				drawingFinishedSem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			/*
			while (drawingFinished == false) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			*/
			return true;
		}
		
		return false;
    }

    private void computeSrcRect() {
		int left = 0;
		int top = 0;
		int right = 0;
		int bottom= 0;
		
		// smooth zooming when requested
		if (newXWidth != xWidth) {
			// zoom in
			if (newXWidth < xWidth) {
				if ((xWidth - newXWidth) > MAX_ZOOMSTEP) {
					xWidth -= MAX_ZOOMSTEP; 
				} else {
					xWidth = newXWidth;
				}
			} else {
				// zoom out
				if ((newXWidth - xWidth) > MAX_ZOOMSTEP) {
					xWidth += MAX_ZOOMSTEP;
				} else {
					xWidth = newXWidth;
				}
			}
		}

		// sanity check of requested zooming
		if (xWidth < MIN_X_WIDTH) {
			xWidth = MIN_X_WIDTH;
		}
		if (xWidth > 640) {
			xWidth = 640;
		}

		// compute zoomed y range
		int yWidth = (int) (((float) xWidth) * 3.0f / 4.0f);
		
		int nrTries = 3;
		boolean finished = false;
		
		while ((nrTries > 0) && (finished == false)) {
			// initial computation with optimum values
			left = centerPoint.x - (xWidth / 2);
			right = centerPoint.x + (xWidth / 2);
			top = centerPoint.y - (yWidth / 2);
			bottom = centerPoint.y + (yWidth / 2);
			
			finished = true;
			
			String debugCorr = "";
			
			// check x and y coordinates outside of (scrolling) viewport
			if (left < scrollx) {
				centerPoint.x += (scrollx - left);
				finished = false;
				debugCorr = "X++;";
			} 
			if (right > (scrollx + 640)) {
				centerPoint.x -= (right - (scrollx + 640));
				finished = false;
				debugCorr += "X--;";
			}
			if (top < scrolly) {
				centerPoint.y += (scrolly - top);
				finished = false;
				debugCorr += "Y++";
			} 
			if (bottom > (scrolly + 480)) {
				centerPoint.y -= (bottom - (scrolly + 480));
				finished = false;
				debugCorr += "Y--";
			}
			
			System.out.println(debugCorr);
			
			nrTries--;
		}

		srcRect = new Rect(left, top, right, bottom);
		
		/*
		System.out.println("centerPoint=" + centerPoint.x + "," + centerPoint.y
				+ " rect=" + srcRect.left + "," + srcRect.top + "," + srcRect.right + ","
				+ srcRect.bottom + ", triesleft=" + nrTries);
				*/
	}

    Handler handler = new Handler() {
    	
    	public void handleMessage(Message m) {
            Canvas c = null;
            GameEventHints hints = (GameEventHints) m.obj;
            int hintMask = hints.getCurrentHints();
            try {
                c = holder.lockCanvas(null);
        		// c.drawBitmap(bitmap, 0, 0, null);
                c.drawBitmap(bitmap, srcRect, destRect, null);
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_EXIT) != 0) {
                	c.drawBitmap(exitIcon, null, exitRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_OPEN) != 0) {
                	c.drawBitmap(openIcon, null, openRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_SAVE) != 0) {
                	c.drawBitmap(saveIcon, null, saveRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_MENU) != 0) {
                	c.drawBitmap(menuIcon, null, menuRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_ESCAPE) != 0) {
                	c.drawBitmap(escapeIcon, null, escapeRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_HOTSPOT) != 0) {
                	c.drawBitmap(helpIcon, null, helpRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_DICTIONARY) != 0) {
                	c.drawBitmap(slownikIcon, null, slownikRect, null);
                }
                if ((hintMask & GameEventHints.GAME_EVENT_HINT_ZOOM) != 0) {
                	c.drawBitmap(zoomIcon, null, zoomRect, null);
                }
                
                c.drawRect(cursorRect, cursorPaint);
                c.drawBitmap(cursorBitmap, cursorRect.left, cursorRect.top, null);
                if (hotspotCounter > 0) {
        			for (GenericPoint pt : hotspots) {
                		if ((pt.x > (srcRect.left + 24)) && 
                				(pt.x < srcRect.right - 24) &&
                				(pt.y >= srcRect.top) &&
                				(pt.y <= srcRect.bottom)) {
                			
                			// at first subtract the scrolling offset and the graphic size
                			int newX = pt.x - scrollx;
                			int newY = pt.y - scrolly;
                			
                			c.drawBitmap(hotspotIcon, xCoordViewportToDisplay(newX) - 24, yCoordViewportToDisplay(newY) - 24, null);
                		}
                	}
                	hotspotCounter--;
                }
        		bitmap = null;
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    holder.unlockCanvasAndPost(c);
                }
                drawingFinishedSem.release();
            }
    	}
    	
    };
    

}
